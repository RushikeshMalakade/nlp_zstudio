from typing import Optional
from fastapi import BackgroundTasks,FastAPI,File, Form, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from transformers import AutoModelForSequenceClassification
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
import numpy as np
from scipy.special import softmax
from utils import s3_handler,get_ner_ui_elements,get_merged_phrase,get_classification_ui_elements
import json
from pydantic import BaseModel
from io import StringIO,BytesIO
import pandas as pd


origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://10.226.66.220:8888",
    "http://10.226.66.220",
    "http://10.226.66.220:8000",
]




class body(BaseModel):
    sentence: str
        
class bodylist(BaseModel):
    sentences:list
        


model_name='Go Emotion Detection'

s3_handler_=s3_handler(model_name)

config = s3_handler_.load_object('config.json')
tokenizer = AutoTokenizer.from_pretrained(config['model_path'])
model = AutoModelForSequenceClassification.from_pretrained(config['model_path'])
Labels={'0': 'admiration', '1': 'amusement', '10': 'disapproval', '11': 'disgust', '12': 'embarrassment', '13': 'excitement', '14': 'fear', '15': 'gratitude', '16': 'grief', '17': 'joy', '18': 'love', '19': 'nervousness', '2': 'anger', '20': 'optimism', '21': 'pride', '22': 'realization', '23': 'relief', '24': 'remorse', '25': 'sadness', '26': 'surprise', '27': 'neutral', '3': 'annoyance', '4': 'approval', '5': 'caring', '6': 'confusion', '7': 'curiosity', '8': 'desire', '9': 'disappointment'}


app = FastAPI(title = config["model_name"].upper().replace('_',' ')+" API",
             version = config["model_version"],
            description = config["model_description"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def predict(sentence,all_=False):
    encoded_input = tokenizer(sentence, return_tensors='pt')
    output = model(**encoded_input)
    scores = output[0][0].cpu().detach().numpy()
    scores = softmax(scores)
    out={'sentence':sentence,'output':[[Labels[str(np.argmax(scores))],float(np.max(scores))]]}
    return out


@app.post("/predict_sentence/")
def predict_sentence(sentence: body):
    return predict(sentence.sentence)



@app.post("/predict_sentence_ui/")
def predict_sentence_ui(sentence: body):
    out=predict(sentence.sentence)
    out['UI'] = get_classification_ui_elements(out['output'],config['labels'])
    return out 

@app.post("/predict_sentences/")
def predict_sentences(sentences: bodylist):
    out={}
    for _,sentence in enumerate(sentences.sentences):
        print(sentence)
        out[_]=predict(sentence)         
    return out

@app.post("/predict_file/")
def predict_file(file: bytes = File(...)):
    s=str(file,'utf-8')
    data=pd.read_csv(StringIO(s))
    data['output']=data['text'].apply(lambda x: predict(x)['output'])
    towrite = StringIO()
    data.to_csv(towrite)
    towrite.seek(0)
    return towrite.getvalue()
   
@app.get("/summaries")
def model_summaries():
    return {'Report':model.report,"Accuracy Log":model.accuracy_log,"Loss Log":model.loss_log}


@app.get("/train")
def train(learning_rate: Optional[float] = config["learning_rate"], epochs: Optional[int] = config["epochs"]):
        
    global model,config
    
    try:
        model.learning_rate=learning_rate
        model.prepare_dataset()
        xpath="model_v"+ str(config["model_version"]) +".dmp"
        status=model.fine_tune_model(epochs,s3_handler_,xpath)
        print(status)
        if type(status) == dict:
            return {"status": 200,"message":"Model is already trained for "+ str(epochs) + " Epochs."}
        config["model_version"]=int(config["model_version"])+1
        app.version = config["model_version"]
        config["learning_rate"]=model.learning_rate
        config["epochs"]=model.epochs
        config["model_path"]=xpath
        config["model_path_epoch"]=model.model_path_epoch
        config["model_path_best"]=config["model_path"]
        s3_handler_.upload_object(config,'config.json')
        print("model trained")
        response= s3_handler_.upload_object(model,config["model_path"])
        return response
    except Exception as e:
        print(e)
        return {"status":500 ,"message":"model training failed"}


@app.get("/")
def home():
        return {"status":"200","message":"App is running"}

