import pandas as pd 
import numpy as np
import torch
import transformers
from torch.utils.data import Dataset, DataLoader
from transformers import BertModel, BertTokenizer
import joblib
import copy
from sklearn.metrics import confusion_matrix, classification_report
from sklearn import metrics
from sklearn.model_selection import train_test_split
import os
import functools
import operator


def list_flatten(a):
    return functools.reduce(operator.iconcat, a, [])

class BertDataset_Multilabel(Dataset):
    def __init__(self, dataframe, tokenizer, max_len, clf_type):
        self.len = len(dataframe)
        self.data = dataframe.reset_index(drop=True)
        self.tokenizer = tokenizer
        self.max_len = max_len
        
    def __getitem__(self, index):
        text = str(self.data.text[index])
        text = " ".join(text.split())
        inputs = self.tokenizer.encode_plus(
            text,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True,
            truncation=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']
        token_type_ids = inputs['token_type_ids']
        return {
            'ids': torch.tensor(ids),
            'mask': torch.tensor(mask),
            'token_type_ids': torch.tensor(token_type_ids),
            'targets': torch.tensor(self.data.label_list[index])
        } 
    
    def __len__(self):
        return self.len


class Custom_BERT_Multilabel(torch.nn.Module):
    def __init__(self, num_labels,base_path="bert-base-uncased"):
        super(Custom_BERT_Multilabel, self).__init__()
        self.l1 = transformers.BertModel.from_pretrained(base_path)
        self.l2 = torch.nn.Dropout(0.3)
        self.l3 = torch.nn.Linear(768, num_labels)
    
    def forward(self, ids, mask, token_type_ids):
        _, output_1= self.l1(ids, attention_mask = mask, token_type_ids = token_type_ids,return_dict=False)
        output_2 = self.l2(output_1)
        output = self.l3(output_2)
        return output
    
    
class Bert_MultiLabel:
    def __init__(self, train_data, val_data, test_data, save_model_path, base_path="bert-base-uncased",
                 train_batch_size = 4,valid_batch_size = 8,epochs = 5,learning_rate = 1e-05,max_len = 500):
        '''
        lm_data : Data to train Language Model.
        clf_data : Data to Train Classification Moel. 
        test_data : Data to generate Confusion Report.
        save_model_path : Path to save the model.  
        '''
        self.val_data = val_data.reset_index(drop=True)
        self.train_data = train_data.reset_index(drop=True)
        self.test_data = test_data.reset_index(drop=True)
        self.model_path = save_model_path
        self.base_path=base_path
        self.label_to_name = None
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.tokenizer = BertTokenizer.from_pretrained(self.base_path)
        self.max_len = max_len
        self.train_batch_size = train_batch_size
        self.valid_batch_size = valid_batch_size
        self.epochs = epochs
        self.epochs_new = -1
        self.learning_rate = learning_rate
        self.loss_log={}
        self.accuracy_log={}
        self.report={}
  
        
    def read_data(self,file_path=False):
        if file_path:
            if self.val_data_path != None:
                self.val_data = pd.read_csv(self.val_data)

            if self.train_data_path!=None:
                self.train_data = pd.read_csv(self.train_data)

            if self.test_data_path!=None:
                self.test_data = pd.read_csv(self.test_data)
            
            
    def utils_to_train(self):
        '''
        This function saves the pickle label to name pickle file and also reorders the column in train dataframe.
        Only for train.
        '''
        
        other_cols = [col for col in list(self.train_data.columns) if col != 'text']
        other_cols =  sorted(other_cols)
        self.label_to_name = {idx:elem for idx,elem in enumerate(other_cols)} 
        self.labels = list(self.label_to_name.values())
        self.model = Custom_BERT_Multilabel(num_labels=len(self.label_to_name),base_path=self.base_path)
        self.model.to(self.device)
        self.optimizer = torch.optim.Adam(params =  self.model.parameters(), lr=self.learning_rate)

    def utils_to_predict(self,data):
        '''
        This function is utility function when we standalone predict on dataset. 
        '''

        data = BertDataset_Multilabel(data, self.tokenizer, self.max_len, 'multilabel')
        test_params = {'batch_size': self.valid_batch_size}
        return DataLoader(data, **test_params)
        
        
    def prepare_dataset(self):
        '''
        This function converts the dataset.
        '''
        self.train_data['label_list'] = self.train_data[self.labels].values.tolist() 
        self.val_data['label_list'] = self.val_data[self.labels].values.tolist() 
        self.test_data['label_list'] = self.test_data[self.labels].values.tolist() 
        training_set = BertDataset_Multilabel(self.train_data, self.tokenizer, self.max_len, 'multilabel')
        validation_set = BertDataset_Multilabel(self.val_data, self.tokenizer, self.max_len, 'multilabel')
        test_set = BertDataset_Multilabel(self.test_data, self.tokenizer, self.max_len, 'multilabel')
        
        train_params = {'batch_size': self.train_batch_size,
                'shuffle': True}

        valid_params = {'batch_size': self.valid_batch_size,
                'shuffle': True}
        
        test_params = {'batch_size': self.valid_batch_size,
                'shuffle': True}
        
        self.training_loader = DataLoader(training_set, **train_params)
        self.validation_loader = DataLoader(validation_set, **valid_params)
        self.test_loader = DataLoader(test_set, **test_params)
        
        
    def loss_fn(self, outputs, targets):
        return torch.nn.BCEWithLogitsLoss()(outputs, targets)
    
        
    def load_model(self, pred= False):
        if pred == True:
            model_path = self.model_path + '/model.bin'
            self.model = torch.load(model_path)
        else:
            self.model = Custom_BERT_Multilabel(num_labels=len(self.label_to_name),base_path=self.base_path)
            self.optimizer = torch.optim.Adam(params =  self.model.parameters(), lr=self.learning_rate)
            self.model.to(self.device)
    
    def save_model(self):
        '''
        This function saves the model at given path. 
        '''
        model_path = self.model_path + '/model.bin'
        vocab_path = self.model_path + '/vocab.bin'
        torch.save(self.model , model_path)
        self.tokenizer.save_vocabulary(vocab_path)
        
    def calcuate_accu(self,big_idx, targets):
        n_correct = (big_idx==targets).sum().item()
        return n_correct
        
    def train(self,epoch,s3_handler_=None,path='./'):
        tr_loss = 0
        nb_tr_steps = 0
        nb_tr_examples = 0
        self.model.train()
        
        for _,data in enumerate(self.training_loader, 0):
            ids = data['ids'].to(self.device)
            mask = data['mask'].to(self.device)
            targets = data['targets'].to(self.device)
            token_type_ids = data['token_type_ids'].to(self.device)
            outputs = self.model(ids, mask, token_type_ids )
            targets = targets.type_as(outputs)
            loss = self.loss_fn(outputs, targets)
            tr_loss += loss.item()
            nb_tr_steps += 1
            nb_tr_examples+=targets.size(0)

            if (_%50==0) and (_!=0):
                loss_step = tr_loss/nb_tr_steps
                print(f"\tTraining Loss per 50 steps: {loss_step}")
                
            self.optimizer.zero_grad()
            loss.backward()
            # # When using GPU
            self.optimizer.step()
            torch.cuda.empty_cache()


        ##print(f'The Total Accuracy for Epoch {epoch}: {(n_correct*100)/nb_tr_examples}')
        epoch_loss = tr_loss/nb_tr_steps
        
        val_report=self.valid(self.validation_loader)
        test_report=self.valid(self.test_loader)
        self.learning_rate = self.optimizer.param_groups[0]['lr']
        self.loss_log['Epoch '+str(epoch)]={'Training Loss':epoch_loss,'Validation Loss':val_report['loss'],
                              'Test Loss':test_report['loss'], 'Learning Rate':self.learning_rate}
        outputs=np.array(outputs.cpu().detach().numpy().tolist()) >= 0.5
        self.accuracy_log['Epoch '+str(epoch)]={'Training Accuracy':metrics.accuracy_score(list_flatten(targets.cpu().detach().numpy().tolist()), list_flatten(outputs)),
                              'Validation Accuracy':val_report['accuracy'],
                              'Test Accuracy':test_report['accuracy'],
                             'Training F1':metrics.f1_score(list_flatten(targets.cpu().detach().numpy().tolist()), list_flatten(outputs)),
                              'Validation F1':val_report['f1'],
                              'Test F1':test_report['f1'],
                             'Training Precision':metrics.precision_score(list_flatten(targets.cpu().detach().numpy().tolist()), list_flatten(outputs)),
                              'Validation Precision':val_report['precision'],
                              'Test Precision':test_report['precision'],
                             'Training Recall':metrics.recall_score(list_flatten(targets.cpu().detach().numpy().tolist()), list_flatten(outputs)),
                              'Validation Recall':val_report['recall'],
                              'Test Recall':test_report['recall']}
        self.test_report()
        self.model_path_epoch='model_epoch_'+str(epoch)+'.dmp'
        self.save_model(s3_handler_,self.model_path_epoch)
        
        print(f"\tTraining Loss Epoch: {epoch_loss}")

    def valid(self,validation_loader):
        self.model.eval()
        fin_targets=[]
        fin_outputs=[]
        nb_valid_steps = 0
        val_loss = 0
        with torch.no_grad():
            for _, data in enumerate(validation_loader, 0):
                ids = data['ids'].to(self.device)
                mask = data['mask'].to(self.device)
                token_type_ids = data['token_type_ids'].to(self.device)
                targets = data['targets'].to(self.device, dtype = torch.float)
                outputs = self.model(ids, mask, token_type_ids)
                loss = self.loss_fn(outputs, targets)
                val_loss += loss.item()
                fin_targets.extend(targets.cpu().detach().numpy().tolist())
                fin_outputs.extend(torch.sigmoid(outputs).cpu().detach().numpy().tolist())
                nb_valid_steps += 1
                torch.cuda.empty_cache()
        
        outputs = np.array(fin_outputs) >= 0.5
        val_loss = val_loss/nb_valid_steps
                
        return {'accuracy': metrics.accuracy_score(list_flatten(fin_targets), list_flatten(outputs)),
                'f1' : metrics.f1_score(list_flatten(fin_targets), list_flatten(outputs)),
                'precision' : metrics.precision_score(list_flatten(fin_targets), list_flatten(outputs)),
                'recall' : metrics.recall_score(list_flatten(fin_targets), list_flatten(outputs)),
                 'loss':val_loss}

    

    def fine_tune_model(self, epochs_new=-1,s3_handler_=None,path='./'):
        '''
        This function fine tunes the Bert Model.
        '''
        best_loss = 100000
        if self.epochs >= epochs_new and self.epochs_new != -1:
            return {"status":"model already trained"}
        for epoch in range(self.epochs) if self.epochs_new == -1 else range(self.epochs,epochs_new):
            print('Training Epoch {} out of {} epochs.'.format(epoch+1, self.epochs))
            self.train(epoch,s3_handler_,path)
            val_loss = self.valid(self.validation_loader)['loss']
            if val_loss < best_loss :
                print('\tSaving best model')
                self.save_model(s3_handler_,path)   
                best_loss = val_loss
        self.epochs = epochs_new
        self.epochs_new = epochs_new
            
    def save_model(self,s3_handler_=None,path='./'):
        '''
        This function saves the model at given path. 
        '''
        if s3_handler_:
            s3_handler_.upload_object(self,path)        
        else:
            joblib.dump(self,path)
        
    def predict_samples(self,test_loader):
        '''
        This function predicts the labels on the data and returns those. 
        '''
        all_results = []
        with torch.no_grad():
            for _, unseen_data in enumerate(test_loader, 0):
                ids = unseen_data['ids'].to(self.device)
                mask = unseen_data['mask'].to(self.device)
                token_type_ids = unseen_data['token_type_ids'].to(self.device)
                outputs = self.model(ids, mask, token_type_ids).squeeze()
                result = torch.sigmoid(outputs).cpu().detach().numpy()
                torch.cuda.empty_cache()
                all_results.extend([result.tolist()] if len(result.shape) == 1 else result.tolist())
                
        
        return all_results
        
    
    def predict_multilabels_dataframe(self, pred_data,prob_thresh = 0.5):
        '''
        This function predicted the classes. 
        '''
        test_data = pred_data.copy()
        test_data['label_list'] = [[0]*len(self.label_to_name)]*len(test_data)
        labels = list(self.label_to_name.values())
        for label in labels:
            test_data[label]=0
            test_data[label+'_score']=0
        test_loader=self.utils_to_predict(test_data)
        pred_labels = self.predict_samples(test_loader)
        pred_labels = np.stack(pred_labels)
        test_data[[label+'_score' for label in labels]] = pred_labels
        matrix_thresh = np.full(pred_labels.shape, prob_thresh)
        pred_labels = np.where(pred_labels <= matrix_thresh, 0 , 1).tolist()
        test_data[labels] = pred_labels
        if 'label_list' in list(test_data.columns):
            test_data.drop('label_list', axis =1, inplace = True)
        torch.cuda.empty_cache()
        return test_data
    
    def predict_multilabels_sentences(self, sentences):
        '''
        This function predicted the classes. 
        '''
        test_data = pd.DataFrame({'text': sentences})
        test_data['label_list'] = [[0]*len(self.label_to_name)]*len(test_data)
        labels = list(self.label_to_name.values())
        test_loader=self.utils_to_predict(test_data)
        pred_labels = self.predict_samples(test_loader)
        pred_labels = np.stack(pred_labels)
        test_data['output'] = [ sorted(list(zip(labels,score)), key = lambda x: x[1],reverse=True) for score in pred_labels]
        torch.cuda.empty_cache()
        return test_data[['text','output']].rename({'text':'sentence'},axis=1).to_dict('index') 
    
    def test_report(self):
        
        self.report=self.generate_confusion_df(self.test_data, self.predict_multilabels_dataframe(self.test_data.copy()))
        

    def generate_confusion_df(self, df_target, df_pred):
        '''
        This function generates the classification report.
        '''
        
        col_names = list(self.label_to_name.values())
        clf_report = pd.DataFrame(classification_report(df_target[col_names].values, df_pred[col_names].values , 
                                                        output_dict = True)).transpose()
        
        met={'F1 Score':clf_report.loc['weighted avg','f1-score'],'Accuracy':metrics.accuracy_score(list_flatten(df_target[col_names].values), list_flatten(df_pred[col_names].values)),
                          'Precision':clf_report.loc['weighted avg','precision'],'Recall':clf_report.loc['weighted avg','recall']}
        clf_report = clf_report.reset_index()
        label_to_name = {str(key):val for key,val in self.label_to_name.items()}
        clf_report['index'] = clf_report['index'].apply(lambda x: label_to_name[x] if x in label_to_name else x)
        clf_report=clf_report.set_index('index')
        return {'Metrics':met,'Report':clf_report.to_dict('index')}