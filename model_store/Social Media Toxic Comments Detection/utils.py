import tempfile
import boto3
import joblib
import os
import requests


class s3_handler():
    def __init__(self,folder_name,
    iamrole='aws-a0038-glbl-00-s-rol-shrd-awb-ads-stg_28',
    bucket_name='aws-a0038-glbl-00-s-s3b-shrd-awb-ads-stg-28'):
        r = requests.get('http://169.254.169.254/latest/meta-data/iam/security-credentials/'+iamrole)
        r = r.json()
        self.s3 = boto3.Session(aws_access_key_id=r['AccessKeyId'],aws_secret_access_key=r['SecretAccessKey'],
                            aws_session_token=r['Token'])
        self.s3 = self.s3.resource('s3').Bucket(bucket_name)
        self.folder_name='nlp_zstudio_models/'+folder_name
        
        
    def upload_object(self,obj,file_name):
        
        try:
            with tempfile.TemporaryFile() as fp:
                joblib.dump(obj, fp)
                fp.seek(0)
                self.s3.upload_fileobj(fp,self.folder_name+'/'+file_name)
            return { "Status":"Model Object Saved Sucessfully"}
        except Exception as e:
            print(e)
            return { "Status":"Model Object Upload Failed"}
    
    def load_object(self,file_name):
        
        try:
            with tempfile.TemporaryFile() as fp:
                self.s3.download_fileobj(Fileobj=fp, Key=self.folder_name+'/'+file_name)
                fp.seek(0)
                model = joblib.load(fp)
            return model
        except Exception as e:
            print(e)
            return { "Status":"Model Object Download Failed"}
            
                
def get_classification_ui_elements(data,labels):
    return ''.join(['<span class="model-label" style="background: '+labels[i[0]]+'; vertical-align: super;">'+i[0]+'<span class="model-label-inner">'+str(i[1])[:5]+'</span></span>' for i in data]) 

        
        