from typing import Optional
from fastapi import BackgroundTasks,FastAPI,File, Form, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from classifier import *
from utils import s3_handler,get_classification_ui_elements
import json
from pydantic import BaseModel
from io import StringIO,BytesIO



origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://10.226.66.220:8888",
    "http://10.226.66.220",
    "http://10.226.66.220:8000",
]




class body(BaseModel):
    sentence: str
        
class bodylist(BaseModel):
    sentences:list
        


model_name='Social Media Toxic Comments Detection'

s3_handler_=s3_handler(model_name)

config = s3_handler_.load_object('config.json')
model = s3_handler_.load_object(config["model_path"])



if type(model) == dict:
    print('No model found')
    model=Bert_MultiLabel(s3_handler_.load_object(config["train_data_path"]), 
                          s3_handler_.load_object(config["val_data_path"]), 
                          s3_handler_.load_object(config["test_data_path"]) , None ,
                            base_path=config["base_path"],
                            train_batch_size=config["train_batch_size"],
                            valid_batch_size=config["valid_batch_size"],
                            epochs=config["epochs"],
                            learning_rate=config["learning_rate"],
                            max_len=config["max_len"])
    model.read_data()
    model.utils_to_train()
    s3_handler_.upload_object(model,config["model_path"])

app = FastAPI(title = config["model_name"].upper().replace('_',' ')+" API",
             version = config["model_version"],
            description = config["model_description"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/predict_sentence/")
def predict_sentence(sentence: body):
    return model.predict_multilabels_sentences([sentence.sentence])[0]



@app.post("/predict_sentence_ui/")
def predict_sentence_ui(sentence: body):
    out = model.predict_multilabels_sentences([sentence.sentence])[0] 
    out['UI'] = get_classification_ui_elements(out['output'],config['labels'])
    return out 

@app.post("/predict_sentences/")
def predict_sentences(sentences: bodylist):
    return model.predict_multilabels_sentences(sentences)

@app.post("/predict_file/")
def predict_file(file: bytes = File(...)):
    s=str(file,'utf-8')
    data=model.predict_multilabels_dataframe(pd.read_csv(StringIO(s)))
    towrite = StringIO()
    data.to_csv(towrite)
    towrite.seek(0)
    return towrite.getvalue()
   
@app.get("/summaries")
def model_summaries():
    return {'Report':model.report,"Accuracy Log":model.accuracy_log,"Loss Log":model.loss_log}


@app.get("/train")
def train(learning_rate: Optional[float] = config["learning_rate"], epochs: Optional[int] = config["epochs"]):
        
    global model,config
    
    try:
        model.learning_rate=learning_rate
        model.prepare_dataset()
        xpath="model_v"+ str(config["model_version"]) +".dmp"
        status=model.fine_tune_model(epochs,s3_handler_,xpath)
        print(status)
        if type(status) == dict:
            return {"status": 200,"message":"Model is already trained for "+ str(epochs) + " Epochs."}
        config["model_version"]=int(config["model_version"])+1
        app.version = config["model_version"]
        config["learning_rate"]=model.learning_rate
        config["epochs"]=model.epochs
        config["model_path"]=xpath
        config["model_path_epoch"]=model.model_path_epoch
        config["model_path_best"]=config["model_path"]
        s3_handler_.upload_object(config,'config.json')
        print("model trained")
        response= s3_handler_.upload_object(model,config["model_path"])
        return response
    except Exception as e:
        print(e)
        return {"status":500 ,"message":"model training failed"}


@app.get("/")
def home():
        return {"status":"200","message":"App is running"}

