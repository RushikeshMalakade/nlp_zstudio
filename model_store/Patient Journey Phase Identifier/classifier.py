import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split


from flair.models.text_classification_model import TARSClassifier
from flair.data import Sentence
from flair.data import Corpus
from flair.datasets import SentenceDataset
from flair.trainers import ModelTrainer
import seaborn as sn
import joblib
import copy
import pickle
import os

class Flair_FSL_MultiClass:
    def __init__(self, clf_data_path, test_data_path, save_model_path,model_name):
        '''
        clf_data : Data to Train Classification Moel. 
        test_data_path : Data to generate Confusion Report.
        save_model_path : Path to save the model.  
        '''
        self.train_data_path = clf_data_path
        self.test_data_path = test_data_path
        self.model_path = save_model_path
        self.train_data = None
        self.test_data = None
        self.names_categories = None
        self.corpus = None
        if model_name in os.listdir(save_model_path):
            self.tars_model=TARSClassifier.load(save_model_path+'/'+model_name)
        else:
            print('No model found')
            self.tars_model=TARSClassifier.load('tars-base')
            
    def save_pickle_file(self, save_dict, pickle_file_path):
        '''
        This function saves the pickle file. 
        '''
        with open(pickle_file_path, 'wb') as handle:
            pickle.dump(save_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
            
    def read_pickle(self, pickle_file_path):
        with open(pickle_file_path, 'rb') as handle:
            label_to_name = pickle.load(handle)
        return label_to_name
    
    def read_data(self):

        if self.train_data_path!=None:
            self.train_data = pd.read_csv(self.train_data_path)
            #self.train_data = self.train_data.sample(n = 2000, random_state = 0).reset_index(drop = True)
        if self.test_data_path!=None:
            self.test_data = pd.read_csv(self.test_data_path)
    
    def utils(self):
        '''
        This function saves the pickle label to name pickle file and also reorders the column in train dataframe. 
        '''
        #other_cols = [col for col in list(self.train_data.columns) if col != 'text']
        other_cols = self.train_data['label'].unique()
        other_cols =  sorted(other_cols)
        self.label_to_name = {idx:elem for idx,elem in enumerate(other_cols)}
        pickle_file_name = self.model_path + '/label_to_name.pickle'
        self.save_pickle_file(self.label_to_name, pickle_file_name)

    
    def convert_data_flair_format(self, data):
        '''
        This function converts the data in flair format. 
        '''
        flair_sentence = []
        for i in range(len(data)):
            flair_sentence.append(Sentence(data.iloc[i]['text']).add_label('journey_phase', data.iloc[i]['label']))
        return flair_sentence  
        
    
    def prepare_training_corpus(self):
        '''
        This function prepares the training corpus to train the model. 
        '''
        self.train_data = self.train_data.reset_index(drop = True)
        self.test_data = self.test_data.reset_index(drop = True)
        train_sentences = self.convert_data_flair_format(self.train_data)
        test_sentences = self.convert_data_flair_format(self.test_data)     
        train = SentenceDataset(train_sentences)  # training dataset
        test = SentenceDataset(test_sentences)  # test dataset
        self.corpus = Corpus(train=train, test=test)
              
        
    def train_classification_model(self, project_model_path):
        '''
        This function trains and saves the classifier model. 
        '''
        #Updating the training Corpus
        self.prepare_training_corpus()
        tars = TARSClassifier.load('tars-base')
        tars.add_and_switch_to_new_task("JOURNEY_PHASES", label_dictionary=self.corpus.make_label_dictionary())
        trainer = ModelTrainer(tars, self.corpus)        
        trainer.train(base_path=self.model_path, # path to store the model artifacts
              learning_rate=0.02, # use very small learning rate
              mini_batch_size=1, # small mini-batch size since corpus is tiny
              max_epochs=5, # terminate after 5 epochs
              train_with_dev=True,
              )
        
    def do_predictions_few_shot(self,text):

        sentence = Sentence(text)
        self.tars_model.predict(sentence)
        labels = sentence.labels
#         label = labels[0].value
        return labels
        
    
        
    def predict_multiclass(self,pred_df,get_prediction_results):
        '''
        This function loads the trained model and runs the multi-label predictions and convert those predictions in prodigy format. 
        '''
        label_to_name = self.read_pickle(self.model_path + '/label_to_name.pickle')
        pred_df = copy.deepcopy(pred_df)
        trained_model = TARSClassifier.load(self.model_path + '/final-model.pt')
        pred_df['pred_label'] = pred_df['text'].apply(lambda x: self.do_predictions_few_shot(x, trained_model))
        if get_prediction_results == True:
            return pred_df
        annotation_data =  convert_multiclass_pred_prodigy(pred_df, label_to_name)
        return annotation_data
    
    
    def generate_confusion_df(self, df_target, df_pred):
        '''
        This function generates the classification report.
        '''
        label_to_name = self.read_pickle(self.model_path + '/label_to_name.pickle')
        col_names = list(label_to_name.values())
        clf_report = pd.DataFrame(classification_report(df_target['label'].values, df_pred['pred_label'].values , output_dict = True)).transpose()
        f1_score = clf_report.loc['accuracy','f1-score']
        clf_report = clf_report.reset_index()
        return clf_report,f1_score
    
    def update_confusion_matrix(self, model_summary_path, confusion_matrix,f1_score):
        if not os.path.exists(model_summary_path):
            os.mkdir(model_summary_path)
            
        if not os.path.isfile(model_summary_path + '/' + 'overall_summary.csv'):
            summ_df = pd.DataFrame(columns = ['Model Version','Training Sample Count','Num Training Classes','F-1 Score'])
        else:
            summ_df = pd.read_csv(model_summary_path + '/' + 'overall_summary.csv')
        version_name = self.model_path.split('/')[-1]
        
        num_labels = len(confusion_matrix)-4
        
        if self.train_data_path:
            num_samples = len(self.train_data)
        else:
            num_samples = 0
        summ_df.loc[len(summ_df)] = [version_name,num_labels,num_samples,f1_score]
        
        summ_df.to_csv(model_summary_path + '/' + 'overall_summary.csv',index = False)
        confusion_matrix.to_csv(model_summary_path + '/' +version_name+'_summary.csv', index = False)