from joblib import Parallel,delayed
import copy
import pandas as pd
from unidecode import unidecode
import contractions
import re

def text_tokenizer(string):
    '''
    Convert string to list of tokens and seperating non alaphabets from alpha numeric and extra spaces.
    takes:
        string: string to tokeinze
    '''
    t = ' '
    for x in string :
        if not str.isalpha(x) and not x.isdigit() and x != ' ' :
            if t[-1] != ' ':
                t+= ' '
            t += x
            t += ' '
        else: t += x
    return unidecode(t[1:].strip())

import string
def punct_rep(text):
    last = None
    output = []

    for c in text:

        if c != last:
            if c in string.punctuation:
                last = c
            else:
                last = None
            output.append(c)

    return (''.join(output))


def correct_contractions(text):
    expanded_words= []
    for word in text.split():  # using contractions.fix to expand the shotened words
        expanded_words.append(contractions.fix(word))
        expanded_text= ' '.join(expanded_words)
    return expanded_text


def parallel(data,function,number_cuts=6,batchsize=20071):
    return pd.concat(Parallel(n_jobs=-1,max_nbytes='1M')(delayed(function)
                                 (copy.deepcopy(data.iloc[i*batchsize:i*batchsize+batchsize, ])) for i in (range(0, number_cuts))))



def data_processing(data,text_column='content'):
    '''
    Cleans the data for specified column by filling missing values and convert to lower case
    takes:
        data:pandas dataframe object for data
        text-column:column name which needs to be cleaned
    '''
    data[text_column]=data[text_column].fillna(' ').apply(unidecode)
    data[text_column]=data[text_column].apply(lambda x: punct_rep(x))
    
    data[text_column] = data[text_column].apply(correct_contractions)
    data[text_column] = data[text_column].apply(lambda x: re.sub(r"([0-9]+(\.[0-9]+)?)",r" \1 ", x).strip())    
    data[text_column]=data[text_column].apply(lambda x: ' '.join(x.split())) 
    data['clean_'+text_column]=data[text_column].apply(lambda x: text_tokenizer(x.lower()))
    data['token']=data['clean_'+text_column].str.split()
  
    return data

def parallel_data_processing(data,number_cuts):
    '''
    Runs job on given number of cpu.
    takes:
        data: data to be processed
        number_cuts: number of section to divide the job (number of available cpu)
    '''
    data=parallel(data,data_processing,number_cuts,int(len(data)/number_cuts)+1)
    return data