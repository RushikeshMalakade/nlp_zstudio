import os
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn.utils import clip_grad_norm_
import matplotlib.pyplot as plt
import time
import logging
import re
import pandas as pd
from tqdm.notebook import tqdm
from src.BERT.modeling_bert import BertModel as Model
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from src.BERT.tokenization_bert import BertTokenizer as Tokenizer
from src.tasks.train_funcs import load_state, load_results, evaluate_, evaluate_results
import joblib
tqdm.pandas()
from sklearn.metrics import classification_report
# from src.ontology_utility import *
from sklearn.metrics import precision_recall_fscore_support

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', \
                    datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

class Relations_Mapper(object):
    def __init__(self, relations):
        self.rel2idx = {}
        self.idx2rel = {}
        
        logger.info("Mapping relations to IDs...")
        self.n_classes = 0
        for relation in tqdm(relations):
            if relation not in self.rel2idx.keys():
                self.rel2idx[relation] = self.n_classes
                self.n_classes += 1
        
        for key, value in self.rel2idx.items():
            self.idx2rel[value] = key

class Pad_Sequence():
    """
    collate_fn for dataloader to collate sequences of different lengths into a fixed length batch
    Returns padded x sequence, y sequence, x lengths and y lengths of batch
    """
    def __init__(self, seq_pad_value, label_pad_value=-1, label2_pad_value=-1,\
                 ):
        self.seq_pad_value = seq_pad_value
        self.label_pad_value = label_pad_value
        self.label2_pad_value = label2_pad_value
        
    def __call__(self, batch):
        sorted_batch = sorted(batch, key=lambda x: x[0].shape[0], reverse=True)
        seqs = [x[0] for x in sorted_batch]
        seqs_padded = pad_sequence(seqs, batch_first=True, padding_value=self.seq_pad_value)
        x_lengths = torch.LongTensor([len(x) for x in seqs])
        
        labels = list(map(lambda x: x[1], sorted_batch))
        labels_padded = pad_sequence(labels, batch_first=True, padding_value=self.label_pad_value)
        y_lengths = torch.LongTensor([len(x) for x in labels])
        
        labels2 = list(map(lambda x: x[2], sorted_batch))
        labels2_padded = pad_sequence(labels2, batch_first=True, padding_value=self.label2_pad_value)
        y2_lengths = torch.LongTensor([len(x) for x in labels2])
        
        return seqs_padded, labels_padded, labels2_padded, \
                x_lengths, y_lengths, y2_lengths

class semeval_dataset(Dataset):
    
    def get_e1e2_start(self,x, e1_id, e2_id):
        try:
            e1_e2_start = ([i for i, e in enumerate(x) if e == e1_id][0],\
                            [i for i, e in enumerate(x) if e == e2_id][0])
        except Exception as e:
            e1_e2_start = None
            logger.warn(e)
        return e1_e2_start
    
    def __init__(self, df, tokenizer, e1_id, e2_id):
        self.e1_id = e1_id
        self.e2_id = e2_id
        self.df = df
        logger.info("Tokenizing data...")
        self.df['input'] = self.df.progress_apply(lambda x: tokenizer.encode(x['sents']),\
                                                             axis=1)
        
        self.df['e1_e2_start'] = self.df.progress_apply(lambda x: self.get_e1e2_start(x['input'],\
                                                       e1_id=self.e1_id, e2_id=self.e2_id), axis=1)
        print("\nInvalid rows/total: %d/%d" % (df['e1_e2_start'].isnull().sum(), len(df)))
        self.df.dropna(axis=0, inplace=True)
    
    def __len__(self,):
        return len(self.df)
        
    def __getitem__(self, idx):
        return torch.LongTensor(self.df.iloc[idx]['input']),\
                torch.LongTensor(self.df.iloc[idx]['e1_e2_start']),\
                torch.LongTensor([self.df.iloc[idx]['relations_id']])
    
class RELTraining(object):
    
    def __init__(self,model='bert-base-uncased',lower_case_flag= True, model_name= 'BERT', learning_rate=0.00007, max_length=50000, model_no=0, batch_size=8, 
                 gradient_acc_steps=2, max_norm=1, epochs=8, fp16=False, model_path='model/', amp= None):
        self.cuda = torch.cuda.is_available()
        self.model = model
        self.lower_case = lower_case_flag
        self.model_name = model_name
        self.learning_rate=learning_rate
        self.max_length=max_length

        self.model_no= model_no
        self.batch_size=batch_size
        self.gradient_acc_steps=gradient_acc_steps
        self.max_norm=max_norm
        self.epochs=epochs
        self.fp16=fp16
        self.model_path=model_path
        self.amp = amp

    
    def preprocess_semeval2010_8(self,df_train,df_test):

        '''
        Data preprocessing for SemEval2010 task 8 dataset
        '''

        if len(df_train)%self.batch_size !=0:
            df_train=pd.concat([df_train,df_train.iloc[:self.batch_size-len(df_train)%self.batch_size]])
        
        if len(df_test)%self.batch_size !=0:
            df_test=pd.concat([df_test,df_test.iloc[:self.batch_size-len(df_test)%self.batch_size]])

        rm = Relations_Mapper(df_train['relations'])
        df_test['relations_id'] = df_test.progress_apply(lambda x: rm.rel2idx[x['relations']], axis=1)
        df_train['relations_id'] = df_train.progress_apply(lambda x: rm.rel2idx[x['relations']], axis=1)
        logger.info("Finished and saved!")
        return df_train, df_test, rm
    
    def load_state(self,net, optimizer, scheduler,model_no,model_path, load_best=False):
        """ Loads saved model and optimizer states if exists """
        base_path = model_path
        amp_checkpoint = None
        checkpoint_path = os.path.join(base_path,"test_checkpoint_%d.pth.tar" % model_no)
        best_path = os.path.join(base_path,"test_model_best_%d.pth.tar" % model_no)
        start_epoch, best_pred, checkpoint = 0, 0, None
        if (load_best == True) and os.path.isfile(best_path):
            checkpoint = torch.load(best_path)
            logger.info("Loaded best model.")
        elif os.path.isfile(checkpoint_path):
            checkpoint = torch.load(checkpoint_path)
            logger.info("Loaded checkpoint model.")
        if checkpoint != None:
            start_epoch = checkpoint['epoch']
            best_pred = checkpoint['best_acc']
            net.load_state_dict(checkpoint['state_dict'])
            if optimizer is not None:
                optimizer.load_state_dict(checkpoint['optimizer'])
            if scheduler is not None:
                scheduler.load_state_dict(checkpoint['scheduler'])
            amp_checkpoint = checkpoint['amp']
            logger.info("Loaded model and optimizer.")    
        return start_epoch, best_pred, amp_checkpoint
    
    def train(self, train_file, test_file, pretrained_model):
        
        df_train, df_test, rm = self.preprocess_semeval2010_8(train_file,test_file)
        net= Model.from_pretrained(pretrained_model,force_download=False,model_size=self.model,
                                        task='classification',n_classes_=rm.n_classes)
        tokenizer = Tokenizer.from_pretrained(pretrained_model, do_lower_case=False)
        tokenizer.add_tokens(['[E1]', '[/E1]', '[E2]', '[/E2]', '[BLANK]'])
        e1_id = tokenizer.convert_tokens_to_ids('[E1]')
        e2_id = tokenizer.convert_tokens_to_ids('[E2]')
        
        train_set = semeval_dataset(df_train, tokenizer=tokenizer, e1_id=e1_id, e2_id=e2_id)
        test_set = semeval_dataset(df_test, tokenizer=tokenizer, e1_id=e1_id, e2_id=e2_id)
        train_length = len(train_set); test_length = len(test_set)
        PS = Pad_Sequence(seq_pad_value=tokenizer.pad_token_id,\
                          label_pad_value=tokenizer.pad_token_id,\
                          label2_pad_value=-1)
        train_loader = DataLoader(train_set, batch_size=self.batch_size, shuffle=True, \
                                  num_workers=0, collate_fn=PS, pin_memory=False)
        test_loader = DataLoader(test_set, batch_size=self.batch_size, shuffle=True, \
                                          num_workers=0, collate_fn=PS, pin_memory=False)
        cuda = torch.cuda.is_available()
        if cuda:
            net.cuda()

        net.resize_token_embeddings(len(tokenizer))
        criterion = nn.CrossEntropyLoss(ignore_index=-1)
        optimizer = optim.Adam([{"params":net.parameters(), "lr": self.learning_rate}])

        scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[2,4,6,8,12,15,18,20,22,23,24,26,30], gamma=0.8)
        
        start_epoch, best_pred, amp_checkpoint = self.load_state(net, optimizer, scheduler, self.model_no, self.model_path,load_best=False)
        
        losses_per_epoch, accuracy_per_epoch, test_f1_per_epoch = load_results(self.model_no)
        
        pad_id = tokenizer.pad_token_id
        mask_id = tokenizer.mask_token_id
        
        update_size =len(train_loader)//32
        
        joblib.dump(rm,self.model_path+'/class_info_'+str(self.model_no)+'.dmp')
        joblib.dump(tokenizer,self.model_path+'/tokenizer_'+str(self.model_no)+'.dmp')
        
        best_train_f1 = 0.0
        best_test_f1  = 0.0
        train_f1_per_epoch = []

        for epoch in tqdm(range(start_epoch, self.epochs)):
            start_time = time.time()
            net.train(); total_loss = 0.0; losses_per_batch = []; total_acc = 0.0; accuracy_per_batch = []; tr_f1_per_batch = []; tr_f1 = 0
            for i, data in tqdm(enumerate(train_loader, 0),total=len(train_loader)):
                try:
                    x, e1_e2_start, labels, _,_,_ = data
                    attention_mask = (x != pad_id).float()
                    token_type_ids = torch.zeros((x.shape[0], x.shape[1])).long()

                    if cuda:
                        x = x.cuda()
                        labels = labels.cuda()
                        attention_mask = attention_mask.cuda()
                        token_type_ids = token_type_ids.cuda()
                        
                    classification_logits = net(x, token_type_ids=token_type_ids, attention_mask=attention_mask,Q=None,e1_e2_start=e1_e2_start)

                    loss = criterion(classification_logits, labels.squeeze(1))
                    loss = loss/self.gradient_acc_steps


                    loss.backward()

                    if self.fp16:
                        grad_norm = torch.nn.utils.clip_grad_norm_(self.amp.master_params(optimizer), self.max_norm)
                    else:
                        grad_norm = clip_grad_norm_(net.parameters(), self.max_norm)

                    if (i % self.gradient_acc_steps) == 0:
                        optimizer.step()
                        optimizer.zero_grad()

                    total_loss += loss.item()
                    evaluated = evaluate_(classification_logits, labels, \
                                           ignore_idx=-1)
#                     print(evaluated)
                    total_acc += evaluated[0]

                    # Added here
                    y_pred = evaluated[1][0]
                    y_true = evaluated[1][1]
#                     print("Y true:",y_true)
#                     print("Y_pred:",y_pred)
                    mets = precision_recall_fscore_support(y_true, y_pred, average='macro')
#                     print(mets)
                    tr_f1 += mets[2]

                    if (i % update_size) == (update_size - 1):
                        losses_per_batch.append(self.gradient_acc_steps*total_loss/update_size)
                        accuracy_per_batch.append(total_acc/update_size)
                        
                        tr_f1_per_batch.append(tr_f1/update_size)
                        
                        logger.info('[Epoch: %d, %5d/ %d points] total loss, accuracy per batch: %.3f, %.3f' %
                              (epoch + 1, (i + 1)*self.batch_size, train_length, losses_per_batch[-1], accuracy_per_batch[-1]))
                        total_loss = 0.0; total_acc = 0.0;tr_f1 = 0.0

                        scheduler.step()
                        results = evaluate_results(net, test_loader, pad_id, cuda)
                        losses_per_epoch.append(sum(losses_per_batch)/len(losses_per_batch))
                        accuracy_per_epoch.append(sum(accuracy_per_batch)/len(accuracy_per_batch))
                        train_f1_per_epoch.append(sum(tr_f1_per_batch)/len(tr_f1_per_batch))
                        test_f1_per_epoch.append(results['f1'])

                        logger.info("Epoch finished, took %.2f seconds." % (time.time() - start_time))
                        logger.info("Losses at Epoch %d: %.7f" % (epoch + 1, losses_per_epoch[-1]))
                        logger.info("Train accuracy at Epoch %d: %.7f" % (epoch + 1, accuracy_per_epoch[-1]))
                        logger.info("Test f1 at Epoch %d: %.7f" % (epoch + 1, test_f1_per_epoch[-1]))
                        logger.info("Train f1 at Epoch %d: %.7f" % (epoch + 1, train_f1_per_epoch[-1]))

                        if train_f1_per_epoch[-1] > best_train_f1:
                            best_train_f1 = train_f1_per_epoch[-1]
                            torch.save({
                                    'epoch': epoch + 1,\
                                    'state_dict': net.state_dict(),\
                                    'best_acc': accuracy_per_epoch[-1],\
                                    'optimizer' : optimizer.state_dict(),\
                                    'scheduler' : scheduler.state_dict(),\
                                    'amp': self.amp.state_dict() if self.amp is not None else self.amp
                                }, os.path.join(self.model_path , "task_test_model_best_%d.pth.tar" % self.model_no))

                        # Create checkpoint model after every 3 epochs
                        if (epoch % 3) == 0:
                            torch.save({
                                    'epoch': epoch + 1,\
                                    'state_dict': net.state_dict(),\
                                    'best_acc': accuracy_per_epoch[-1],\
                                    'optimizer' : optimizer.state_dict(),\
                                    'scheduler' : scheduler.state_dict(),\
                                    'amp': self.amp.state_dict() if self.amp is not None else self.amp
                                }, os.path.join(self.model_path, "task_test_checkpoint_%d.pth.tar" % self.model_no))
                except Exception as e:
                    print(e)
                    continue

        logger.info("Finished Training!")


# Graph rel filter dump
def get_l4toparent(ontology_file, most_granular):
    '''
    input : ontology file path
    output : returns the dictionary with tags rolled up to their parents, from most granular to least granular
    '''
    onto_obj = Ontology(ontology_file, most_granular)
    onto=onto_obj.read_onto_file()
    # Generate the Mappings
    l4toparent = onto_obj.get_l4toParent_mapping(onto)
    return l4toparent

def get_rel_topic_filter_file(rel_train_data_path,rel_filter_save_path, ontoloy_file, most_granular):
    '''
    input : takes in the path of the training data for relation model
    output : saves the relation topic filter helper file for use in the pipeline
    '''
    reData = pd.read_csv(rel_train_data_path, compression='gzip')
    
    l4toparent = get_l4toparent(ontoloy_file, most_granular)
    
    reData['topicTypeName1'] = reData['entity_label_1'].apply(lambda x : l4toparent[x] if x in l4toparent.keys() else None )
    reData['topicTypeName2'] = reData['entity_label_2'].apply(lambda x : l4toparent[x] if x in l4toparent.keys() else None )
    reData = reData[reData.link!='Other']

    df=reData[['topicTypeName1', 'topicTypeName2','text']].groupby(['topicTypeName1', 'topicTypeName2']).count().reset_index().sort_values(by='text',ascending=False)
    df = reData.groupby(['topicTypeName1', 'topicTypeName2']).size().rename('count').reset_index()
    df = df.sort_values('count', ascending=False).reset_index()
    df = df[['topicTypeName1', 'topicTypeName2','count']]

    validator={ }
    for i,j,k in df.values:
        if j+'_'+i in validator:
            if i+'_'+j not in validator:
                continue
            if abs(k - validator[i+'_'+j]) > 100 and abs(k - validator[i+'_'+j]) < 2:
                del validator[i+'_'+j]
                validator[j+'_'+i]=k

        else:      
            validator[i+'_'+j]=k
    df['flag']=df.apply(lambda x: 1 if x['topicTypeName1']+'_'+x['topicTypeName2'] in validator  else 0,axis=1)
    joblib.dump(list(df[df['flag'] == 1].apply(lambda x: x['topicTypeName1']+'_'+x['topicTypeName2'],axis=1)), rel_filter_save_path)

def get_performance_summary(train, test):
    '''
    input : takes in the inference output on test data and train data for support calculation
    output : generates the performance summary of the model
    '''    
    cr=classification_report(test.relations, test['pred_link'],output_dict=True)

    cr=pd.DataFrame(cr).T
    cr = cr.merge(train.relations.value_counts(),left_index=True,right_index=True, how='left')
    cr = cr.reset_index()
    cr.rename(columns={'support': 'test_support', 'relations': 'train_support', 'index': 'RE_class'}, inplace=True)
    cr['test_support'] = cr['test_support'].astype(int)
    # cr.to_csv(save_path+'/RE_performance_model_' + str(config['rel_model_no']) + '.csv', index=False)
    return cr
    
if __name__ == "__main__":
    trainer = RELTraining(model = 'bert-base-uncased',lower_case_flag = True, model_name = 'BERT', learning_rate=0.00007,max_length=50000,model_no=8,batch_size=8,gradient_acc_steps=2,max_norm=1,epochs=11,fp16=False,model_path="./rel_model/",amp = None)
    trainer.train(train_file, test_file, pretrained_model)