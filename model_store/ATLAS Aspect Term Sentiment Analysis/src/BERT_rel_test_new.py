import joblib
import torch
import os
import timeit
import time
from joblib import Parallel,delayed
import copy
import pandas as  pd
from src.BERT.modeling_bert import BertModel as Model


def load_state(net, optimizer, scheduler,model_no,model_path, load_best=False):
    print("loading...")
    base_path = model_path
    amp_checkpoint = None
    checkpoint_path = os.path.join(base_path,"task_test_checkpoint_%d.pth.tar" % model_no)
    best_path = os.path.join(base_path,"task_test_model_best_%d.pth.tar" % model_no)
#     print(checkpoint_path,best_path)
    start_epoch, best_pred, checkpoint = 0, 0, None
    if (load_best == True) and os.path.isfile(best_path):
        checkpoint = torch.load(best_path)
        print("Loaded best model.")
    elif os.path.isfile(checkpoint_path):
        checkpoint = torch.load(checkpoint_path)
        print("Loaded checkpoint model.")
    if checkpoint != None:
        start_epoch = checkpoint['epoch']
        best_pred = checkpoint['best_acc']
        net.load_state_dict(checkpoint['state_dict'])
        if optimizer is not None:
            optimizer.load_state_dict(checkpoint['optimizer'])
        if scheduler is not None:
            scheduler.load_state_dict(checkpoint['scheduler'])
        amp_checkpoint = checkpoint['amp']
        print("Loaded model and optimizer.")    
    return start_epoch, best_pred, amp_checkpoint

def load_state2(net, optimizer, scheduler,model_no,model_path, load_best=False):
    print("loading...")
    base_path = model_path
    amp_checkpoint = None
    checkpoint_path = os.path.join(base_path,"task_test_checkpoint_%d.pth.tar" % model_no)
    best_path = os.path.join(base_path,"task_test_model_best_%d.pth.tar" % model_no)
#     print(checkpoint_path,best_path)
    start_epoch, best_pred, checkpoint = 0, 0, None
    if (load_best == True) and os.path.isfile(best_path):
        checkpoint = torch.load(best_path)
        print("Loaded best model.")
    elif os.path.isfile(checkpoint_path):
        checkpoint = torch.load(checkpoint_path)
        print("Loaded checkpoint model.")
    if checkpoint != None:
        start_epoch = checkpoint['epoch']
        best_pred = checkpoint['best_acc']
        net.load_state_dict(checkpoint['state_dict'])
        if optimizer is not None:
            optimizer.load_state_dict(checkpoint['optimizer'])
        if scheduler is not None:
            scheduler.load_state_dict(checkpoint['scheduler'])
        amp_checkpoint = checkpoint['amp']
        print("Loaded model and optimizer.")    
    return start_epoch, best_pred, amp_checkpoint

class Relations_Mapper(object):
    '''Maps relation to class and creates dictionary object to access relation by indexand vise versa'''
    def __init__(self, relations):
        self.rel2idx = {}
        self.idx2rel = {}
        
        print("Mapping relations to IDs...")
        self.n_classes = 0
        for relation in tqdm(relations):
            if relation not in self.rel2idx.keys():
                self.rel2idx[relation] = self.n_classes
                self.n_classes += 1
        
        for key, value in self.rel2idx.items():
            self.idx2rel[value] = key



class infer_from_trained(object):
    """ Loads saved model and optimizer states if exists 
    
        :ivar net: Network
        :ivar Optimizer: Tokenizer
        :ivarscheduler: 
        :ivar model_no: 
        :ivar model_path:
        :ivar load_best:
        """
    def __init__(self, class_info,tokenizer_path,model_path,pretrained_path,logger,model_no=1,best=True,cuda_dis=True,model_size='bert-base-uncased'):
        try:
            self.cuda = torch.cuda.is_available() if cuda_dis else False
            self.rm=joblib.load(class_info)
            self.logger = logger
            from src.BERT.modeling_bert import BertModel as Model
            model = model_size #'bert-base-uncased'
            lower_case = True
            model_name = 'BERT'
            self.net = Model.from_pretrained(pretrained_path, force_download=False, \
                                             model_size=model_size,\
                                             task='classification', n_classes_=self.rm.n_classes)

            self.tokenizer = joblib.load(tokenizer_path)
            self.net.resize_token_embeddings(len(self.tokenizer))
            
            if self.cuda:
                self.net.cuda()
            start_epoch, best_pred, amp_checkpoint = load_state2(self.net, None, None, model_no,model_path, load_best=best)
            logger.info("Loading bert model complete")
            self.e1_id = self.tokenizer.convert_tokens_to_ids('[E1]')
            self.e2_id = self.tokenizer.convert_tokens_to_ids('[E2]')
            self.pad_id = self.tokenizer.pad_token_id
            self.net.eval()
            logger.info("Loading bert model complete")
        except Exception as e:
            logger.error("Failed to load bert model",exc_info=True)
        
        
    
    def get_e1e2_start(self, x):
        e1_e2_start = ([i for i, e in enumerate(x) if e == self.e1_id][0],\
                        [i for i, e in enumerate(x) if e == self.e2_id][0])
        return e1_e2_start
    
    def infer_one_sentence(self, sentence):
        tokenized = self.tokenizer.encode(sentence); 
        
    
        e1_e2_start = self.get_e1e2_start(tokenized); 
        
        
        tokenized = torch.LongTensor(tokenized).unsqueeze(0) ;
        
        
        e1_e2_start = torch.LongTensor(e1_e2_start).unsqueeze(0);
        
        
        attention_mask = (tokenized != self.pad_id).float();
        
        
        token_type_ids = torch.zeros((tokenized.shape[0], tokenized.shape[1])).long() ;
        
        
        if self.cuda:
            tokenized = tokenized.cuda()
            attention_mask = attention_mask.cuda()
            token_type_ids = token_type_ids.cuda()
        
        
        with torch.no_grad():
            classification_logits = self.net(tokenized, token_type_ids=token_type_ids, attention_mask=attention_mask, Q=None,\
                                        e1_e2_start=e1_e2_start)
            predicted = torch.softmax(classification_logits, dim=1)
        
#         start_time = time.time() #time calc
#         li=[]
#         aa=predicted.shape[1]
#         di=dict()
#         for xx in range(aa):
# #             print((self.rm.idx2rel[xx],predicted[0][xx].item()))
#             di[self.rm.idx2rel[xx].strip('\n')]=predicted[0][xx].item()
# #             li.append((self.rm.idx2rel[xx].strip('\n'),predicted[0][xx].item()))
#         return di
    
        
    
        return self.rm.idx2rel[predicted.max(1)[1].item()].strip('\n'),predicted.max(1)[0].item()
    
    def infer_sentence(self, sentence):
        '''  
        self.infer_one_sentence(sentence) 
        Args:
            sentence: String which is used to get predictions
        Returns:
            Predictions for given sentence
        '''
        return self.infer_one_sentence(sentence)
    
    def timeit_infersentence(self,sentence):
        timeit_complete = '''  
        self.infer_one_sentence(sentence) 
        Args:
            sentence: String which is used to get predictions
        '''
        print (timeit.timeit(stmt = timeit_complete, 
                     number = 10000)) 
        

        

def get_rel_link(inferer,graph_data,logger):
    ''' 
    Bert Relation Extraction model used to predict relation present between two entities given review text, entity1 and entity2
    Args:
        inferer: Bert relation extraction model
        graph_data: pandas dataframe with concept and concept type information
    Returns:
        Pandas dataframe with relation between entities
    '''
#     try:
    graph_data['all_links']=graph_data['ann_content'].apply(inferer.infer_sentence)

    graph_data['link']=graph_data['all_links'].apply(lambda x: max(x, key=x.get))

    graph_data['score']=graph_data['all_links'].apply(lambda x: max(x.values()))
    graph_data=graph_data.drop(['inbetween_count', 'sent_len'],axis=1)
    logger.info("Relation extraction successful")
    return graph_data
#     except Exception as e:
#         logger.error("Relation extraction failed", exc_info = True)


def parallel(data,function,number_cuts=6,batchsize=20071):
    return pd.concat(Parallel(n_jobs=-1,max_nbytes='1M')(delayed(function)
                                 (copy.deepcopy(data.iloc[i*batchsize:i*batchsize+batchsize, ])) for i in (range(0, number_cuts))))


 
    
def take_window(content):
    content=content.replace("!",".")
    sent_list = content.split('.')
    idx_e1=idx_e2=-1
    for i, sen in enumerate(sent_list):
        if '[E1]' in sen:
            idx_e1=i
            if "[E2]" in sen:
                idx_e2=i
            break
    if idx_e2==-1: 
        for i, sen in enumerate(sent_list):
            if '[E2]' in sen:
                idx_e2=i
                if "[E1]" in sen:
                    idx_e1=i
                break
    idx_min=min(idx_e1,idx_e2)
    idx_max=max(idx_e1,idx_e2)
    # aspect found in first sentence:
    if idx_min==0:
        if len(sent_list)==1:
            return '.'.join(sent_list)
        else:
            return '.'.join(sent_list[0:idx_max+2])
    # if aspect found in last sentence:
    elif idx_max==len(sent_list)-1:
        return '.'.join(sent_list[idx_min:])
    else:
        return '.'.join(sent_list[idx_min-1:idx_max+2])
    


def get_parallel_relation(model_num,model_path,pretrained_path,graph_data,graph_data_path,logger,number_cuts):
    '''
    Input: model_num which is the model number saved,graph_data, logger
    Output: inferred graph_data
    '''
    print("Starting inference...")
    def parallel_infer(graph_data):
        inferer=infer_from_trained(model_path+'/class_info_'+str(model_num)+'.dmp',model_path+'/tokenizer_'+str(model_num)+'.dmp',model_path,pretrained_path,logger,best=True,model_no=model_num)
        graph_data['ann_content']=graph_data['ann_content'].apply(take_window)
        graph_data = graph_data[graph_data.ann_content.apply(len) < 1500 ]
        graph_data['all_links']=graph_data['ann_content'].apply(lambda x: inferer.infer_one_sentence(x))
        graph_data['link']=graph_data['all_links'].apply(lambda x: x[0])
        graph_data['score']=graph_data['all_links'].apply(lambda x: x[1])
        return graph_data
        
        return graph_data

    graph_data_infer=parallel(graph_data,parallel_infer,number_cuts,int(len(graph_data)/number_cuts)+1)
    print("Done. Saving data where link is not 'Other'...")
    # graph_data_infer = graph_data_infer[graph_data_infer.link!='Other']
    # graph_data_infer.to_csv(graph_data_path,index=False,compression='gzip')
    
    return graph_data_infer


def get_parallel_relation_cuda(model_num,model_path,pretrained_path,graph_data,graph_data_path,logger,number_cuts):
    '''
    Input: model_num which is the model number saved,graph_data, logger
    Output: inferred graph_data
    '''
    print("Starting inference...")
    def parallel_infer(graph_data):
        inferer=infer_from_trained('model/class_info_'+str(model_num)+'.dmp','model/tokenizer_'+str(model_num)+'.dmp',model_path,pretrained_path,logger,best=True,model_no=model_num)
        graph_data['ann_content_old'] = graph_data['ann_content'].copy()
        graph_data['ann_content']=graph_data['ann_content_old'].apply(take_window)
        graph_data.drop(['ann_content_old'],axis=1,inplace=True)
        graph_data = graph_data[graph_data.ann_content.apply(len)<1500]
        graph_data['all_links']=graph_data['ann_content'].apply(lambda x: inferer.infer_one_sentence(x))
        graph_data['link']=graph_data['all_links'].apply(lambda x: max(x, key=x.get))
        graph_data['score']=graph_data['all_links'].apply(lambda x: max(x.values()))
        
        return graph_data

#     graph_data_infer=parallel(graph_data,parallel_infer,number_cuts,int(len(graph_data)/number_cuts)+1)
    graph_data_infer=parallel_infer(graph_data)
    print("Done. Saving data where link is not 'Other'...")
    graph_data_infer = graph_data_infer[graph_data_infer.link!='Other']
    return graph_data_infer

