import numpy as np
import joblib
from src.validation import compute_f1
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import TimeDistributed, Conv1D, Dense, Embedding, Input, Dropout, LSTM, Bidirectional, MaxPooling1D, Flatten, concatenate
from src.prepro import readfile, createBatches, createMatrices, iterate_minibatches, addCharInformation, padding, createEqualBatches
from tensorflow.keras.utils import plot_model
from tensorflow.keras.initializers import RandomUniform
from tensorflow.keras.optimizers import SGD, Nadam
# from IPython.display import display

from tensorflow.keras.models import load_model
import tensorflow as tf
from tensorflow.keras.optimizers import SGD, Nadam
import pandas as pd
from unidecode import unidecode

import keras.backend.tensorflow_backend
import tensorflow as tf

from keras.backend import clear_session


import warnings
warnings.filterwarnings(action='ignore')

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Mute if want to use GPU
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import tensorflow as tf
import logging
tf.get_logger().setLevel(logging.ERROR)


def getCasing(word, caseLookup):
    casing = 'other'

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit():  # Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainly_numeric'
    elif word.islower():  # All lower case
        casing = 'allLower'
    elif word.isupper():  # All upper case
        casing = 'allUpper'
    elif word[0].isupper():  # is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'contains_digit'

    return caseLookup[casing]

def createMatrices(sentences, word2Idx, label2Idx, case2Idx, char2Idx):
    unknownIdx = word2Idx['UNKNOWN_TOKEN']
    paddingIdx = word2Idx['PADDING_TOKEN']

    dataset = []

    wordCount = 0
    unknownWordCount = 0

    for sentence in sentences:
        wordIndices = []
        caseIndices = []
        charIndices = []
        labelIndices = []

        for word, char, label in sentence:
            wordCount += 1
            if word in word2Idx:
                wordIdx = word2Idx[word]
            elif word.lower() in word2Idx:
                wordIdx = word2Idx[word.lower()]
            else:
                wordIdx = unknownIdx
                unknownWordCount += 1
            charIdx = []
            for x in char:
                x=str(x).replace('\n',' ')
                charIdx.append(char2Idx[x])
            # Get the label and map to int
            wordIndices.append(wordIdx)
            caseIndices.append(getCasing(word, case2Idx))
            charIndices.append(charIdx)
            labelIndices.append(label2Idx[label])

        dataset.append([wordIndices, caseIndices, charIndices, labelIndices])

    return dataset

class CNN_BLSTM(object):
    """
    
    """
    def __init__(self,TRAIN,TEST, EPOCHS, DROPOUT, DROPOUT_RECURRENT, LSTM_STATE_SIZE, CONV_SIZE, LEARNING_RATE, OPTIMIZER):
        
        self.epochs = EPOCHS
        self.dropout = DROPOUT
        self.dropout_recurrent = DROPOUT_RECURRENT
        self.lstm_state_size = LSTM_STATE_SIZE
        self.conv_size = CONV_SIZE
        self.learning_rate = LEARNING_RATE
        self.optimizer = OPTIMIZER
        self.wv_model = None
        self.temp_entities = TRAIN
        self.temp_entities_test = TEST

    def loadData(self):
        """Load data and add character information"""
#         train=temp_entities.sample(frac=0.9,random_state=2000000)
        self.trainSentences = self.temp_entities
        self.devSentences = self.temp_entities_test
        self.testSentences = self.temp_entities_test

    def addCharInfo(self):
        # format: [['EU', ['E', 'U'], 'B-ORG\n'], ...]
        ## self.trainSentences = addCharInformation(self.trainSentences)
        self.trainSentences = parallel(self.trainSentences,addCharInformation,16,int(len(self.trainSentences)/16)+1)
        #print("Training data conversion done.")
        ## self.devSentences = addCharInformation(self.devSentences)
        self.devSentences = parallel(self.devSentences,addCharInformation,16,int(len(self.devSentences)/16)+1)
        #print("Eval data conversion done.")
        ## self.testSentences = addCharInformation(self.testSentences)
        self.testSentences = parallel(self.testSentences,addCharInformation,16,int(len(self.testSentences)/16)+1)
        #print("Test data conversion done.")

    def embed(self):
        """Create word- and character-level embeddings"""

        labelSet = set()
        words = {}
        # unique words and labels in data  
        for dataset in [self.trainSentences, self.devSentences, self.testSentences]:
            for sentence in dataset:
                try:
                    for token, char, label in sentence:
                        # token ... token, char ... list of chars, label ... BIO labels   
                        labelSet.add(label)
                        words[token.lower()] = True
                except Exception as e:
                    print(sentence)
           
#         #display(labelSet)
#         #display(words)
        # mapping for labels
        self.label2Idx = {}
        for label in labelSet:
            self.label2Idx[label] = len(self.label2Idx)
#         #display(self.label2Idx)
        # mapping for token cases
        case2Idx = {'numeric': 0, 'allLower': 1, 'allUpper': 2, 'initialUpper': 3, 'other': 4, 'mainly_numeric': 5,
                    'contains_digit': 6, 'PADDING_TOKEN': 7}
        self.caseEmbeddings = np.identity(len(case2Idx), dtype='float32')  # identity matrix used 
#         #display(self.caseEmbeddings)
        # read GLoVE word embeddings
        word2Idx = {}
        self.wordEmbeddings = []
        
        fEmbeddings = open("models/ulm_embed.txt", encoding="utf-8")

        # loop through each word in embeddings
        for line in fEmbeddings:
            split = line.strip().split(" ")
            
            word = split[0]  # embedding word entry
            
#             print(word)

            if len(word2Idx) == 0:  # add padding+unknown
                word2Idx["PADDING_TOKEN"] = len(word2Idx)
                #display(len(split))
                vector = np.zeros(len(split) - 1)  # zero vector for 'PADDING' word
                self.wordEmbeddings.append(vector)
                #display(vector.shape)
                word2Idx["UNKNOWN_TOKEN"] = len(word2Idx)
                vector = np.random.uniform(-0.25, 0.25, len(split) - 1)
                self.wordEmbeddings.append(vector)
                #display(vector.shape)
            if split[0].lower() in words:
                vector = np.array([float(num) for num in split[1:]])
                self.wordEmbeddings.append(vector)  # word embedding vector
                word2Idx[split[0]] = len(word2Idx)  # corresponding word dict
        #display(word2Idx)
        self.wordEmbeddings = np.array(self.wordEmbeddings)
        #display(self.wordEmbeddings)
        # dictionary of all possible characters
        self.char2Idx = {"PADDING": 0, "UNKNOWN": 1}
        for c in " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.,-_()[]{}!?:;#'\"/\\%$`&=*+@^~|<>":
            self.char2Idx[c] = len(self.char2Idx)
        self.word2Id=word2Idx
        self.case2Id=case2Idx
   
        # format: [[wordindices], [caseindices], [padded word indices], [label indices]]
        self.train_set = padding(createMatrices(self.trainSentences, word2Idx, self.label2Idx, case2Idx, self.char2Idx))
        self.dev_set = padding(createMatrices(self.devSentences, word2Idx, self.label2Idx, case2Idx, self.char2Idx))
        self.test_set = padding(createMatrices(self.testSentences, word2Idx, self.label2Idx, case2Idx, self.char2Idx))

        #print('done')
        self.idx2Label = {v: k for k, v in self.label2Idx.items()}
        
    def createBatches(self):
        """Create batches"""
        self.train_batch, self.train_batch_len = createBatches(self.train_set)
        self.dev_batch, self.dev_batch_len = createBatches(self.dev_set)
        self.test_batch, self.test_batch_len = createBatches(self.test_set)
        
    def tag_dataset(self, dataset, model):
        """Tag data with numerical values"""
        correctLabels = []
        predLabels = []
        for i, data in enumerate(dataset):
            tokens, casing, char, labels = data
            tokens = np.asarray([tokens])
            casing = np.asarray([casing])
            char = np.asarray([char])
            pred = model.predict([tokens, casing, char], verbose=False)[0]
            pred = pred.argmax(axis=-1)  # Predict the classes
            correctLabels.append(labels)
            predLabels.append(pred)
        return predLabels, correctLabels
    
    def buildModel(self):
        """Model layers"""

        # character input
        character_input = Input(shape=(None, 52,), name="Character_input")
        embed_char_out = TimeDistributed(
            Embedding(len(self.char2Idx), 30, embeddings_initializer=RandomUniform(minval=-0.5, maxval=0.5)), name="Character_embedding")(
            character_input)

        dropout = Dropout(self.dropout)(embed_char_out)

        # CNN
        conv1d_out = TimeDistributed(Conv1D(kernel_size=self.conv_size, filters=30, padding='same', activation='tanh', strides=1), name="Convolution")(dropout)
        maxpool_out = TimeDistributed(MaxPooling1D(52), name="Maxpool")(conv1d_out)
        char = TimeDistributed(Flatten(), name="Flatten")(maxpool_out)
        char = Dropout(self.dropout)(char)

        # word-level input
        words_input = Input(shape=(None,), dtype='int32', name='words_input')
        words = Embedding(input_dim=self.wordEmbeddings.shape[0], output_dim=self.wordEmbeddings.shape[1], weights=[self.wordEmbeddings],
                          trainable=False)(words_input)

        # case-info input
        casing_input = Input(shape=(None,), dtype='int32', name='casing_input')
        casing = Embedding(output_dim=self.caseEmbeddings.shape[1], input_dim=self.caseEmbeddings.shape[0], weights=[self.caseEmbeddings],
                           trainable=False)(casing_input)

        # concat & BLSTM
        output = concatenate([words, casing, char])
        output = Bidirectional(LSTM(self.lstm_state_size, 
                                    return_sequences=True, 
                                    dropout=self.dropout,                        # on input to each LSTM block
                                    recurrent_dropout=self.dropout_recurrent     # on recurrent input signal
                                   ), name="BLSTM")(output)
        output = TimeDistributed(Dense(len(self.label2Idx), activation='softmax'),name="Softmax_layer")(output)

        # set up model
        self.model = Model(inputs=[words_input, casing_input, character_input], outputs=[output])
        
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer=self.optimizer)
        
        self.init_weights = self.model.get_weights()
        
        #plot_model(self.model, to_file='model.png')
        
        #print("Model built. Saved model.png\n")
    
    def inferbuildModel(self,model_path,char2Idx,wordEmbeddings,caseEmbeddings,label2Idx,word2Id,case2Id,idx2Label):
        """Model layers"""
        graph1 = tf.Graph()
        with graph1.as_default():
            session1 = tf.Session()
            with session1.as_default():
        
                self.char2Idx=char2Idx
                self.wordEmbeddings=wordEmbeddings
                self.caseEmbeddings=caseEmbeddings
                self.label2Idx=label2Idx
                self.word2Id=word2Id
                self.case2Id=case2Id
                self.idx2Label=idx2Label
                self.model_path=model_path
                
                # character input
                character_input = Input(shape=(None, 52,), name="Character_input")
                embed_char_out = TimeDistributed(
                    Embedding(len(self.char2Idx), 30, embeddings_initializer=RandomUniform(minval=-0.5, maxval=0.5)), name="Character_embedding")(
                    character_input)

                dropout = Dropout(self.dropout)(embed_char_out)

                # CNN
                conv1d_out = TimeDistributed(Conv1D(kernel_size=self.conv_size, filters=30, padding='same', activation='tanh', strides=1), name="Convolution")(dropout)
                maxpool_out = TimeDistributed(MaxPooling1D(52), name="Maxpool")(conv1d_out)
                char = TimeDistributed(Flatten(), name="Flatten")(maxpool_out)
                char = Dropout(self.dropout)(char)

                # word-level input
                words_input = Input(shape=(None,), dtype='int32', name='words_input')
                words = Embedding(input_dim=self.wordEmbeddings.shape[0], output_dim=self.wordEmbeddings.shape[1], weights=[self.wordEmbeddings],
                                  trainable=False)(words_input)

                # case-info input
                casing_input = Input(shape=(None,), dtype='int32', name='casing_input')
                casing = Embedding(output_dim=self.caseEmbeddings.shape[1], input_dim=self.caseEmbeddings.shape[0], weights=[self.caseEmbeddings],
                                   trainable=False)(casing_input)

                # concat & BLSTM
                output = concatenate([words, casing, char])
                output = Bidirectional(LSTM(self.lstm_state_size, 
                                            return_sequences=True, 
                                            dropout=self.dropout,                        # on input to each LSTM block
                                            recurrent_dropout=self.dropout_recurrent     # on recurrent input signal
                                           ), name="BLSTM")(output)
                output = TimeDistributed(Dense(len(self.label2Idx), activation='softmax'),name="Softmax_layer")(output)

                # set up model
                self.model = Model(inputs=[words_input, casing_input, character_input], outputs=[output])
                # self.model=load_model(model_path)
                self.model.compile(loss='sparse_categorical_crossentropy', optimizer=self.optimizer)

                self.init_weights = self.model.get_weights()
        self.model=load_model(self.model_path)
        
#         plot_model(self.model, to_file='model.png')
        
        #print("Model built. Saved model.png\n")

    def infer_loadData(self,text):
        self.predsentences=[[[i,'O'] for i in txt.split()] for txt in text]

    def infer_addCharInfo(self):
        self.predsentences = addCharInformation(self.predsentences)


    def infer_embed(self):
        """Create word- and character-level embeddings"""
        #display(self.predsentences)
        # format: [[wordindices], [caseindices], [padded word indices], [label indices]]
        self.pred_set= padding(createMatrices(self.predsentences, self.word2Id, self.label2Idx, self.case2Id, self.char2Idx))
        ##display(self.train_set)
        self.idx2Label = {v: k for k, v in self.label2Idx.items()}
        #display(self.idx2Label) 


    def infer_createBatches(self):
        """Create batches"""
        self.pred_batch, self.pred_batch_len = createBatches(self.pred_set)

    def infer_tag_dataset(self, dataset, model):
        """Tag data with numerical values"""
        correctLabels = []
        predLabels = []
        token_arr = []
        predscores=[]
        for i, data in enumerate(dataset):
            tokens, casing, char, labels = data
            tokens = np.asarray([tokens])
            casing = np.asarray([casing])
            char = np.asarray([char])
            pred = model.predict([tokens, casing, char], verbose=False)[0]
            predscores.append(pred)
            pred = pred.argmax(axis=-1)  # Predict the classes
            token_arr.append(tokens)
            predLabels.append(pred)
#         clear_session()
#         if keras.backend.tensorflow_backend._SESSION:
#             tf.reset_default_graph()
#             keras.backend.tensorflow_backend._SESSION.close()
#             keras.backend.tensorflow_backend._SESSION = None
        return token_arr,predLabels,predscores

    def infer(self,text):  
        '''Infer text'''
        self.infer_loadData(text)
        self.infer_addCharInfo()
        self.infer_embed()
        #self.infer_createBatches()
        return [txt.split() for txt in text],self.infer_tag_dataset(self.pred_set, self.model)
    

    def get_pred(self,numb1,numb2):
        pred_data=readfiles('test.tsv')[numb1:numb2]
        sentences=[]
        for i in pred_data:
            sentences.append(' '.join([j[0] for j in i]))
        entities=[]
        entitiy=[]
        for i in pred_data:
            entitiy=[]
            for j in i:
                if j[1] !='O':
                    entitiy.append(j)

            entities.append(entitiy)

#         for i in range(len(entities)):
            #print(sentences[i])
            #print('\n')
            #print(entities[i])
            #print('\n')

        
    def train(self,model_path):
        """Default training"""

#         mod_=load_model(model_path)
#         self.init_weights = mod_.get_weights()

        self.f1_test_history = []
        self.f1_dev_history = []

        for epoch in range(self.epochs):    
            print("Epoch {}/{}".format(epoch, self.epochs))
            print(self.train_batch_len)
            batches_size=50
            batch_up=0
            for batch_counter in range(batches_size,len(self.train_batch_len),batches_size):
                batched, batched_cc = createBatches(self.train_set[batch_up:batch_counter])
                for i,batch in enumerate(iterate_minibatches(batched,batched_cc)):
#                     print(i)
#                     print(len(batch))
                    labels, tokens, casing,char = batch       
                    self.model.train_on_batch([tokens, casing,char], labels)

            # compute F1 scores
            predLabels, correctLabels = self.tag_dataset(self.test_batch, self.model)
            pre_test, rec_test, f1_test = compute_f1(predLabels, correctLabels, self.idx2Label)
            self.f1_test_history.append(f1_test)
            #print("f1 test ", round(f1_test, 4))

            predLabels, correctLabels = self.tag_dataset(self.dev_batch, self.model)
            pre_dev, rec_dev, f1_dev = compute_f1(predLabels, correctLabels, self.idx2Label)
            self.f1_dev_history.append(f1_dev)
            print("f1 dev ", round(f1_dev, 4), "\n")
            
        
            
        print("Final F1 test score: ", f1_test)
            
        print("Training finished.")
            
        # save model
        
        self.model.save(model_path)
#         print("Model weights saved.")
        
        # self.model.set_weights(self.init_weights)  # clear model
#         print("Model weights cleared.")

    def writeToFile(self):
        """Write output to file"""

        # .txt file format
        # [epoch  ]
        # [f1_test]
        # [f1_dev ]
        
        output = np.matrix([[int(i) for i in range(self.epochs)], self.f1_test_history, self.f1_dev_history])

        fileName = self.modelName + ".txt"
        with open(fileName,'wb') as f:
            for line in output:
                np.savetxt(f, line, fmt='%.5f')
                
        #print("Model performance written to file.")

    #print("Class initialised.")
    
def addCharInformation_s(sentence):
    out=[]
    for i in sentence:

        out.append([i[0],[c for c in i[0]],i[1]])
    return out

def load_ner_model():
    '''Loads pretrained model trained on ner data'''
    EPOCHS = 50               # paper: 80
    DROPOUT = 0.5             # paper: 0.68
    DROPOUT_RECURRENT = 0.25  # not specified in paper, 0.25 recommended
    LSTM_STATE_SIZE = 200     # paper: 275
    CONV_SIZE = 3             # paper: 3
    LEARNING_RATE = 0.0105    # paper 0.0105
    OPTIMIZER = Nadam()       # paper uses SGD(lr=self.learning_rate), Nadam() recommended

    cnn_blstm = CNN_BLSTM(None,None,EPOCHS, DROPOUT, DROPOUT_RECURRENT, LSTM_STATE_SIZE, CONV_SIZE, LEARNING_RATE, OPTIMIZER)
    
    char2Idx=joblib.load('model/char2Idx_l4_prod')
    wordEmbeddings=joblib.load('model/wordEmbeddings_l4_prod')
    caseEmbeddings=joblib.load('model/caseEmbeddings_l4_prod')
    label2Idx=joblib.load('model/label2Idx_l4_prod')
    word2Id=joblib.load('model/word2Id_l4_prod')
    case2Id=joblib.load('model/case2Id_l4_prod')
    idx2Label=joblib.load('model/idx2Label_l4_prod')
    model_path='model/initial_model.h5'

    cnn_blstm.inferbuildModel(model_path,char2Idx,wordEmbeddings,caseEmbeddings,label2Idx,word2Id,case2Id,idx2Label)

    return cnn_blstm
    
def train_ner_model(train_data):
    '''Trains model on ner data '''
    EPOCHS = 15               # paper: 80
    DROPOUT = 0.5             # paper: 0.68
    DROPOUT_RECURRENT = 0.25  # not specified in paper, 0.25 recommended
    LSTM_STATE_SIZE = 200     # paper: 275
    CONV_SIZE = 3             # paper: 3
    LEARNING_RATE = 0.0105    # paper 0.0105
    OPTIMIZER = Nadam()       # paper uses SGD(lr=self.learning_rate), Nadam() recommended
    model_path='model/initial_model.h5'

    test=joblib.load('data/test.dmp')
    train_init=joblib.load('data/train.dmp')

    train=pd.DataFrame(train_data)
    training_data=joblib.load('training_data.dmp')
    
    train['ground']=train.apply(lambda x: list(zip(x['text'],x['tags'])),axis=1)
    train['ground']=train['ground'].apply(lambda x: addCharInformation_s(x))

    training_data.append(train)
    train=pd.concat(training_data)
    joblib.dump(training_data,'training_data.dmp')

    if len(training_data)%50 == 0:

        cnn_blstm = CNN_BLSTM(pd.concat([train_init,train['ground']]),test,EPOCHS, DROPOUT, DROPOUT_RECURRENT, LSTM_STATE_SIZE, CONV_SIZE, LEARNING_RATE, OPTIMIZER)
        
        print('initiated')
        cnn_blstm.loadData()
        cnn_blstm.embed()
        cnn_blstm.createBatches()
        print('linked')
        cnn_blstm.buildModel()
        print('builded')
        cnn_blstm.train(model_path)

        joblib.dump(cnn_blstm.char2Idx,'model/char2Idx_l4_prod')
        joblib.dump(cnn_blstm.wordEmbeddings,'model/wordEmbeddings_l4_prod')
        joblib.dump(cnn_blstm.caseEmbeddings,'model/caseEmbeddings_l4_prod')
        joblib.dump(cnn_blstm.label2Idx,'model/label2Idx_l4_prod')
        joblib.dump(cnn_blstm.word2Id,'model/word2Id_l4_prod')
        joblib.dump(cnn_blstm.case2Id,'model/case2Id_l4_prod')
        joblib.dump(cnn_blstm.idx2Label,'model/idx2Label_l4_prod')
        print('done!')
    

def train_ulmfit_cnn_bilstm_model(train,test,prefix='',path='ner_model/'):
    '''Trains ulmfit cnn model on given train and test data'''
    EPOCHS = 20               # paper: 80
    DROPOUT = 0.5             # paper: 0.68
    DROPOUT_RECURRENT = 0.25  # not specified in paper, 0.25 recommended
    LSTM_STATE_SIZE = 200     # paper: 275
    CONV_SIZE = 3             # paper: 3
    LEARNING_RATE = 0.1    # paper 0.0105
    OPTIMIZER = Nadam()       # paper uses SGD(lr=self.learning_rate), Nadam() recommended
    model_path=path+'initial_model_'+prefix+'.h5'
    train['token']=train['token'].astype(str).apply(lambda x: [unidecode(i) if unidecode(i) != '' else ' ' for i in eval(x)])
    test['token']=test['token'].astype(str).apply(lambda x: [unidecode(i) if unidecode(i) != '' else ' ' for i in eval(x)])
#     train['ground']=train['ground'].apply(eval)
    train['ground']=train.apply(lambda x: list(zip(x['token'],x['ground'])),axis=1)
    train['ground']=train['ground'].apply(lambda x: addCharInformation_s(x))
#     test['ground']=test['ground'].apply(eval)
    test['ground']=test.apply(lambda x: list(zip(x['token'],x['ground'])),axis=1)
    test['ground']=test['ground'].apply(lambda x: addCharInformation_s(x))
    
    cnn_blstm = CNN_BLSTM(train['ground'],test['ground'],EPOCHS, DROPOUT, DROPOUT_RECURRENT, LSTM_STATE_SIZE, CONV_SIZE, LEARNING_RATE, OPTIMIZER)
    
    print('initiated')
    cnn_blstm.loadData()
    cnn_blstm.embed()
    cnn_blstm.createBatches()
    print('linked')
    cnn_blstm.buildModel()
    print('builded')
    
    cnn_blstm.train(model_path)
    
    joblib.dump(cnn_blstm.char2Idx,path+'char2Idx_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.wordEmbeddings,path+'wordEmbeddings_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.caseEmbeddings,path+'caseEmbeddings_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.label2Idx,path+'label2Idx_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.word2Id,path+'word2Id_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.case2Id,path+'case2Id_l4_prod_'+prefix)
    joblib.dump(cnn_blstm.idx2Label,path+'idx2Label_l4_prod_'+prefix)
    print('done!')
    
def infer_ner_model(prefix='',model_path='model'):
    '''Return model inferred from pretrained'''
    EPOCHS = 30               # paper: 80
    DROPOUT = 0.5             # paper: 0.68
    DROPOUT_RECURRENT = 0.25  # not specified in paper, 0.25 recommended
    LSTM_STATE_SIZE = 200     # paper: 275
    CONV_SIZE = 3             # paper: 3
    LEARNING_RATE = 0.1    # paper 0.0105
    OPTIMIZER = Nadam()       # paper uses SGD(lr=self.learning_rate), Nadam() recommended
    cnn_blstm = CNN_BLSTM(None,None,EPOCHS, DROPOUT, DROPOUT_RECURRENT, LSTM_STATE_SIZE, CONV_SIZE, LEARNING_RATE, OPTIMIZER)

    char2Idx=joblib.load(model_path+'/char2Idx_l4_prod_'+prefix)
    wordEmbeddings=joblib.load(model_path+'/wordEmbeddings_l4_prod_'+prefix)
    caseEmbeddings=joblib.load(model_path+'/caseEmbeddings_l4_prod_'+prefix)
    label2Idx=joblib.load(model_path+'/label2Idx_l4_prod_'+prefix)
    word2Id=joblib.load(model_path+'/word2Id_l4_prod_'+prefix)
    case2Id=joblib.load(model_path+'/case2Id_l4_prod_'+prefix)
    idx2Label=joblib.load(model_path+'/idx2Label_l4_prod_'+prefix)
    model_path=model_path+'/initial_model_'+prefix+'.h5'
    
    cnn_blstm.inferbuildModel(model_path,char2Idx,wordEmbeddings,caseEmbeddings,label2Idx,word2Id,case2Id,idx2Label)
    return cnn_blstm
    