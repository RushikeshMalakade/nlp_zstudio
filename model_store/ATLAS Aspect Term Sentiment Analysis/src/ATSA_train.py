import os
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn.utils import clip_grad_norm_
import matplotlib.pyplot as plt
import time
import logging
import re
import pandas as pd
from tqdm.notebook import tqdm
from src.BERT.modeling_bert import BertModelABSAE1 as Model
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from src.BERT.tokenization_bert import BertTokenizer as Tokenizer
from src.tasks.train_funcs import load_state, load_results, evaluate_, evaluate_results_senti
import joblib
tqdm.pandas()
import spacy
nlp=spacy.load('en_core_web_sm', disable=['parser', 'tagger', 'ner'])
nlp.add_pipe(nlp.create_pipe('sentencizer'))

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', \
                    datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

class Relations_Mapper(object):
    def __init__(self, relations):
        self.rel2idx = {}
        self.idx2rel = {}
        
        logger.info("Mapping relations to IDs...")
        self.n_classes = 0
        for relation in tqdm(relations):
            if relation not in self.rel2idx.keys():
                self.rel2idx[relation] = self.n_classes
                self.n_classes += 1
        
        for key, value in self.rel2idx.items():
            self.idx2rel[value] = key

class Pad_Sequence():
    """
    collate_fn for dataloader to collate sequences of different lengths into a fixed length batch
    Returns padded x sequence, y sequence, x lengths and y lengths of batch
    """
    def __init__(self, seq_pad_value, label_pad_value=-1, label2_pad_value=-1,\
                 ):
        self.seq_pad_value = seq_pad_value
        self.label_pad_value = label_pad_value
        self.label2_pad_value = label2_pad_value
        
    def __call__(self, batch):
        sorted_batch = sorted(batch, key=lambda x: x[0].shape[0], reverse=True)
        seqs = [x[0] for x in sorted_batch]
        seqs_padded = pad_sequence(seqs, batch_first=True, padding_value=self.seq_pad_value)
        x_lengths = torch.LongTensor([len(x) for x in seqs])
        
        labels = list(map(lambda x: x[1], sorted_batch))
        labels_padded = pad_sequence(labels, batch_first=True, padding_value=self.label_pad_value)
        y_lengths = torch.LongTensor([len(x) for x in labels])
        
        labels2 = list(map(lambda x: x[2], sorted_batch))
        labels2_padded = pad_sequence(labels2, batch_first=True, padding_value=self.label2_pad_value)
        y2_lengths = torch.LongTensor([len(x) for x in labels2])
        
        return seqs_padded, labels_padded, labels2_padded, \
                x_lengths, y_lengths, y2_lengths

class proc_dataset(Dataset):
    
    def get_e1e1n_start(self,x, e1_id,e1n_id):
        try:
            e1_e1n_start = ([i for i, e in enumerate(x) if e == e1_id][0],\
                            [i for i, e in enumerate(x) if e == e1n_id][0])
        except Exception as e:
            e1_e1n_start = None
            logger.warn(e)
        return e1_e1n_start
    
    def __init__(self, df, tokenizer, e1_id, e1n_id):
        self.e1_id = e1_id
        self.e1n_id = e1n_id
        self.df = df
        logger.info("Tokenizing data...")
        self.df['input'] = self.df.progress_apply(lambda x: tokenizer.encode(x['input']),\
                                                             axis=1)
        
        self.df['e1_e1n_start'] = self.df.progress_apply(lambda x: self.get_e1e1n_start(x['input'],\
                                                       e1_id=self.e1_id, e1n_id=self.e1n_id), axis=1)
        print("\nInvalid rows/total: %d/%d" % (df['e1_e1n_start'].isnull().sum(), len(df)))
        self.df.dropna(axis=0, inplace=True)
    
    def __len__(self,):
        return len(self.df)
        
    def __getitem__(self, idx):
        return torch.LongTensor(self.df.iloc[idx]['input']),\
                torch.LongTensor(self.df.iloc[idx]['e1_e1n_start']),\
                torch.LongTensor([self.df.iloc[idx]['polarity_id']])
    
class ABSATraining(object):
    
    def __init__(self,model='bert-base-uncased',lower_case_flag= True, model_name= 'BERT', learning_rate=0.00007, max_length=50000, model_no=0, batch_size=8, 
                 gradient_acc_steps=2, max_norm=1, epochs=11, fp16=False, model_path='model/', amp= None):
        self.cuda = torch.cuda.is_available()
        self.model = model
        self.lower_case = lower_case_flag
        self.model_name = model_name
        self.learning_rate=learning_rate
        self.max_length=max_length

        self.model_no= model_no
        self.batch_size=batch_size
        self.gradient_acc_steps=gradient_acc_steps
        self.max_norm=max_norm
        self.epochs=epochs
        self.fp16=fp16
        self.model_path=model_path
        self.amp = amp
        

    
    def preprocess_data(self,train_data,test_data):
        '''
        Data preprocessing for SemEval2010 task 8 dataset
        '''
        
        df_train = train_data[[self.input_col,'polarity']].rename({self.input_col:'input'}, axis=1)
        df_test = test_data[[self.input_col,'polarity']].rename({self.input_col:'input'}, axis=1)
        
        rm = Relations_Mapper(df_train['polarity'])
        df_test['polarity_id'] = df_test.progress_apply(lambda x: rm.rel2idx[x['polarity']], axis=1)
        df_train['polarity_id'] = df_train.progress_apply(lambda x: rm.rel2idx[x['polarity']], axis=1)
        logger.info("Finished and saved!")

        return df_train, df_test, rm
    
    def load_state(self,net, optimizer, scheduler,model_no,model_path, load_best=False):
        """ Loads saved model and optimizer states if exists """
        base_path = model_path
        amp_checkpoint = None
        checkpoint_path = os.path.join(base_path,"test_checkpoint_%d.pth.tar" % model_no)
        best_path = os.path.join(base_path,"test_model_best_%d.pth.tar" % model_no)
        start_epoch, best_pred, checkpoint = 0, 0, None
        if (load_best == True) and os.path.isfile(best_path):
            checkpoint = torch.load(best_path)
            logger.info("Loaded best model.")
        elif os.path.isfile(checkpoint_path):
            checkpoint = torch.load(checkpoint_path)
            logger.info("Loaded checkpoint model.")
        if checkpoint != None:
            start_epoch = checkpoint['epoch']
            best_pred = checkpoint['best_acc']
            net.load_state_dict(checkpoint['state_dict'])
            if optimizer is not None:
                optimizer.load_state_dict(checkpoint['optimizer'])
            if scheduler is not None:
                scheduler.load_state_dict(checkpoint['scheduler'])
            amp_checkpoint = checkpoint['amp']
            logger.info("Loaded model and optimizer.")    
        return start_epoch, best_pred, amp_checkpoint
    
    def train(self, train_file, test_file, pretrained_model, input_col='input'):
        self.input_col = input_col
        df_train, df_test, rm = self.preprocess_data(train_file,test_file)
        net= Model.from_pretrained(pretrained_model,force_download=False,model_size=self.model,
                                        task='classification',n_classes_=rm.n_classes)
        tokenizer = Tokenizer.from_pretrained(pretrained_model, do_lower_case=False)
        tokenizer.add_tokens(['[E1]', '[/E1]', '[BLANK]'])
        e1_id = tokenizer.convert_tokens_to_ids('[E1]')
        e1n_id = tokenizer.convert_tokens_to_ids('[/E1]')
        train_set = proc_dataset(df_train, tokenizer=tokenizer, e1_id=e1_id,e1n_id=e1n_id)
        test_set = proc_dataset(df_test, tokenizer=tokenizer, e1_id=e1_id, e1n_id=e1n_id)
        train_length = len(train_set); test_length = len(test_set)
        PS = Pad_Sequence(seq_pad_value=tokenizer.pad_token_id,\
                          label_pad_value=tokenizer.pad_token_id,\
                          label2_pad_value=-1)
        train_loader = DataLoader(train_set, batch_size=self.batch_size, shuffle=True, \
                                  num_workers=0, collate_fn=PS, pin_memory=False)
        test_loader = DataLoader(test_set, batch_size=self.batch_size, shuffle=True, \
                                          num_workers=0, collate_fn=PS, pin_memory=False)
        cuda = torch.cuda.is_available()
        if cuda:
            print("USING GPU")
            net.cuda()
        else:
            print("USING CPU")
        
        net.resize_token_embeddings(len(tokenizer))
        criterion = nn.CrossEntropyLoss(ignore_index=-1)
        optimizer = optim.Adam([{"params":net.parameters(), "lr": self.learning_rate}])

        scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[2,4,6,8,12,15,18,20,22,23,24,26,30], gamma=0.8)
        
        start_epoch, best_pred, amp_checkpoint = self.load_state(net, optimizer, scheduler, self.model_no, self.model_path,load_best=False)
        
        losses_per_epoch, accuracy_per_epoch, test_f1_per_epoch = load_results(self.model_no)
        
        pad_id = tokenizer.pad_token_id
        mask_id = tokenizer.mask_token_id
        
        update_size =len(train_loader)//8
        
        joblib.dump(rm,self.model_path+'/class_info_'+str(self.model_no)+'.dmp')
        joblib.dump(tokenizer,self.model_path+'/tokenizer_'+str(self.model_no)+'.dmp')
        
        for epoch in tqdm(range(start_epoch, self.epochs)):
            start_time = time.time()
            net.train(); total_loss = 0.0; losses_per_batch = []; total_acc = 0.0; accuracy_per_batch = []
            for i, data in tqdm(enumerate(train_loader, 0),total=len(train_loader)):
#                 try:
                x, e1_e1n_start, labels, _,_,_ = data

                attention_mask = (x != pad_id).float()
                token_type_ids = torch.zeros((x.shape[0], x.shape[1])).long()

                if cuda:
                    x = x.cuda()
                    labels = labels.cuda()
                    attention_mask = attention_mask.cuda()
                    token_type_ids = token_type_ids.cuda()

                classification_logits = net(x, token_type_ids=token_type_ids, attention_mask=attention_mask,Q=None,e1_e1n_start=e1_e1n_start)


                loss = criterion(classification_logits, labels.squeeze(1))
                loss = loss/self.gradient_acc_steps


                loss.backward()

                if self.fp16:
                    grad_norm = torch.nn.utils.clip_grad_norm_(self.amp.master_params(optimizer), self.max_norm)
                else:
                    grad_norm = clip_grad_norm_(net.parameters(), self.max_norm)

                if (i % self.gradient_acc_steps) == 0:
                    optimizer.step()
                    optimizer.zero_grad()

                total_loss += loss.item()
#                 print(len(classification_logits),len(labels))
                total_acc += evaluate_(classification_logits, labels, ignore_idx=-1)[0]
                if (i % update_size) == (update_size - 1):
                    losses_per_batch.append(self.gradient_acc_steps*total_loss/update_size)
                    accuracy_per_batch.append(total_acc/update_size)
                    logger.info('[Epoch: %d, %5d/ %d points] total loss, accuracy per batch: %.3f, %.3f' %
                          (epoch + 1, (i + 1)*self.batch_size, train_length, losses_per_batch[-1], accuracy_per_batch[-1]))
                    total_loss = 0.0; total_acc = 0.0

                    scheduler.step()
                    results = evaluate_results_senti(net, test_loader, pad_id, cuda)
                    losses_per_epoch.append(sum(losses_per_batch)/len(losses_per_batch))
                    accuracy_per_epoch.append(sum(accuracy_per_batch)/len(accuracy_per_batch))
                    test_f1_per_epoch.append(results['f1'])
                    logger.info("Epoch finished, took %.2f seconds." % (time.time() - start_time))
                    logger.info("Losses at Epoch %d: %.7f" % (epoch + 1, losses_per_epoch[-1]))
                    logger.info("Train accuracy at Epoch %d: %.7f" % (epoch + 1, accuracy_per_epoch[-1]))
                    logger.info("Test f1 at Epoch %d: %.7f" % (epoch + 1, test_f1_per_epoch[-1]))

                    if accuracy_per_epoch[-1] > best_pred:
                        best_pred = accuracy_per_epoch[-1]
                        torch.save({
                                'epoch': epoch + 1,\
                                'state_dict': net.state_dict(),\
                                'best_acc': accuracy_per_epoch[-1],\
                                'optimizer' : optimizer.state_dict(),\
                                'scheduler' : scheduler.state_dict(),\
                                'amp': self.amp.state_dict() if self.amp is not None else self.amp
                            }, os.path.join(self.model_path , "task_test_model_best_%d.pth.tar" % self.model_no))

                    if (epoch % 1) == 0:
                        torch.save({
                                'epoch': epoch + 1,\
                                'state_dict': net.state_dict(),\
                                'best_acc': accuracy_per_epoch[-1],\
                                'optimizer' : optimizer.state_dict(),\
                                'scheduler' : scheduler.state_dict(),\
                                'amp': self.amp.state_dict() if self.amp is not None else self.amp
                            }, os.path.join(self.model_path, "task_test_checkpoint_%d.pth.tar" % self.model_no))
#                 except Exception as e:
#                     print(e)
#                     continue

        logger.info("Finished Training!")

def get_atsa_bert_input(data):
    text=data['content']
    start=data['start']
    end=data['end']
#         try:
    if start == end:
        stext=text.replace(data['entity'],'[E1]'+data['entity']+'[/E1]')
        if '[E1]' not in stext and '[/E1]' not in stext:
            data['sentiment'] = 'NEU'
            data['sentiment_score'] = 0.476
            return data
    else:
        stext=text[:start]+'[E1]'+text[start:]
        stext=stext[:end+4]+'[/E1]'+stext[end+4:]
    sents = [str(i) for i in nlp(stext.replace('[E1]','E1').replace('[/E1]','E2')).sents]
    stext_=''
    for _,sentence in enumerate(sents):
        if 'E1' in sentence:

            stext_=sentence.replace('E1','[E1]').replace('E2','[/E1]')
            sindex_s=_
        elif 'E2' not in sentence:
            stext_+=' '+sentence
        if 'E2' in sentence:

            if stext_!=sentence.replace('E1','[E1]').replace('E2','[/E1]'):

                stext_+=' '+sentence.replace('E1','[E1]').replace('E2','[/E1]')
            sindex_l=_
            break


#     print(stext_)
    if stext_ == '':
        del stext_
    stext_s=stext_

    stext_= sents[sindex_s-1]+' '+stext_ if sindex_s != 0 else stext_

    stext_= stext_+ ' ' + sents[sindex_l+1] if sindex_l < len(sents)-1 else stext_
    stext=stext_ if len(stext_.split()) <300 else (stext_s if len(stext_s.split()) <300 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:]))
    data['input_sen'] = stext
    return data
    
if __name__ == "__main__":
    trainer = RELTraining(model = 'bert-base-uncased',lower_case_flag = True, model_name = 'BERT', learning_rate=0.00007,max_length=50000,model_no=8,batch_size=8,gradient_acc_steps=2,max_norm=1,epochs=11,fp16=False,model_path="./rel_model/",amp = None)
    trainer.train(train_file, test_file, pretrained_model)