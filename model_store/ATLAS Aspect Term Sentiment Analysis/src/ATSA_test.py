import joblib
import torch
import os
import timeit
import time
# from src.ATSA_train import *
from joblib import Parallel,delayed
import copy
import pandas as  pd
import spacy
import joblib



def load_state(net, optimizer, scheduler,model_no,model_path, load_best=False):
    print("loading...")
    base_path = model_path
    amp_checkpoint = None
    checkpoint_path = os.path.join(base_path,"task_test_checkpoint_%d.pth.tar" % model_no)
    best_path = os.path.join(base_path,"task_test_model_best_%d.pth.tar" % model_no)
#     print(checkpoint_path,best_path)
    start_epoch, best_pred, checkpoint = 0, 0, None
    if (load_best == True) and os.path.isfile(best_path):
        checkpoint = torch.load(best_path)
        print("Loaded best model.")
    elif os.path.isfile(checkpoint_path):
        checkpoint = torch.load(checkpoint_path)
        print("Loaded checkpoint model.")
    if checkpoint != None:
#         print(checkpoint)
        start_epoch = checkpoint['epoch']
        best_pred = checkpoint['best_acc']
        net.load_state_dict(checkpoint['state_dict'])
        if optimizer is not None:
            optimizer.load_state_dict(checkpoint['optimizer'])
        if scheduler is not None:
            scheduler.load_state_dict(checkpoint['scheduler'])
        amp_checkpoint = checkpoint['amp']
        print("Loaded model and optimizer.")    
    return start_epoch, best_pred, amp_checkpoint

class Relations_Mapper(object):
    '''Maps relation to class and creates dictionary object to access relation by indexand vise versa'''
    def __init__(self, relations):
        self.rel2idx = {}
        self.idx2rel = {}
        
        print("Mapping relations to IDs...")
        self.n_classes = 0
        for relation in tqdm(relations):
            if relation not in self.rel2idx.keys():
                self.rel2idx[relation] = self.n_classes
                self.n_classes += 1
        
        for key, value in self.rel2idx.items():
            self.idx2rel[value] = key



class infer_from_trained(object):
    """ Loads saved model and optimizer states if exists 
    
        :ivar net: Network
        :ivar Optimizer: Tokenizer
        :ivarscheduler: 
        :ivar model_no: 
        :ivar model_path:
        :ivar load_best:
        """
    def __init__(self,class_info,tokenizer_path,model_path,pretrained_path='bert-base-uncased',model_no=0,best=True,model_size='bert-base-uncased'):
#         try:
        self.cuda = torch.cuda.is_available()
        self.rm=joblib.load(class_info)

        from src.BERT.modeling_bert import BertModelABSAE1 as Model
        model = model_size #'bert-base-uncased'
        lower_case = True
        model_name = 'BERT'
        self.net = Model.from_pretrained(pretrained_path, force_download=False, \
                                         model_size=model_size,\
                                         task='classification', n_classes_=self.rm.n_classes)

        self.tokenizer = joblib.load(tokenizer_path)
        self.net.resize_token_embeddings(len(self.tokenizer))

        if self.cuda:
            self.net.cuda()
        start_epoch, best_pred, amp_checkpoint = load_state(self.net, None, None, model_no,model_path, load_best=best)
        print("Loading bert model complete")
        self.e1_id = self.tokenizer.convert_tokens_to_ids('[E1]')
        self.e1n_id = self.tokenizer.convert_tokens_to_ids('[/E1]')
        self.pad_id = self.tokenizer.pad_token_id
        self.net.eval()
        print("Loading bert model complete")
#         except Exception as e:
#             print("Failed to load bert model")
        
        
    
    def get_e1e1n_start(self, x):
        e1_e1n_start = ([i for i, e in enumerate(x) if e == self.e1_id][0],\
                        [i for i, e in enumerate(x) if e == self.e1n_id][0])
        return e1_e1n_start
    
    def infer_one_sentence(self, sentence):
        tokenized = self.tokenizer.encode(sentence); 
        
    
        e1_e1n_start = self.get_e1e1n_start(tokenized); 
        
        
        tokenized = torch.LongTensor(tokenized).unsqueeze(0) ;
        
        
        e1_e1n_start = torch.LongTensor(e1_e1n_start).unsqueeze(0);
        
        
        attention_mask = (tokenized != self.pad_id).float();
        
        
        token_type_ids = torch.zeros((tokenized.shape[0], tokenized.shape[1])).long() ;
        
        
        if self.cuda:
            tokenized = tokenized.cuda()
            attention_mask = attention_mask.cuda()
            token_type_ids = token_type_ids.cuda()
        
        
        with torch.no_grad():
            classification_logits = self.net(tokenized, token_type_ids=token_type_ids, attention_mask=attention_mask, Q=None,\
                                        e1_e1n_start=e1_e1n_start)
            predicted = torch.softmax(classification_logits, dim=1)
        
        start_time = time.time() #time calc
#         li=[]
#         aa=predicted.shape[1]

#         for xx in range(aa):
#             print((self.rm.idx2rel[xx],predicted[0][xx].item()))
#             li.append((self.rm.idx2rel[xx],predicted[0][xx].item()))
#         return li     
        return self.rm.idx2rel[predicted.max(1)[1].item()],predicted.max(1)[0].item()
    
    def infer_sentence(self, sentence):
        '''  
        self.infer_one_sentence(sentence) 
        Args:
            sentence: String which is used to get predictions
        Returns:
            Predictions for given sentence
        '''
        
        return self.infer_one_sentence(sentence)
    
    def timeit_infersentence(self,sentence):
        timeit_complete = '''  
        self.infer_one_sentence(sentence) 
        Args:
            sentence: String which is used to get predictions
        '''
        print (timeit.timeit(stmt = timeit_complete, 
                     number = 10000)) 
        

        

def get_atsa_bert(model,data):
    text=data['content']
    start=data['start']
    end=data['end']
    try:
        if start == end:
            data['sentiment'] = 'NEU'
            data['sentiment_score'] = 0.4
            return data
        stext=text[:start]+'[E1]'+text[start:]
        stext=stext[:end+4]+'[/E1]'+stext[end+4:]
        stext_=[i for i in stext.split('.') if '[E1]' in i][0]
        stext=stext if len(stext.split()) <300 else (stext_ if len(stext_.split()) <300 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:]))
#         stext=stext_ if len(stext_.split()) <300 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:])
        out=model.infer_one_sentence(stext)
        data['sentiment'] = out[0]
        data['sentiment_score'] = out[1]
        return data
    except:
#         print(stext)
        data['sentiment'] = 'NEU'
        data['sentiment_score'] = 0.4
        return data
    

def get_entity_sentiment_atsa(absa_model_path,absa_model_no,pretrained_path,data):
    model_absa=infer_from_trained(absa_model_path+'/class_info_'+str(absa_model_no)+'.dmp',absa_model_path+'/tokenizer_'+str(absa_model_no)+'.dmp',absa_model_path,pretrained_path)
    data=data.progress_apply(lambda x: get_atsa_bert(model_absa,x),axis=1)
    try:
        pol_mapper = {'NEG':-1, 'POS':1, 'NEU':0}
        data['sentiment'] = data['sentiment'].progress_apply(lambda x : pol_mapper[x])
    except Exception:
        print('Already Numeric Notation')
    return data

def parallel(data,function,number_cuts=6,batchsize=20071):
    return pd.concat(Parallel(n_jobs=-1,max_nbytes='1M')(delayed(function)
                                 (copy.deepcopy(data.iloc[i*batchsize:i*batchsize+batchsize, ])) for i in (range(0, number_cuts))))


def get_entity_sentiment_atsa_Parallel(absa_model_path,absa_model_no,pretrained_path,data,n_cuts):
    model_size='bert-base-uncased'
    best=True
    model_no=absa_model_no
    model_path=absa_model_path
    cuda = False#torch.cuda.is_available()
    rm=joblib.load(absa_model_path+'/class_info_'+str(absa_model_no)+'.dmp')

    from src.BERT.modeling_bert import BertModelABSAE1 as Model
    model = model_size #'bert-base-uncased'
    lower_case = True
    model_name = 'BERT'
    net = Model.from_pretrained(pretrained_path, force_download=False, \
                                     model_size=model_size,\
                                     task='classification', n_classes_=rm.n_classes)

    tokenizer = joblib.load(absa_model_path+'/tokenizer_'+str(absa_model_no)+'.dmp')
    net.resize_token_embeddings(len(tokenizer))

    if cuda:
        net.cuda()
    start_epoch, best_pred, amp_checkpoint = load_state(net, None, None, model_no,model_path, load_best=best)
    print("Loading bert model complete")
    e1_id = tokenizer.convert_tokens_to_ids('[E1]')
    e1n_id = tokenizer.convert_tokens_to_ids('[/E1]')
    pad_id = tokenizer.pad_token_id
    net.eval()

    def get_atsa_bert_input(data):
        text=data['content']
        start=data['start']
        end=data['end']
        if start == end:
            data['input_sen'] = ''
            return data
        stext=text[:start]+'[E1]'+text[start:]
        stext=stext[:end+4]+'[/E1]'+stext[end+4:]
        stext_=[i for i in stext.split('.') if '[E1]' in i][0]
        stext=stext if len(stext.split()) <300 else (stext_ if len(stext_.split()) <300 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:]))
        data['input_sen'] = stext
        return data
    
#     def proc_data_creator(df):
#         def get_e1e1n_start(x, e1_id,e1n_id):
#             try:
#                 e1_e1n_start = ([i for i, e in enumerate(x) if e == e1_id][0],\
#                                 [i for i, e in enumerate(x) if e == e1n_id][0])
#             except Exception as e:
#                 e1_e1n_start = None
#                 logger.warn(e)
#             return e1_e1n_start

#         df['tokenized'] = df.apply(lambda x: torch.LongTensor(tokenizer.encode(x['input'])).unsqueeze(0),\
#                                                              axis=1)

#         df['e1_e1n_start'] = df.apply(lambda x: torch.LongTensor(get_e1e1n_start(x['tokenized'],\
#                                                        e1_id=e1_id, e1n_id=e1n_id)).unsqueeze(0), axis=1)
        
#         df['attention_mask'] = df.apply(lambda x: (x['tokenized'] != pad_id).float() , axis=1)
        
#         df['token_type_ids'] = df.apply(lambda x: torch.zeros((x['tokenized'].shape[0], x['tokenized'].shape[1])).long()  , axis=1)
        
#         print("\nInvalid rows/total: %d/%d" % (df['e1_e1n_start'].isnull().sum(), len(df)))
#         df.dropna(axis=0, inplace=True)
#         return df

    def proc_data_creator(df):
        def get_e1e1n_start(x, e1_id,e1n_id):
            try:
                e1_e1n_start = ([i for i, e in enumerate(x) if e == e1_id][0],\
                                [i for i, e in enumerate(x) if e == e1n_id][0])
            except Exception as e:
                e1_e1n_start = None
                logger.warn(e)
            return e1_e1n_start

        df['tokenized'] = df.apply(lambda x: tokenizer.encode(x['input']),\
                                                             axis=1)

        df['e1_e1n_start'] = df.apply(lambda x: get_e1e1n_start(x['tokenized'],\
                                                       e1_id=e1_id, e1n_id=e1n_id), axis=1)
        print("\nInvalid rows/total: %d/%d" % (df['e1_e1n_start'].isnull().sum(), len(df)))
#         df.dropna(axis=0, inplace=True)
        return df

    def infer_one_sentence(tokenized,e1_e1n_start):
#         tokenized = tokenizer.encode(sentence); 
#         e1_e1n_start = get_e1e1n_start(tokenized); 
        tokenized = torch.LongTensor(tokenized).unsqueeze(0) ;
        print(tokenized.shape)
        e1_e1n_start = torch.LongTensor(e1_e1n_start).unsqueeze(0);
        attention_mask = (tokenized != pad_id).float();
        token_type_ids = torch.zeros((tokenized.shape[0], tokenized.shape[1])).long() ;

        if cuda:
            tokenized = tokenized.cuda()
            attention_mask = attention_mask.cuda()
            token_type_ids = token_type_ids.cuda()

        with torch.no_grad():
            classification_logits = net(tokenized, token_type_ids=token_type_ids, attention_mask=attention_mask, Q=None,\
                                        e1_e1n_start=e1_e1n_start)
            predicted = torch.softmax(classification_logits, dim=1)
 
        return rm.idx2rel[predicted.max(1)[1].item()],predicted.max(1)[0].item()

#     def get_sentiment_data(inferer_d):
#         inferer_d= inferer_d.apply(lambda x: get_atsa_bert_input(x),axis=1)
#         inferer_d['input']=inferer_d['input_sen']
#         return proc_data_creator(inferer_d)

    def parallel_infer(inferer_d):
        inferer_d= inferer_d.apply(lambda x: get_atsa_bert_input(x),axis=1)
        inferer_d['input']=inferer_d['input_sen']
        inferer_d=proc_data_creator(inferer_d)
        inferer_d['sentiment']=inferer_d.apply(lambda x: infer_one_sentence(x['tokenized'],x['e1_e1n_start']),axis=1)
        return inferer_d
        
    data=parallel(data,parallel_infer,n_cuts,int(len(data)/n_cuts)+1)
#     data=parallel_infer(data)
    print(data)
    data['sentiment_score']=data['sentiment'].apply(lambda x: x[1])
    data['sentiment']=data['sentiment'].apply(lambda x: x[0])
    try:
        pol_mapper = {'NEG':-1, 'POS':1, 'NEU':0}
        data['sentiment'] = data['sentiment'].progress_apply(lambda x : pol_mapper[x])
        return data
    except Exception:
        return data
        print('Already Numeric Notation')

def parallel_sentiment_inference_cuda(absa_model_path,absa_model_no,pretrained_path,data,n_cuts):
    
    def get_para_sent(data):
        nlp=spacy.load('en_core_web_sm', disable=['parser', 'tagger', 'ner'])
        nlp.add_pipe(nlp.create_pipe('sentencizer'))

        def get_atsa_bert(model,data,):
            text=data['content']
            start=data['start']
            end=data['end']

#             try:
            if start == end:
                stext=text.lower().replace(data['entity'].lower(),'[E1]'+data['entity'].lower()+'[/E1]')
                if '[E1]' not in stext and '[/E1]' not in stext:
                    data['sentiment'] = 'NEU'
                    data['sentiment_score'] = 0.476
                    return data
            else:
                stext=text[:start]+'[E1]'+text[start:]
                stext=stext[:end+4]+'[/E1]'+stext[end+4:]
            sents = [str(i) for i in nlp(stext.replace('[E1]','E1').replace('[/E1]','E2')).sents]
            stext_=''
            sindex_s=None
            for _,sentence in enumerate(sents):
                if 'E1' in sentence:

                    stext_=sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_s=_
                elif 'E2' not in sentence:
                    stext_+=' '+sentence
                if 'E2' in sentence:

                    if stext_!=sentence.replace('E1','[E1]').replace('E2','[/E1]'):

                        stext_+=' '+sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_l=_
                    break


#             print(stext_)
            if stext_ == '':
                del stext_
            stext_s=stext_
            
            if sindex_s==None:
                data['sentiment'] = 'NEU'
                data['sentiment_score'] = 0.546
                return data

            stext_= sents[sindex_s-1]+' '+stext_ if sindex_s != 0 else stext_

            stext_= stext_+ ' ' + sents[sindex_l+1] if sindex_l < len(sents)-1 else stext_
            stext=stext_ if len(stext_.split()) <250 else (stext_s if len(stext_s.split()) <250 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:]))
    #         
            if '[E1]' in stext and '[/E1]' in stext and len(stext.split()) < 250:
                out=model.infer_one_sentence(stext)
                data['sentiment'] = out[0]
                data['sentiment_score'] = out[1]
                return data
            else:
                data['sentiment'] = 'NEU'
                data['sentiment_score'] = 0.576
                return data
#             except Exception as e:
#                 joblib.dump({str(stext):str(e)},'error_'+str(data['index']))
#                 data['sentiment'] = 'NEU'
#                 data['sentiment_score'] = 0.376
#                 return data
        model_absa=infer_from_trained(absa_model_path+'/class_info_'+str(absa_model_no)+'.dmp',absa_model_path+'/tokenizer_'+str(absa_model_no)+'.dmp',absa_model_path,pretrained_path)
        data=data.reset_index()
        data=data.progress_apply(lambda x: get_atsa_bert(model_absa,x),axis=1)
        return data
    print('Creating parallel instances...')
    data=parallel(data,get_para_sent,n_cuts,int(len(data)/n_cuts)+1)
#     data=get_para_sent(data)
    try:
        pol_mapper = {'NEG':-1, 'POS':1, 'NEU':0}
        data['sentiment'] = data['sentiment'].progress_apply(lambda x : pol_mapper[x])
    except Exception:
        print('Already Numeric Notation')
    return data

def parallel_sentiment_inference_test(absa_model_path,absa_model_no,pretrained_path,data,n_cuts):
    nlp=spacy.load('en_core_web_sm', disable=['parser', 'tagger', 'ner'])
    nlp.add_pipe(nlp.create_pipe('sentencizer'))
    
    def get_atsa_bert(model,data,):
        text=data['content']
        start=data['start']
        end=data['end']
        
        try:
            if start == end:
                stext=text.lower().replace(data['entity'].lower(),'[E1]'+data['entity'].lower()+'[/E1]')
                if '[E1]' not in stext and '[/E1]' not in stext:
                    data['sentiment'] = 'NEU'
                    data['sentiment_score'] = 0.476
                    return data
            else:
                stext=text[:start]+'[E1]'+text[start:]
                stext=stext[:end+4]+'[/E1]'+stext[end+4:]
            sents = [str(i) for i in nlp(stext.replace('[E1]','E1').replace('[/E1]','E2')).sents]
            stext_=''
            for _,sentence in enumerate(sents):
                if 'E1' in sentence:

                    stext_=sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_s=_
                elif 'E2' not in sentence:
                    stext_+=' '+sentence
                if 'E2' in sentence:

                    if stext_!=sentence.replace('E1','[E1]').replace('E2','[/E1]'):

                        stext_+=' '+sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_l=_
                    break


#             print(stext_)
            if stext_ == '':
                del stext_
            stext_s=stext_

            stext_= sents[sindex_s-1]+' '+stext_ if sindex_s != 0 else stext_

            stext_= stext_+ ' ' + sents[sindex_l+1] if sindex_l < len(sents)-1 else stext_
            stext=stext_ if len(stext_.split()) <250 else (stext_s if len(stext_s.split()) <250 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:]))
    #         
            if '[E1]' in stext and '[/E1]' in stext:
                out=model.infer_one_sentence(stext)
                data['sentiment'] = out[0]
                data['sentiment_score'] = out[1]
                return data
            else:
                data['sentiment'] = 'NEU'
                data['sentiment_score'] = 0.576
                return data
        except Exception as e:
            joblib.dump({str(stext):str(e)},'error_'+str(data['index']))
            data['sentiment'] = 'NEU'
            data['sentiment_score'] = 0.376
            return data
    
    def get_para_sent(data):
        model_absa=infer_from_trained(absa_model_path+'/class_info_'+str(absa_model_no)+'.dmp',absa_model_path+'/tokenizer_'+str(absa_model_no)+'.dmp',absa_model_path,pretrained_path)
        data=data.reset_index()
        data=data.progress_apply(lambda x: get_atsa_bert(model_absa,x),axis=1)
        return data
#     data=parallel(data,get_para_sent,n_cuts,int(len(data)/n_cuts)+1)
    data=get_para_sent(data)
    try:
        pol_mapper = {'NEG':-1, 'POS':1, 'NEU':0}
        data['sentiment'] = data['sentiment'].progress_apply(lambda x : pol_mapper[x])
    except Exception:
        print('Already Numeric Notation')
    return data


def parallel_sentiment_inference_sentences(data,n_cuts):
    nlp=spacy.load('en_core_web_sm', disable=['parser', 'tagger', 'ner'])
    nlp.add_pipe(nlp.create_pipe('sentencizer'))
    
    def get_atsa_sentence(data):
        text=data['content']
        start=data['start']
        end=data['end']
        
        try:
            if start == end:
                stext=text.lower().replace(data['entity'].lower(),'[E1]'+data['entity'].lower()+'[/E1]')
                if '[E1]' not in stext and '[/E1]' not in stext:
                    data['sentiment_sentence'] = stext
                    data['sentiment_error_flag'] =1 
                    return data
            else:
                stext=text[:start]+'[E1]'+text[start:]
                stext=stext[:end+4]+'[/E1]'+stext[end+4:]
            sents = [str(i) for i in nlp(stext.replace('[E1]','E1').replace('[/E1]','E2')).sents]
            stext_=''
            for _,sentence in enumerate(sents):
                if 'E1' in sentence:

                    stext_=sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_s=_
                elif 'E2' not in sentence:
                    stext_+=' '+sentence
                if 'E2' in sentence:

                    if stext_!=sentence.replace('E1','[E1]').replace('E2','[/E1]'):

                        stext_+=' '+sentence.replace('E1','[E1]').replace('E2','[/E1]')
                    sindex_l=_
                    break


#             print(stext_)
            if stext_ == '':
                del stext_
            stext_s=stext_

            stext_= sents[sindex_s-1]+' '+stext_ if sindex_s != 0 else stext_

            stext_= stext_+ ' ' + sents[sindex_l+1] if sindex_l < len(sents)-1 else stext_
            stext=stext_ if len(stext_.split()) <250 else (stext_s if len(stext_s.split()) <250 else ' '.join(stext_.split('[E1]')[0].split()[:-10])+'[E1]'+' '.join(stext_.split('[E1]')[1].split()[-10:])) 
            if '[E1]' in stext and '[/E1]' in stext:
                data['sentiment_sentence'] = stext
                data['sentiment_error_flag'] =0 
                return data
            else:
                data['sentiment_sentence'] = stext
                data['sentiment_error_flag'] =1 
                return data
        except Exception as e:
            data['sentiment_sentence'] = stext
            data['sentiment_error_flag'] =1
            return data
    
    def get_para_sentence(data):
        data=data.apply(lambda x: get_atsa_sentence(x),axis=1)
        return data
    data=parallel(data,get_para_sentence,n_cuts,int(len(data)/n_cuts)+1)

    return data


def parallel_sentiment_inference_bypass(absa_model_path,absa_model_no,pretrained_path,data,n_cuts,para=1):
    
    def get_para_sentiment(data):
        model_absa=infer_from_trained(absa_model_path+'/class_info_'+str(absa_model_no)+'.dmp',absa_model_path+'/tokenizer_'+str(absa_model_no)+'.dmp',absa_model_path,pretrained_path)
#         data=data.reset_index()
        data['sentiment']=data['sentiment_sentence'].progress_apply(lambda x: model_absa.infer_one_sentence(x))
        return data
    if para == 1:
        data=parallel(data,get_para_sentiment,n_cuts,int(len(data)/n_cuts)+1)
    else:
        data=get_para_sentiment(data)
#     try:
#         pol_mapper = {'NEG':-1, 'POS':1, 'NEU':0}
#         data['sentiment'] = data['sentiment'].progress_apply(lambda x : pol_mapper[x])
#     except Exception:
#         print('Already Numeric Notation')
    return data