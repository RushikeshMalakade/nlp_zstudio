import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
import logging
tf.get_logger().setLevel(logging.ERROR)
import warnings
warnings.filterwarnings(action='ignore')
import re
from unidecode import unidecode
import pandas as pd
from spacy.tokens import Doc
from spacy.vocab import Vocab
from spacy.gold import offsets_from_biluo_tags
import spacy
nlp=spacy.load('en_core_web_sm')
from unidecode import unidecode
import json
import logging
import pandas as pd
import itertools
from collections import Iterable
from sklearn.metrics import classification_report
import joblib 
from pandas import ExcelWriter
from tqdm.notebook import tqdm
tqdm.pandas()
import numpy as np


def text_tokenizer(string):
    '''
    Convert string to list of tokens and seperating non alaphabets from alpha numeric and extra spaces.
    Args:
        string: string to tokeinze
    Returns:
        List of tokens
    '''
    t = ' '
    for x in string :
        if not str.isalpha(x) and not x.isdigit() and x != ' ' :
            if t[-1] != ' ':
                t+= ' '
            t += x
            t += ' '
        else: t += x
    return unidecode(t[1:].strip())

def correct_BILU(new):
    '''Correct the tags from B,I,L,U and returns a new data
    Args:
        new: String with BILU tags
    Returns:
        x: String with corrected tags
        '''
    x=new.copy()
    for _,i in enumerate(x):
        if _ !=0 and _+1 < len(x):
            if (('O' in x[_-1][0] or 'U' in x[_-1][0] or 'L' in x[_-1][0]) and ('O' in x[_+1][0] or 'B' in x[_+1][0] or 'U' in x[_+1][0])): 
                x[_]=i.replace('I-','U-').replace('L-','U-').replace('B-','U-')
            elif ('O' in x[_-1][0] or 'U' in x[_-1][0] or 'L' in x[_-1][0]) and ('I' in x[_+1][0] or 'L' in x[_+1][0]): 
                x[_]=i.replace('I-','B-').replace('L-','B-')

            elif ('I-' in i) and (('B' in x[_-1][0] or 'I' in x[_-1][0]) and 'O' in x[_+1][0]):
                x[_]=i.replace('I-','L-')
            elif ('I-' in i or 'L-' in i or 'U-' in i) and ('O' in x[_-1][0] and (('O' not in x[_+1][0] )or ('B' not in x[_+1][0] ) )):
                x[_]=i.replace('I-','B-').replace('L-','B-').replace('U-','B-')
            else:
                x[_]=i
        elif  _ ==0 and _+1 < len(x):
            if ('I-' in i or 'L-' in i or 'B-' in i ) and ('O' in x[_+1][0] or 'B' in x[_+1][0] or 'U' in x[_+1][0]):
                x[_]=i.replace('I-','U-').replace('L-','U-').replace('B-','U-')
            elif ('I-' in i or 'L-' in i ) :
                x[_]=i.replace('I-','U-').replace('L-','U-').replace('B-','U-')
            else:
                x[_]=i
        elif  _ !=0 and _+1 >=len(x):
            if ('I-' in i or 'L-' in i or 'B-' in i ) and ('O' in x[_-1][0] or 'U' in x[_-1][0] or 'L' in x[_-1][0]):
                x[_]=i.replace('I-','U-').replace('L-','U-').replace('B-','U-')
            elif ('I-' in i ) and ('B' in x[_-1][0]):
                x[_]=i.replace('I-','L-')
            else:
                x[_]=i
        else:
            x[_]=i
    return x

def get_prodigy_format_data(token,tags,score=None):
    '''Creates span object for prodigy
    Args:
        token: List of string tokens
        tags: Tags for tokens
        score: 
    Returns:
        Dictinoary with data in spacy format
     '''
    try:
#         token=[unidecode(i) if unidecode(i) != '' else ' ' for i in token]

        out={"text":' '.join(token)}
        if score:
            out['score']=score
        doc = Doc(Vocab(), words=token)
        out['spans']=[{"start": start, "end": end, "label":label} for start, end, label in offsets_from_biluo_tags(doc, tags) ]
        
        return out
    except:
        return ''

def spacy_data_creator(file_path,out_path):
    ''' Applies prodiy format on data
    Args:
        file_path: String path to dataframe
        out_path: String path as destination 
    Returns:
        None'''
    train_data=pd.read_csv(file_path)
    train_data['token']=train_data['token'].apply(eval)
    train_data['ground']=train_data['ground'].apply(eval)
    out=list(train_data.apply(lambda x: get_prodigy_format_data(x['token'],x['ground']),axis=1))

    with open(out_path,'w',encoding='utf-8') as f:
        for i in out:
            x=json.dumps(i)
            f.writelines(x+'\n')

def spacy_data_creator_score(file_path,out_path):
    """Create output of BILU tagging on data
    Args:
        file_path: Path to input file as string
        out_path: Path to output as string
    Returns:
        None
    """
    train_data=pd.read_csv(file_path)
    
    train_data['token']=train_data['token'].apply(eval)
    train_data['pred']=train_data['pred'].apply(eval)
    
    out=[i for i in list(train_data.apply(lambda x: get_prodigy_format_data(x['token'],correct_BILU(x['pred']),x['score']),axis=1)) if i !='']

    with open(out_path,'w',encoding='utf-8') as f:
        for i in out:
            x=json.dumps(i)
            f.writelines(x+'\n')

    
def get_mapper(text):
    '''Maps data to index'''
    indexer={}
    cc=0
    for i,v in enumerate(text.split()):
        indexer[cc]=i
        cc+=len(v)+1
    return indexer

def relExtraction(input_dic):
    '''Extract relation labels from input dictionary'''
    dict_text = input_dic
    text = dict_text["text"]
    spans = dict_text["spans"]
    indexer=get_mapper(text)
    l = list(itertools.permutations(spans, 2))
    preds = []
    t_list = []
    for i in range(len(l)):
        if l[i][0]["start"] < l[i][1]["start"]:
            t = text[: l[i][0]["start"]] + "[E1]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E1]" + text[l[i][0]["end"]: l[i][1]["start"]] + "[E2]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E2]" + text[l[i][1]["end"]: ]
        elif l[i][0]["start"] > l[i][1]["start"]:
            t = text[: l[i][1]["start"]] + "[E1]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E1]" + text[l[i][1]["end"]: l[i][0]["start"]] + "[E2]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E2]" + text[l[i][0]["end"]: ]

        t_list.append(t)
        pred = inferer.infer_sentence(t)
        link = inferer.rm.idx2rel[pred[0]].strip()
        prob = pred[1]
        
        if '(e1' in link or 'other' in link:
            head=indexer[l[i][0]["start"]]
            child=indexer[l[i][1]["start"]]
        else:
            head=indexer[l[i][1]["start"]]
            child=indexer[l[i][0]["start"]]

        preds.append({"head": head, "child": child, "label": link, "score": prob})
        
    return preds

def get_tags(text):
    '''Extract tags from sentence'''
    sentence = Sentence(text)
    model.predict(sentence)
    out=[]
    for i in sentence.to_tagged_string().replace(' <B','_<B').replace(' <I','_<I').split(' '):
        if '_<B' in i or  '_<I' in i:
            out.append(i.split('_<')[1][:-1])
        else:
            out.append('O')
    return out



def flatten(items):
    """Yield items from any nested iterable; see Reference."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            for sub_x in flatten(x):
                yield sub_x
        else:
            yield x
def get_entities(binils):
    '''Extrace entities from input dictionary'''

     
    counter=0
    words=[]
    entity=[]
    score=[]
#     print(binils)
    while counter < len(binils):
#         print(counter)
        temp=[]
        temp_score=[]
        if counter < len(binils):
#             print(binils[counter])
            if binils[counter][1]=='O' and counter < len(binils):
                words.append(binils[counter][0])
                score.append(0)
                counter+=1
        if counter < len(binils):
            if 'B-' in binils[counter][1] and counter < len(binils):
                temp.append(binils[counter][0])
                temp_score.append(0)
                tg=binils[counter][1][2:]
                
                if counter >= len(binils):
                    break
                counter+=1
                if counter < len(binils):
    #                 print(counter)
                    while 'I-' in binils[counter][1] and counter < len(binils):
                        temp.append(binils[counter][0])
                        temp_score.append(0)
                        counter+=1
                        if counter >= len(binils):
                            break
                words.append(' '.join(temp).strip())
                score.append(np.mean(temp_score))
                entity.append((' '.join(temp).strip(),tg,np.mean(temp_score)))
        if counter < len(binils):
            if 'I-' in binils[counter][1] and counter < len(binils):
                temp.append(binils[counter][0])
                temp_score.append(0)
                tg=binils[counter][1][2:]
                counter+=1
                if counter < len(binils):
                    while binils[counter][1]=='I'and counter < len(binils):
                        temp.append(binils[counter][0])
                        temp_score.append(0)
                        counter+=1
                words.append(' '.join(temp).strip())
                score.append(np.mean(temp_score))
                entity.append((' '.join(temp).strip(),tg,np.mean(temp_score)))
    
    return entity    

def get_data_sorted_tag(data):
    '''Apply get_entities to the entire dataset'''
    data['sentence']=data['token'].apply(lambda x: ' '.join(x))
    
    
    data['pred']=data['pred'].astype(str).str.replace('L-','I-').str.replace('U-','B-').apply(eval)
    data['ground']=data['ground'].astype(str).str.replace('L-','I-').str.replace('U-','B-').apply(eval)
    data['goldstandard']=data.progress_apply(lambda x: get_entities(list(zip(x['token'],x['ground']))),axis=1)
    data.dropna(inplace=True)
    data['prediction']=data.progress_apply(lambda x: get_entities(list(zip(x['token'],x['pred']))),axis=1)
    return data[['sentence','goldstandard','prediction']]

def get_data_sorted_tag_word(test_df_l_50):
    return pd.DataFrame({'token':list(flatten(test_df_l_50['token'])),'ground':list(flatten(test_df_l_50['ground'])),'pred':list(flatten(test_df_l_50['pred']))})

def get_precision(x,y):
    '''return precision'''
    return str(100-(((x-y)/x)*100))+'%'

def get_recall(x,y):
    '''returns recall'''
    if x==0:
        if y ==0:
            return str(0)
        else: 
            return str((y+1)*100)+'%'
    else: return str((y/x)*100)+'%'
    
    

def get_preds(cnn_blstm,ls):
    '''Returns entity labels and prediction score
    Args:
        cnn_blstm: NER model
        ls: list of text for creating NER predictions
     '''
#     ls=[unidecode(text_tokenizer(i)) for i in ls]
#     ls=[unidecode(i) for i in ls]
#     print('prediction started')
    text,data_test_m=cnn_blstm.infer(ls)
#     print('prediction done')
    entity_ls=[]
    scores=[]
    for k,data_test in enumerate(data_test_m[1]):
        binils=[]
        binil_scores=[]
          
        for i in range(len(data_test)):
            binils.append([text[k][i],cnn_blstm.idx2Label[data_test[i]]])
            binil_scores.append(np.max(data_test_m[2][k][i]))
        entity_ls.append(binils)
        scores.append(binil_scores)
    return entity_ls,scores
    
def get_predictions(cnn_blstm,logger,test,text_type=None):
    '''Add predicted label and score columns to test data given in input after applying preprocessing
    Args:
        cnn_blstm: NER model for entity recognition
        test: Pandas dataframe for entity recognition  
    Returns:
        A dataframe
    '''
    if text_type:
        data_t['token']=data_t['text'].apply(unidecode).str.split(' ')
    test['text']=test['token'].apply(lambda x: ' '.join(x).strip())
    logger.info('starting prediction')
    try:
        x,s=get_preds(cnn_blstm,list(test['text']))
        logger.info("NER prediction generated")
    except Exception as e:
        logger.error("Failed to get NER prediction",exc_info=True)
    test['pred']=x
    test['pred']=test['pred'].apply(lambda x: [i[1] for i in x]) 
    test['score']=s
    try:
        test['pred']=test['pred'].apply(correct_BILU)
    except Exception as e:
        logger.error("BILU tagging failed",exc_info=True)
#     test=test.sort_values('score',ascending=True)
    return test

def get_summary(test_data):
    '''Generate result summary along with results'''
    test_data['token']=test_data['token'].astype(str).apply(eval)
    test_data['ground']=test_data['ground'].astype(str).apply(eval)
    test_data['pred']=test_data['pred'].astype(str).apply(eval)
#     test_data=test_data.copy()
    
    test_data1=test_data.copy()
    s=test_data1[test_data1['ground'].apply(len) != test_data1['pred'].apply(len)]
    test_data=test_data.drop(s.index)
    if len(s) != 0:
        test_data1=test_data1.drop(s.index)
        s['pred']=s['pred'].apply(lambda x: x.append('O'))
        test_data1=pd.concat([test_data1,s])
#     else:
#         print(s)
#     print(len(s))
    test_data['ground']=test_data['ground'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
    test_data['pred']=test_data['pred'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
#     print('Evaluation:')
#     print(classification_report(list(flatten(test_data['ground'])),list(flatten(test_data['pred']))))
    report=pd.DataFrame(classification_report(list(flatten(test_data['ground'])),list(flatten(test_data['pred'])),output_dict=True)).transpose() 
    sentence_level=get_data_sorted_tag(test_data1)
    temp={'id':[],'sentence':[],'phrase':[],'goldstandard':[],'prediction':[]}
    for _,i in sentence_level.iterrows():
        
        gs=dict(itertools.zip_longest(*[iter(i['goldstandard'])] * 2, fillvalue=""))
        #print(gs)
        ps=dict(itertools.zip_longest(*[iter(i['prediction'])] * 2, fillvalue=""))
        for k,l in gs.items():
            temp['id'].append(_)
            temp['sentence'].append(i['sentence'])
            temp['phrase'].append(k)
            temp['goldstandard'].append(l)
            if k in ps:
                temp['prediction'].append(ps[k])
                del ps[k]
            else:
                temp['prediction'].append(' ')
        for k,l in ps.items():
            temp['id'].append(_)
            temp['sentence'].append(i['sentence'])
            temp['phrase'].append(k)
            temp['goldstandard'].append(' ')
            temp['prediction'].append(l)
    phrase_level=pd.DataFrame(temp)
    out={'ID':[],'text':[],'token':[],'ground':[],'pred':[]}
    for _,i in test_data1.sort_index().iterrows():
        for l,j,k in zip(i['token'],i['ground'],i['pred']):
            out['ID'].append(_)
            out['text'].append(i['text'])
            out['ground'].append(j)
            out['pred'].append(k)
            out['token'].append(l)
#     print(pd.DataFrame(out))
    return report,pd.DataFrame(out),sentence_level,phrase_level


def get_summary_report(test_data):
    '''Generate result summary'''
    test_data['token']=test_data['token'].astype(str).apply(eval)
    test_data['ground']=test_data['ground'].astype(str).apply(eval)
    test_data['pred']=test_data['pred'].astype(str).apply(eval)
#     test_data=test_data.copy()
    
    test_data1=test_data.copy()
    s=test_data1[test_data1['ground'].apply(len) != test_data1['pred'].apply(len)]
    if len(s) != 0:
        test_data1=test_data1.drop(21)
        s['pred']=s['pred'].apply(lambda x: x.append('O'))
        test_data1=pd.concat([test_data1,s])
#     else:
#         print(s)
#     print(len(s))
    test_data['ground']=test_data['ground'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
    test_data['pred']=test_data['pred'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
#     print('Evaluation:')
#     print(classification_report(list(flatten(test_data['ground'])),list(flatten(test_data['pred']))))
    report=pd.DataFrame(classification_report(list(flatten(test_data['ground'])),list(flatten(test_data['pred'])),output_dict=True)).transpose() 
   
    return report


def get_train_test_support(phrase_level):
    '''Calculates precision,recalll and f1 score for input data'''
    out={'Entity':[],'Test Support':[],'Match':[],'Total_Predict':[]}
    for i in set(phrase_level['goldstandard']):
        out['Entity'].append(i)
        out['Test Support'].append(len(phrase_level[phrase_level['goldstandard'] == i]))
        out['Match'].append(len(phrase_level[phrase_level['goldstandard'] == i][phrase_level['goldstandard']==phrase_level['prediction']]))
        out['Total_Predict'].append(len(phrase_level[phrase_level['prediction'] == i]))
    out= pd.DataFrame(out)
    out['precision']=out['Match']/out['Total_Predict']
    out['recall']=out['Match']/out['Test Support']
    out['f1']=2*((out['precision']*out['recall'])/(out['precision']+out['recall']))
    out=out.fillna(0)
    out=out[out['Entity']!=' ']
    return out

def generate_report(train,test,out_path):
    '''For given train and test files generate output and save it at given location'''
    report_test,token_level_test,sentence_level_test,phrase_level_test=get_summary(test)
    report_train,token_level_train,sentence_level_train,phrase_level_train=get_summary(train)
    report_test=report_test.reset_index().rename({'index':'Entity'},axis=1)
    phrase_level_train_support=dict(phrase_level_train['goldstandard'].value_counts())
    token_level_train_support=dict(report_train['support'])
    report_test_exact=get_train_test_support(phrase_level_test)
    report_test_exact['train_support']=report_test_exact['Entity'].apply(lambda x: phrase_level_train_support[x] if x in phrase_level_train_support else 0 )
    report_test['train_support']=report_test['Entity'].apply(lambda x: token_level_train_support[x] if x in token_level_train_support else 0 )
    out={'token_level_evaluation':report_test,'entity_level_evaluation':report_test_exact,'output_token':token_level_test,'output_phrase':phrase_level_test,'Sentence_level':sentence_level_test}
    with ExcelWriter(out_path) as writer:
        for n, df in tqdm(out.items()):
            df.to_excel(writer,n)
        writer.save()
    print('done')
    
def generate_report_ui(train,test):
    report_test=get_summary_report(test)
    report_train=get_summary_report(train)
    report_test=report_test.reset_index().rename({'index':'Entity'},axis=1)
    token_level_train_support=dict(report_train['support'])
    report_test['train_support']=report_test['Entity'].apply(lambda x: token_level_train_support[x] if x in token_level_train_support else 0 )
    return report_test


def get_batch_predictions(data,cnn_blstm,batch_size,logger):
    '''
    Generate BIO tags for NER.
    Args:
        data: Pandas dataframe with token todetect Name Entities
        cnn_blstm: Trained NER model
        batch_size: Batch size to be used for preictions in one go
    Returns:
        Pandas Dataframe
    '''
    try:
        cc=0
        preds=[]
        print(len(data['token']))
        for i in tqdm(range(batch_size,len(data['token'])+batch_size,batch_size)):
            preds.append(get_predictions(cnn_blstm,logger,data.iloc[cc:i]))
            cc=i
        return pd.concat(preds)
    except:
        print("oops")
        logger.error("Failed to get NER tags for provided input",exc_info=True)


def correct_bio(pred):
    '''
        pred: [O,O,B-Back Pain,B-Back pain, O, O, B-Usage,I-Usage,B-Usage]
    
    
    '''
    out=pred.copy()
    for j,i in enumerate(pred):
    
        if j+1< len(pred):
            if i != 'O' and ('I-' or 'B-' in i):
                tok = i[0:2]
                tag = i[2:]
                if pred[j+1] != 'O':
                    if tag == pred[j+1][2:]:
                        out[j+1]='I-'+tag
                
                
    return out
        

import datetime

def get_char_indexes(text,phrases):
    try:
        counts={i: [(match.start(),match.end()) for match in re.finditer('\\b'+i.replace(' ','\s*').replace('+','\+').replace('*','\*').replace('$','\$')+'\\b', text, re.IGNORECASE)] if i not in ['$','"'] else [] for i in phrases}
        out=[]
        for j in range(len(phrases)):
            flg=0
            if len(counts[phrases[j]]) == 0:
                counts[phrases[j]]=[(match.start(),match.end()) for match in re.finditer(phrases[j].replace(' ','\s*').replace('$','\$').replace('+','\+'), text, re.IGNORECASE)]
                
            if len(counts[phrases[j]]) == 0:
                counts[phrases[j]]=[(match.start(),match.end()) for match in re.finditer(phrases[j].replace('$','\$').replace('*','\*').replace('^','\^'), text, re.IGNORECASE)]
            
            if len(counts[phrases[j]]) == 0:
                counts[phrases[j]]=[(match.start(),match.end()) for match in re.finditer(phrases[j].replace(' ','\s*').replace('*','\*').replace('$','\$'), text, re.IGNORECASE)]
                
            if len(counts[phrases[j]]) == 0:
                counts[phrases[j]]=[(match.start(),match.end()) for match in re.finditer(phrases[j], text, re.IGNORECASE)]

            if len(counts[phrases[j]])  == 1:
                flg=1
                out.append({'start':counts[phrases[j]][0][0],'end':counts[phrases[j]][0][1]})
            else:
                if j!=0:
                    for i in counts[phrases[j]]:

                        if i[0]>= out[-1]['end']:
                            flg=1
                            out.append({'start':i[0],'end':i[1]})
                            break
#                     if len(out)<j+1:
#                         print(phrases[j-1],out[-1]['end'],phrases[j])


                else:
                    i=counts[phrases[j]][0]
                    flg=1
                    out.append({'start':i[0],'end':i[1]})
            if flg == 0:
                out.append({'start':out[-1]['end'],'end':out[-1]['end']})
            
        if len(out) != len(phrases):
            print('ERROR 1')
            print(counts,text,phrases)
            print(len(out),len(phrases))
            print('\n')
        return out
    except:
#         print('ERROR 2')
#         print(text,phrases)
#         print(counts)
        out=[]
        for i in phrases:
            out.append({'start':0,'end':0})
        return out
        

def get_entities_with_index(binils):
    '''Extrace entities from input dictionary'''
#     print(binils)
     
    counter=0
    words=[]
    entity=[]
    score=[]
    k=0
    i=-1
    j=-2
#     print(binils)
    while counter < len(binils):
#         print(words)
        temp=[]
        temp_score=[]
        
        index=list()
        if counter < len(binils):
            
            if binils[counter][1]=='O' and counter < len(binils):
                i=j+2
                j=i+len(str(binils[counter][0]))-1
                words.append(binils[counter][0])
#                 print(binils[counter],1)
                score.append(binils[counter][2])
                
                counter+=1
        if counter < len(binils):
            if 'B-' in binils[counter][1] and counter < len(binils):
#                 print(binils[counter],2)
                temp.append(binils[counter][0])
                temp_score.append(binils[counter][2])
                i=j+2
                j=i+len(str(binils[counter][0]))-1
                tg=binils[counter][1][2:]
                
                if counter >= len(binils):
                    break
                index.append(counter)
                counter+=1
                if counter < len(binils):

 

                    while 'I-' in binils[counter][1] and counter < len(binils):

 

                        j=j+1
                        j=j+len(str(binils[counter][0]))
                        index.append(counter)
                        temp.append(binils[counter][0])
                        temp_score.append(binils[counter][2])
                        counter+=1
                        if counter >= len(binils):
                            break
                words.append(' '.join(temp).strip())
                score.append(np.mean(temp_score))
                
                entity.append((' '.join(temp).strip(),tg,np.mean(temp_score),index))
        if counter < len(binils):
            
            if 'I-' in binils[counter][1] and counter < len(binils):

 

                temp.append(binils[counter][0])
                temp_score.append(binils[counter][2])
                temp_index.append(counter)
                tg=binils[counter][1][2:]
                counter+=1
                if counter < len(binils):
                    while binils[counter][1]=='I'and counter < len(binils):

 

                        temp.append(binils[counter][0])
                        temp_score.append(binils[counter][2])
                        counter+=1
                words.append(' '.join(temp).strip())
                score.append(np.mean(temp_score))
                
                entity.append((' '.join(temp).strip(),tg,np.mean(temp_score),index))
    
    
    
    return entity
        

def get_entity_table(data,l4toparent,logger):
    '''Add entity, concept_type and concept columns to data
    Args:
        data: Pandas dataframe with text tokens and NER predictions data columns
        l4toparent: Dictionary to map concpet to its concept type
    Returns:
        Pandas dataframe with entity and concept type.
        '''
    logger.info("Generating entity table")
    try:
        
        data['token']=data['token'].astype(str).apply(eval)
        data['pred']=data['pred'].astype(str).apply(eval)
        data['score']=data['score'].astype(str).apply(eval)

        data['pred']=data['pred'].apply(lambda x:correct_BILU(x))

        print("starting conversion")
        data['pred']=data['pred'].astype('str').str.replace('L-','I-').str.replace('U-','B-').apply(eval)
        data['pred']=data['pred'].apply(lambda x:correct_bio(x))
        data['prediction']=data.progress_apply(lambda x: get_entities_with_index(list(zip(x['token'],x['pred'],x['score']))),axis=1)
        data['concept']=data['prediction'].apply(lambda x: [i[1] for i in x])
        data['entity']=data['prediction'].apply(lambda x: [i[0].replace('(','').replace(')','').replace('*','').strip() for i in x])
        data['score'] = data['prediction'].apply(lambda x: [i[2] for i in x])
        data['word_token']=data['prediction'].apply(lambda x: [i[3] for i in x])
        data['span']=data.progress_apply(lambda x: get_char_indexes(x['content'],x['entity']),axis=1)
        
        
        
    except Exception as e:
        logger.error("Failed to get entities from token",exc_info=True)

#     try:

    temp={i:[] for i in ['id','prod_id','brand','product', 'date', 'product_description', 'content','clean_content','entity','concept','concept_type', 'rating','score', 'source','start','end','word_token']}
    

    
    for _,i in data.iterrows():
        for ind,a in enumerate(i['entity']):
            for k in ['id','prod_id','brand','product', 'date', 'product_description', 'content','clean_content', 'rating','source',]:
                temp[k].append(i[k])
            temp['entity'].append(a)
            temp['concept'].append(i['concept'][ind])
            temp['start'].append(i['span'][ind]['start'])
            temp['end'].append(i['span'][ind]['end'])
            temp['concept_type'].append(l4toparent[i['concept'][ind]] if i['concept'][ind] in l4toparent else 'Other')
            temp['score'].append(i['score'][ind])
            temp['word_token'].append(i['word_token'][ind])  
        


    return pd.DataFrame(temp)


def rel_to_text(dict_text):
    '''Adds E1/ E2 relation tags in text'''
    text = dict_text["text"]
    spans = dict_text["spans"]
    l = list(itertools.permutations(spans, 2))
    out=[]
    for i in range(len(l)):
        if l[i][0]["start"] < l[i][1]["start"]:
            
            t = text[: l[i][0]["start"]] + "[E1]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E1]" + text[l[i][0]["end"]: l[i][1]["start"]] + "[E2]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E2]" + text[l[i][1]["end"]: ]
            e1=text[l[i][0]["start"]: l[i][0]["end"]]
            e2=text[l[i][1]["start"]: l[i][1]["end"]]
        elif l[i][0]["start"] > l[i][1]["start"]:
            t = text[: l[i][1]["start"]] + "[E2]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E2]" + text[l[i][1]["end"]: l[i][0]["start"]] + "[E1]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E1]" + text[l[i][0]["end"]: ]
            e1=text[l[i][0]["start"]: l[i][0]["end"]]
            e2=text[l[i][1]["start"]: l[i][1]["end"]]
        out.append((t,e1,e2))
    return out

def sentence_tokenization_for_graph(data,logger):
    '''Creates tokenized data needed to create graph
    Args:
        data:Pandas dataframe with cleaned token column
     Returns:
         pandas dataframe with cleaned tokens'''
    try:
        temp_out={i:[] for i in ['id', 'product', 'date', 'product_description', 'content', 'rating', 'brand', 'clean_content', 'token', 'text', 'pred','score']}

        for _,i in data.iterrows():
            temp_tok=[]
            temp_pred=[]
            

            for j,k in zip(i['token'],i['pred']):
                temp_tok.append(j)
                temp_pred.append(k)
                
                if j.strip() == '.':
                    for l in ['id', 'product', 'date', 'product_description', 'content', 'rating', 'brand', 'clean_content','score']:
                        temp_out[l].append(i[l])
                    temp_out['token'].append(temp_tok)
                    temp_out['pred'].append(temp_pred)
                    
                    temp_out['text'].append(' '.join(temp_tok))
                    temp_tok=[]
                    temp_pred=[]
                    
            if temp_tok and temp_pred:
                for l in ['id', 'product','date', 'product_description', 'content', 'rating', 'brand', 'clean_content','score']:
                    temp_out[l].append(i[l])
                temp_out['token'].append(temp_tok)
                temp_out['pred'].append(temp_pred)
                
                temp_out['text'].append(' '.join(temp_tok))
 
        return pd.DataFrame(temp_out)
    
    except Exception as e:
        logger.error("Failed to tokenize and clean the dataframe",exc_info=True)

def get_graph_data(data_,logger):
    '''Generate graph data for entity labels created from get_prodigy_format_data
    Args:
        data_: Pandas dataframe with cleaned token column
    Returns:
        pandas dataframe with graph level information in prodigy annotation format
     '''
#     display(data_)
    data_['token'] = data_['token'].astype(str).apply(eval)
    data_['pred'] = data_['pred'].astype(str).apply(eval)
    data_['score'] = data_['score'].astype(str).apply(eval)
    
    data=sentence_tokenization_for_graph(data_,logger)
    try:
        print("Creating spacy tags")
        data['spacy_tag']=data.progress_apply(lambda x: get_prodigy_format_data(x['token'],correct_BILU(x['pred']),x['score']),axis=1)
        data=data[data['spacy_tag'] != '']
        data['spacy_tag']=data['spacy_tag'].progress_apply(rel_to_text)
#         display(data)
        print("Spacy tags created")
        logger.info("Generating spacy tag sucessful")  
    except Exception as e:
        logger.error("Creating data to prodigy forma failed.",exc_info=True)

    try:
        print('Graph data population:')
        temp={i:[] for i in ['id','brand','product', 'date', 'product_description', 'content','clean_content','ann_content','entity1','entity2', 'rating']}
        for _,i in tqdm(data.iterrows()):
            for j in i['spacy_tag']:
                for k in ['id','brand','product', 'date', 'product_description', 'content','clean_content', 'rating']:
                    temp[k].append(i[k])
                temp['entity1'].append(j[1])
                temp['entity2'].append(j[2])
                temp['ann_content'].append(j[0])
        logger.info("Generating graph data sucessful")
        print("Graph Data Generated Successfully")
        return pd.DataFrame(temp)
    except Exception as e:
        logger.error("Could not create graph data",exc_info=True)

def get_concept_type_graph(data,entity_data,logger):
    """
    Function to generate concept type graph relationship.
    Args:
        data: Pandas dataframe with entity and concept information
        entity_data: Pandas dataframe to map concept to entity and concept to concept type
    Returns:
        Pandas dataframe
    """
    try:
        concept_mapper={str(i)+'_'+str(j).strip(): k for i,j,k in entity_data[['id','entity','concept']].values}
#         merged_concept_mapper={str(i)+'_'+str(j).strip(): k for i,j,k in entity_data[['id','entity','concept_merged']].values}
        
        
        concept_type_mapper={str(i)+'_'+str(j).strip(): k for i,j,k in entity_data[['id','entity','concept_type']].values}
        
        data['concept1']=data.progress_apply(lambda x: concept_mapper[str(x['id'])+'_'+str(x['entity1']).strip()] if str(x['id'])+'_'+str(x['entity1']).strip() in concept_mapper else 'EXCLUDE',axis=1)
        
#         data['merged_concept1']=data.progress_apply(lambda x: merged_concept_mapper[str(x['id'])+'_'+str(x['entity1']).strip()] if str(x['id'])+'_'+str(x['entity1']).strip() in concept_mapper else 'EXCLUDE',axis=1)
        
        data['concept2']=data.progress_apply(lambda x: concept_mapper[str(x['id'])+'_'+str(x['entity2']).strip()] if str(x['id'])+'_'+str(x['entity2']).strip() in concept_mapper else 'EXCLUDE',axis=1)
        
#         data['merged_concept2']=data.progress_apply(lambda x: merged_concept_mapper[str(x['id'])+'_'+str(x['entity1']).strip()] if str(x['id'])+'_'+str(x['entity1']).strip() in concept_mapper else 'EXCLUDE',axis=1)
        
        data['concept_type1']=data.progress_apply(lambda x: concept_type_mapper[str(x['id'])+'_'+str(x['entity1']).strip()]  if str(x['id'])+'_'+str(x['entity1']).strip() in concept_type_mapper else 'EXCLUDE',axis=1)
        
        data['concept_type2']=data.progress_apply(lambda x: concept_type_mapper[str(x['id'])+'_'+str(x['entity2']).strip()]  if str(x['id'])+'_'+str(x['entity2']).strip() in concept_type_mapper else 'EXCLUDE',axis=1)
        
        data=data[data['concept1'] != data['concept2']][(data['concept1'] != 'EXCLUDE') & (data['concept2'] != 'EXCLUDE')].drop_duplicates()
        data['inbetween_count']=data.progress_apply(lambda x: abs(x['ann_content'].split().index('[E1]'+x['entity1']+'[/E1]') -x['ann_content'].split().index('[E2]'+x['entity2']+'[/E2]')) if '[E1]'+x['entity1'].strip()+'[/E1]' in x['ann_content'].split() and '[E2]'+x['entity2'].strip()+'[/E2]' in x['ann_content'].split() else 0,axis=1)
        data=data[data['inbetween_count'] != 0]
        data=data[data['inbetween_count']<10]
        data['sent_len']=data['ann_content'].str.split().apply(len)
        data=data[data['sent_len'] < 350]
        logger.info("Concept type graph creation successful")
        return data
    
    except Exception as e:
        logger.error("Concept type graph creation failed",exc_info=True)
        
def change_concept_type(concept,concept_type):
    '''
    Changes concept type of specific concepts.
    Please keep updating it if any changes occur in ontology
    '''
    
    concept_type_mapper=dict(zip(concept,concept_type))

    other_mapper={'Length Of Ownership':'Consumer Contexts','Time Of Day':'Consumer Contexts','Measures Of Age':'Product Features',
                  'Innerspring Mattress':'Product Features','Tossing And Turning':'Consumer Journey','Made In Usa':'Product Features'}

    concept_type=concept.apply(lambda x:other_mapper[x] if x in other_mapper.keys() else concept_type_mapper[x])
    return concept_type

def pretrain_bert_domain(data_path,text_train_path,text_test_path,model_path):
    '''
        Input: 
                data_path: .csv file path containing scraped data and reviews should be in coulmn name 'content'
                text_train_path/test_train_path: path where to save intermmediate train/test file
                model_path: path to save the model
        Output:
                Returns None
                prints logs and after sucessful completion saves model in model_path
    '''

    data = pd.read_csv(data_path)
    raw_text = data[['content']]
    raw_text.drop_duplicates(subset ="Raw Review Text",keep = "first", inplace = True) 
    raw_text =  pd.DataFrame(raw_text)
    raw_text.columns=['Text']
    train,val = train_test_split(raw_text)
    train.to_csv(text_train_path, header=None, index=None, sep='\n', mode='a',encoding='utf-8')
    val.to_csv(text_test_path, header=None, index=None, sep='\n', mode='a',encoding='utf-8')
    os.system("mkdir "+model_path)
    exec_line='python src.run_language_modeling.py --output_dir='+model_path+'--model_type=bert --model_name_or_path=bert-base-uncased --do_train --train_data_file='+text_train_path +' --mlm'
    os.system(exec_line)

    
    
def rel_to_text_new(spans,text):
    '''Adds E1/ E2 relation tags in text'''
    
    
    l = list(itertools.permutations(spans, 2))
    out=[]
    for i in range(len(l)):
        
        if l[i][0]["start"] < l[i][1]["start"] and ("." not in text[l[i][0]["start"]: l[i][1]["end"]]) and ("!" not in text[l[i][0]["start"]: l[i][1]["end"]]) and ("?" not in text[l[i][1]["start"]: l[i][0]["end"]]):
            
            t = text[: l[i][0]["start"]] + "[E1]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E1]" + text[l[i][0]["end"]: l[i][1]["start"]] + "[E2]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E2]" + text[l[i][1]["end"]: ]
            e1=text[l[i][0]["start"]: l[i][0]["end"]]
            e2=text[l[i][1]["start"]: l[i][1]["end"]]
            out.append((t,e1,e2,l[i][0]["start"],l[i][0]["end"],l[i][1]["start"],l[i][1]["end"]))
        elif l[i][0]["start"] > l[i][1]["start"] and ("." not in text[l[i][1]["start"]: l[i][0]["end"]]) and ("!" not in text[l[i][1]["start"]: l[i][0]["end"]]) and ("?" not in text[l[i][1]["start"]: l[i][0]["end"]]) : 
            t = text[: l[i][1]["start"]] + "[E2]" + text[l[i][1]["start"]: l[i][1]["end"]] + "[/E2]" + text[l[i][1]["end"]: l[i][0]["start"]] + "[E1]" + text[l[i][0]["start"]: l[i][0]["end"]] + "[/E1]" + text[l[i][0]["end"]: ]
            e1=text[l[i][0]["start"]: l[i][0]["end"]]
            e2=text[l[i][1]["start"]: l[i][1]["end"]]
            out.append((t,e1,e2,l[i][0]["start"],l[i][0]["end"],l[i][1]["start"],l[i][1]["end"]))
    return out   

def inbetween(txt):
    txt=txt.replace('[E1]',' [E1] ').replace('[E2]',' [E2] ').replace('[/E2]',' [/E2] ').replace('[/E1]',' [/E1] ')

    split=txt.split()

    diff=min(abs(split.index('[E1]')-split.index('[/E2]')),abs(split.index('[E2]')-split.index('[/E1]')))
    return diff

def get_concept_type_graph_new(data,entity_data):
    """
    Function to generate concept type graph relationship.
    Args:
        data: Pandas dataframe with entity and concept information
        entity_data: Pandas dataframe to map concept to entity and concept to concept type
    Returns:
        Pandas dataframe
    """

    concept_mapper={str(i)+'_'+str(j).strip(): k for i,j,k in entity_data[['id','entity','concept']].values}
    


    concept_type_mapper={str(i)+'_'+str(j).strip(): k for i,j,k in entity_data[['id','entity','concept_type']].values}

    data['concept1']=data.progress_apply(lambda x: concept_mapper[str(x['id'])+'_'+str(x['entity1']).strip()] if str(x['id'])+'_'+str(x['entity1']).strip() in concept_mapper else 'EXCLUDE',axis=1)



    data['concept2']=data.progress_apply(lambda x: concept_mapper[str(x['id'])+'_'+str(x['entity2']).strip()] if str(x['id'])+'_'+str(x['entity2']).strip() in concept_mapper else 'EXCLUDE',axis=1)



    data['concept_type1']=data.progress_apply(lambda x: concept_type_mapper[str(x['id'])+'_'+str(x['entity1']).strip()]  if str(x['id'])+'_'+str(x['entity1']).strip() in concept_type_mapper else 'EXCLUDE',axis=1)

    data['concept_type2']=data.progress_apply(lambda x: concept_type_mapper[str(x['id'])+'_'+str(x['entity2']).strip()]  if str(x['id'])+'_'+str(x['entity2']).strip() in concept_type_mapper else 'EXCLUDE',axis=1)
    
    data=data[data['concept1'] != data['concept2']][(data['concept1'] != 'EXCLUDE') & (data['concept2'] != 'EXCLUDE')].drop_duplicates()
        
    data['inbetween']=data['ann_content'].progress_apply(inbetween)    
        
    data=data[(data['inbetween']!=0)&(data['inbetween']<=10)]    
            
    return data


def take_window(content):
    content=content.replace("!",".")
    sent_list = content.split('.')
    
    idx_e1=idx_e2=-1
    for i, sen in enumerate(sent_list):
        if '[E1]' in sen:
            
            idx_e1=i
            
            if "[E2]" in sen:
                idx_e2=i
            break
    if idx_e2==-1: 
        for i, sen in enumerate(sent_list):
            if '[E2]' in sen:

                idx_e2=i

                if "[E1]" in sen:
                    idx_e1=i
                break

   
    idx_min=min(idx_e1,idx_e2)
    idx_max=max(idx_e1,idx_e2)
        
    # aspect found in first sentence:
    if idx_min==0:
        if len(sent_list)==1:
            return '.'.join(sent_list)
        else:
            return '.'.join(sent_list[0:idx_max+2])
    
    # if aspect found in last sentence:
    elif idx_max==len(sent_list)-1:
        return '.'.join(sent_list[idx_min:])
    else:
        return '.'.join(sent_list[idx_min-1:idx_max+2])