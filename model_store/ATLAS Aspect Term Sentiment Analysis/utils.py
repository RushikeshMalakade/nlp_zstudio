import tempfile
import boto3
import joblib
import os
import requests
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk import RegexpParser
from nltk import Tree
import spacy
from nltk.corpus import stopwords
# import networkx as nx
from collections import Iterable
# from joblib import Parallel,delayed
import copy
import re

## stopwords
stop=stopwords.words('english')

## spacy nlp module
nlp = spacy.load("en_core_web_sm")
pattern="""NP: {(<VB\w?>)|(<NN\w?>)*}"""

# import sklearn.feature_extraction as sk
# stop = sk.text.ENGLISH_STOP_WORDS.union(stop)


class s3_handler():
    def __init__(self,folder_name,
    iamrole='aws-a0038-glbl-00-s-rol-shrd-awb-ads-stg_28',
    bucket_name='aws-a0038-glbl-00-s-s3b-shrd-awb-ads-stg-28'):
        r = requests.get('http://169.254.169.254/latest/meta-data/iam/security-credentials/'+iamrole)
        r = r.json()
        self.s3 = boto3.Session(aws_access_key_id=r['AccessKeyId'],aws_secret_access_key=r['SecretAccessKey'],
                            aws_session_token=r['Token'])
        self.s3 = self.s3.resource('s3').Bucket(bucket_name)
        self.folder_name='nlp_zstudio_models/'+folder_name
        
        
    def upload_object(self,obj,file_name):
        
        try:
            with tempfile.TemporaryFile() as fp:
                joblib.dump(obj, fp)
                fp.seek(0)
                self.s3.upload_fileobj(fp,self.folder_name+'/'+file_name)
            return { "Status":"Model Object Saved Sucessfully"}
        except Exception as e:
            print(e)
            return { "Status":"Model Object Upload Failed"}
    
    def load_object(self,file_name):
        
        try:
            with tempfile.TemporaryFile() as fp:
                self.s3.download_fileobj(Fileobj=fp, Key=self.folder_name+'/'+file_name)
                fp.seek(0)
                model = joblib.load(fp)
            return model
        except Exception as e:
            print(e)
            return { "Status":"Model Object Download Failed"}
            
                
def get_classification_ui_elements(data,labels):
    return ''.join(['<span class="model-label" style="background: '+labels[i[0]]+'; vertical-align: super;">'+i[0]+'<span class="model-label-inner">'+str(i[1])[:5]+'</span></span>' for i in data])

def get_ner_ui_elements(data,labels):
    return ' '.join(['<span class="model-label" style="background: '+labels[i[1]]+'; vertical-align: super;">'+i[0]+'<span class="model-label-inner">'+i[1]+'</span></span>&nbsp;' if i[1] != "O" else '<span style="vertical-align: super;">'+i[0]+'</span>&nbsp;' for i in data])

def get_merged_phrase(phrase,tag,all_=False):
    out = []
    prev ='O'
    temp=[]
    for i in range(len(tag)):
        
        if tag[i] != prev:
            if temp:
                out.append([' '.join(temp),prev])
            prev = tag[i]
            if tag[i] != 'O':
                temp=[phrase[i]]
            else:
                temp=[]
                if all_:
                    out.append([phrase[i],tag[i]])
 
        elif tag[i] == prev and tag[i] != 'O':
            temp.append(phrase[i])
            prev = tag[i]
        elif all_:
            out.append([phrase[i],tag[i]])
    if temp:
        out.append([' '.join(temp),prev])
            
    return out

def get_phrases(text,pattern,products=[],all_word=False):
    chunker = RegexpParser(pattern)
    trees=chunker.parse([(token.text, token.tag_) for token in nlp(text)])
    phrases=[]
    tags=[]
    for subtree in trees:
        
        if type(subtree) == Tree:
            temp=[]
            last_tag=''
            for _,subent in enumerate(subtree.leaves()):
                if subent[0] in products:
                    if last_tag == 'NN' and len(subtree.leaves()[_+1:]) < 2:
                        temp.append(subent[0])
                        break
                    elif len(subtree.leaves()[_:]) > 2:
                        temp.append(subent[0])
                        phrases.append(' '.join([v[0] for v in subtree.leaves()[_+1:]]))
                        tags.append('N')
                        break
                    
                    else:
                        break
                else:
                    last_tag=str(subent[1])
                    if str(subent[1]) == 'RB':
                        if str(list(nlp(lemmatizer.lemmatize(subent[0])))[0]) != subent[0]:
                            temp.append(subent[0])
                    else:
                        temp.append(subent[0])
            if ' '.join(temp) not in stop and ' '.join(temp) !=' ':
                cc=0
                if len(temp)>2:
#                     print(temp)
                    for i in range(0,len(temp)+2,2):
#                         print(temp[cc:i])
                        phrases.append(' '.join(temp[cc:i]))
                        tags.append('N')
                        cc=i
                    
                else:
                    phrases.append(' '.join(temp))
                    tags.append('N')
            else:
                if all_word == True:
                    phrases.append(' '.join(temp))
                    tags.append('O')
        else:
            if all_word == True:
                phrases.append(subtree[0])
                tags.append('O')
                
    return phrases,tags

        
        