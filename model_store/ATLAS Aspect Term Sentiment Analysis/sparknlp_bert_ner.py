#!/usr/bin/env python
# coding: utf-8

# ## Setup
# 
# 	1. Installing Java 8
# 		a. To remove previous versions: rpm -qa | grep openjdk | xargs  yum -y remove
# 		b. sudo yum install java-1.8.0-openjdk-devel
# 		c. update-alternatives --config java
# 		d. export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
# 
# 
# #!pip install spark-nlp==2.6.2
# #!pip install pyspark==2.4.4
# 
# #!java -version

# ## Initialization

# In[4]:


from pyspark.sql import SparkSession
from sklearn.metrics import classification_report
from pyspark.ml import Pipeline
from sparknlp.annotator import *
from sparknlp.common import *
from sparknlp.base import *
import sparknlp
from sparknlp.training import CoNLL
spark = sparknlp.start(gpu=True)


# In[5]:


# print("Spark NLP version: ", sparknlp.version())
# print("Apache Spark version: ", spark.version)


# ## DataLoader

# In[6]:


def get_conll(file):
    return CoNLL().readDataset(spark, file)


# ## Train SParkNLP-BERT

# In[7]:


def train_sparkbert(train,finetuned_path=None):
    if finetuned_path:
        print('loading finetuned')
        bert = BertEmbeddings.loadSavedModel(finetuned_path, spark).setInputCols(["sentence", "token"]).setOutputCol("bert").setCaseSensitive(False)
        print('[OK!]')
    else:
        bert = BertEmbeddings.pretrained('bert_base_cased', 'en').setInputCols(["sentence",'token']).setOutputCol("bert").setCaseSensitive(False)
    nerTagger = NerDLApproach().setInputCols(["sentence", "token", "bert"]).setLabelColumn("label").setOutputCol("ner").setMaxEpochs(40).setRandomSeed(10).setVerbose(1).setValidationSplit(0.1).setEvaluationLogExtended(True).setEnableOutputLogs(True).setIncludeConfidence(True)
    ner_pipeline = Pipeline(stages = [bert, nerTagger])
    return  ner_pipeline.fit(train)#.limit(1000)

#     .setPo(0.005)\
#     .setDropout(0.2)\
#.setLr(0.5)\

# def train_sparkbert(train,x):
#     bert = BertEmbeddings.pretrained('bert_base_cased', 'en').setInputCols(["sentence",'token']).setOutputCol("bert").setCaseSensitive(False)
#     nerTagger = NerDLApproach()\
#     .setInputCols(["sentence", "token", "bert"])\
#     .setLabelColumn("label")\
#     .setOutputCol("ner")\
#     .setMaxEpochs(40)\
#     .setRandomSeed(10)\
#     .setVerbose(1)\
#     .setValidationSplit(0.1)\
#     .setEvaluationLogExtended(True)\
#     .setEnableOutputLogs(True)\
#     .setIncludeConfidence(True)
#     ner_pipeline = Pipeline(stages = [bert, nerTagger])
#     return  ner_pipeline.fit(train)#.limit(1000)

# #     .setPo(0.005)\
# #     .setDropout(0.2)\
# #.setLr(0.5)\


# ## Save BertModel

# In[8]:


def save_sparkbert(ner_model,path):
    ner_model.stages[1].write().save(path)


# In[9]:


from collections import Iterable


def flatten(items):
    """Yield items from any nested iterable; see Reference."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            for sub_x in flatten(x):
                yield sub_x
        else:
            yield x
            
def len_matcher(data):
    if len(data['ground'])!=len(data['pred']):
        data['pred'].extend(['O' for i in range(len(data['ground'])-len(data['pred']))])
    return data


# ## Load Predictionpipeline-SParkNLP-BERT

# In[10]:


def predpipeline_bert(path,finetuned_path=None):
    loaded_ner_model = NerDLModel.load(path).setInputCols(["sentence", "token", "bert"]).setOutputCol("ner")
    document = DocumentAssembler().setInputCol("text").setOutputCol("document")

    sentence = SentenceDetector().setInputCols(['document']).setOutputCol('sentence')

    token = Tokenizer().setInputCols(['sentence']).setOutputCol('token')

    if finetuned_path:
        bert = BertEmbeddings.loadSavedModel(finetuned_path, spark).setInputCols(["sentence", "token"]).setOutputCol("bert").setCaseSensitive(False)
    else:
        bert = BertEmbeddings.pretrained('bert_base_cased', 'en').setInputCols(["sentence",'token']).setOutputCol("bert").setCaseSensitive(False)
    
    converter = NerConverter().setInputCols(["document", "token", "ner"]).setOutputCol("ner_span")

    ner_prediction_pipeline = Pipeline(
        stages = [
            document,
            sentence,
            token,
            bert,
            loaded_ner_model,
            converter])
    empty_data = spark.createDataFrame([['']]).toDF("text")
    return ner_prediction_pipeline.fit(empty_data)


# ## LightAnnotator

# In[11]:


def anotator_bert(prediction_model):
    return LightPipeline(prediction_model)


# ## Utils Functions

# In[12]:


def train_pred_eval(train_file,model_path,test_file,finetuned_path=None):
    get_ipython().system('rm -r $model_path')
    train=get_conll(train_file)
    print('Training on:',train_file)
    ner_model=train_sparkbert(train,finetuned_path)
    save_sparkbert(ner_model,model_path)
    test=get_conll(test_file)
    print('Evaluation on:',test_file)
    test=ner_model.transform(test).select('token.result','label.result','ner.result').toPandas()
    test.columns=['token','ground','pred']
    test=test.apply(len_matcher,axis=1)
    test['ground']=test['ground'].astype('str').str.replace('L-','I-').str.replace('U-','B-').apply(eval)
    test['pred']=test['pred'].astype('str').str.replace('L-','I-').str.replace('U-','B-').apply(eval)
    test1=test.copy()
    test['ground']=test['ground'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
    test['pred']=test['pred'].astype('str').str.replace('B-','').str.replace('I-','').str.replace('L-','').str.replace('U-','').apply(eval)
    print('Evaluation:')
    print(classification_report(list(flatten(test['ground'])),list(flatten(test['pred']))))
    return test1,pd.DataFrame(classification_report(list(flatten(test['ground'])),list(flatten(test['pred'])),output_dict=True)).transpose()


# In[13]:


def get_entities(binils):
#     print(binils)
    counter=0
    words=[]
    entity=[]
    while counter < len(binils):
#         print(counter)
        temp=[]
        if counter < len(binils):
            if binils[counter][1]=='O' and counter < len(binils):
                words.append(binils[counter][0])
                counter+=1
        if counter < len(binils):
            if 'B-' in binils[counter][1] and counter < len(binils):
                temp.append(binils[counter][0])
                tg=binils[counter][1][2:]
                
                if counter >= len(binils):
                    break
                counter+=1
                if counter < len(binils):
    #                 print(counter)
                    while 'I-' in binils[counter][1] and counter < len(binils):
                        temp.append(binils[counter][0])
                        counter+=1
                        if counter >= len(binils):
                            break
                words.append(' '.join(temp).strip())
                entity.append((' '.join(temp).strip(),tg))
        if counter < len(binils):
            if 'I-' in binils[counter][1] and counter < len(binils):
                temp.append(binils[counter][0])
                tg=binils[counter][1][2:]
                counter+=1
                if counter < len(binils):
                    while binils[counter][1]=='I'and counter < len(binils):
                        temp.append(binils[counter][0])
                        counter+=1
                words.append(' '.join(temp).strip())
                entity.append((' '.join(temp).strip(),tg))

    return entity

def get_data_sorted_tag(data):
    data['sentence']=data['token'].apply(lambda x: ' '.join(x))
    data['goldstandard']=data.progress_apply(lambda x: get_entities(list(zip(x['token'],x['ground']))),axis=1)
    data['prediction']=data.progress_apply(lambda x: get_entities(list(zip(x['token'],x['pred']))),axis=1)
    return data[['sentence','goldstandard','prediction']]
from pandas import ExcelWriter

def generate_output_excel(output,xls_path):
    with ExcelWriter(xls_path) as writer:
        for n, df in tqdm(output.items()):
            get_data_sorted_tag(df).to_excel(writer,n)
            get_data_sorted_tag_word(df).to_excel(writer,n+'_word')
        writer.save()
        
def get_data_sorted_tag_word(test_df_l_50):
    return pd.DataFrame({'token':list(flatten(test_df_l_50['token'])),'ground':list(flatten(test_df_l_50['ground'])),'pred':list(flatten(test_df_l_50['pred']))})


