from typing import Optional
from fastapi import BackgroundTasks,FastAPI,File, Form, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from flair.data import Sentence
from flair.models import MultiTagger
from utils import s3_handler,get_ner_ui_elements,get_merged_phrase
import json
from pydantic import BaseModel
from io import StringIO,BytesIO
import pandas as pd


origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://10.226.66.220:8888",
    "http://10.226.66.220",
    "http://10.226.66.220:8000",
]




class body(BaseModel):
    sentence: str
        
class bodylist(BaseModel):
    sentences:list
        


model_name='HunFlair Medical Extractor'

s3_handler_=s3_handler(model_name)

config = s3_handler_.load_object('config.json')
model=MultiTagger.load(config['model_path'])





app = FastAPI(title = config["model_name"].upper().replace('_',' ')+" API",
             version = config["model_version"],
            description = config["model_description"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def predict(sentence,all_=False):
    tag =[]
    phrase =[]
    sentence = Sentence(sentence)
    model.predict(sentence)
    tags={int(k):j[1] for j in [ (i.position_string,i.tag) for i in sentence.get_spans()] for k in j[0].split('-')}
    [(phrase.append(i.text),tag.append(tags[i.idx] if i.idx in tags else "O")) for i in sentence.tokens]
    out={'sentence':sentence.to_original_text(),'output':get_merged_phrase(phrase,tag,all_)}
    return out
    

@app.post("/predict_sentence/")
def predict_sentence(sentence: body):
    return predict(sentence.sentence)



@app.post("/predict_sentence_ui/")
def predict_sentence_ui(sentence: body):
    out=predict(sentence.sentence,True)
    out['UI'] = get_ner_ui_elements(out['output'],config['labels'])
    return out 

@app.post("/predict_sentences/")
def predict_sentences(sentences: bodylist):
    out={}
    for _,sentence in enumerate(sentences.sentences):
        print(sentence)
        out[_]=predict(sentence)         
    return out

@app.post("/predict_file/")
def predict_file(file: bytes = File(...)):
    s=str(file,'utf-8')
    data=pd.read_csv(StringIO(s))
    data['output']=data['text'].apply(lambda x: predict(x)['output'])
    towrite = StringIO()
    data.to_csv(towrite)
    towrite.seek(0)
    return towrite.getvalue()
   
@app.get("/summaries")
def model_summaries():
    return {'Report':model.report,"Accuracy Log":model.accuracy_log,"Loss Log":model.loss_log}



@app.get("/")
def home():
        return {"status":"200","message":"App is running"}

