"""nlp_zstudio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from nlp_zstudio_home.views import homepage,register_login,logout_request
from nlp_zstudio_models.views import model_list,model_register,model_redirect,model_play_ground,model_play_ground_prediction,column_options,model_play_ground_prediction_file

app_name = 'nlpzstudio'

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", homepage, name="homepage"),
    path("models/", model_list, name="model_list"),
    path("models/register/", model_register, name="model_register"),
    path("models/play_ground/", model_play_ground, name="model_play_ground"),
    path('annotator/', model_list, name="model_list"),
    path('pipelines/', model_list, name="model_list"),
    path("login/", register_login, name="register_login"),
    path("logout/", logout_request, name="logout"),
    path('tinymce/', include('tinymce.urls')),
    path("models/<redirect_url>/", model_redirect, name="model_redirect"),
    path("model_play_ground_prediction/", model_play_ground_prediction, name="model_play_ground_prediction"),
    path("model_play_ground_prediction_file/", model_play_ground_prediction_file, name="model_play_ground_prediction_file"),
    path("column_options/", column_options, name="column_options")
    
]
