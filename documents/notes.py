
# To create initial project
'django-admin startproject nlp_zstudio'

# To create app
'cd nlp_zstudio'
'python manage.py startapp nlp_zstudio_home'

# Register the app in settings
'''
    INSTALLED_APPS = [
        ...
        'nlp_studio_models.apps.NlpZstudioHomeConfig',
        ..
]
'''



# Homepage app build

 # create url redirect path from view in main app
''' 
    from nlp_zstudio_home.views import homepage

    urlpatterns = [ 
        ...,
        path("", views.homepage, name="homepage"),
    ]

'''

 # Create view in home app to render template

'''
    from django.shortcuts import render
    from django.http import HttpResponse
    import os

    # Create your views here.
    def homepage(request):
        return render(request = request,
                    template_name='homepage.html')
'''
 # add static and template folder in UI app. create html in template folder and js,css,img in static


# Models app Build

## create url redirect

'''
    from nlp_zstudio_models.views import modellist

    urlpatterns = [
    path('admin/', admin.site.urls),
    path("", homepage, name="homepage"),
    path("/models", modellist, name="modellist"),
    ]
'''

## add template path in models view 
'''
    from django.shortcuts import render
    from django.http import HttpResponse
    from .models import modelinfo_db

    # Create your views here.
    def model_list(request):
        return render(request = request,
                    template_name='model_list.html',
                    context = {"model_list":modelinfo_db.objects.all})
'''

## create database in models models.py

'''
    from django.db import models

    # Create your models here.
    class modelinfo_db(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    type= models.TextField()
    published = models.DateTimeField(auto_now=True)
'''

## Execute migrate command to create database
'python manage.py makemigrations'
'python3 manage.py migrate'
 
## Enable admin
'python3 manage.py createsuperuser'

## register model with Admin
'''
    from .models import modelinfo_db
    # Register your models here.
    admin.site.register(modelinfo_db)
'''


'''
cd ~/NLP\ ZStudio/nlp_zstudio/
python manage.py runserver 10.226.66.220:8000 &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/EBM\ NLP\ PICO\ element\ extraction/
uvicorn main:app --host 10.226.66.220 --port 8001 --reload &

conda activate pytorch_p36
cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Patient\ Journey\ Phase\ Identifier/
uvicorn main:app --host 10.226.66.220 --port 8002 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Social\ Media\ Toxic\ Comments\ Detection/
uvicorn main:app --host 10.226.66.220 --port 8003 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Tweets\ Sentiment\ Analysis/
uvicorn main:app --host 10.226.66.220 --port 8008 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Go\ Emotion\ Detection/
uvicorn main:app --host 10.226.66.220 --port 8010 --reload &

conda deactivate
cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Spacy\ English/
uvicorn main:app --host 10.226.66.220 --port 8004 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Biomedical\ Entity\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8005 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Med7
uvicorn main:app --host 10.226.66.220 --port 8006 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/HunFlair\ Medical\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8007 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Stanza\ Bio\ Clinical\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8009 --reload &


conda activate tensorflow_p36

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/ATLAS\ Aspect\ Term\ Sentiment\ Analysis/
uvicorn main:app --host 10.226.66.220 --port 8011 --reload &




'''

'''
tmux

jupyter notebook &

cd ~/NLP\ ZStudio/nlp_zstudio/
python manage.py runserver 10.226.66.220:8000 &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/EBM\ NLP\ PICO\ element\ extraction/
uvicorn main:app --host 10.226.66.220 --port 8001 --reload &

conda activate pytorch_p36
cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Patient\ Journey\ Phase\ Identifier/
uvicorn main:app --host 10.226.66.220 --port 8002 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Social\ Media\ Toxic\ Comments\ Detection/
uvicorn main:app --host 10.226.66.220 --port 8003 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Tweets\ Sentiment\ Analysis/
uvicorn main:app --host 10.226.66.220 --port 8008 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Go\ Emotion\ Detection/
uvicorn main:app --host 10.226.66.220 --port 8010 --reload &

conda deactivate
cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Spacy\ English/
uvicorn main:app --host 10.226.66.220 --port 8004 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Biomedical\ Entity\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8005 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Med7
uvicorn main:app --host 10.226.66.220 --port 8006 --reload &

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/HunFlair\ Medical\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8007 --reload &


cd ~/NLP\ ZStudio/nlp_zstudio/model_store/Stanza\ Bio\ Clinical\ Extractor/
uvicorn main:app --host 10.226.66.220 --port 8009 --reload &


conda activate tensorflow_p36

cd ~/NLP\ ZStudio/nlp_zstudio/model_store/ATLAS\ Aspect\ Term\ Sentiment\ Analysis/
uvicorn main:app --host 10.226.66.220 --port 8011 --reload &

'''
