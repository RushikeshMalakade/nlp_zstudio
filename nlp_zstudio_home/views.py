from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
import os
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import logout, authenticate, login
from .forms import NewUserForm

# Create your views here.
def homepage(request):
    return render(request = request,
                  template_name='homepage.html')


def register_login(request):
    if request.method == "POST":
        if 'signin' in request.POST:
            sigin_form = NewUserForm(request.POST)
            login_form = AuthenticationForm
            if sigin_form.is_valid():
                user = sigin_form.save()
                username = sigin_form.cleaned_data.get('username')
                login(request, user)    
                messages.success(request, 'Logged in as '+ username+'.')
                valuenext= request.POST.get('next')
                return redirect(valuenext if valuenext else '/')

            else:
                # for msg in form.error_messages:
                #     print(form.cleaned_data.get('username'),form.cleaned_data.get('password1'),form.cleaned_data.get('password2'))
                #     print(form.error_messages[msg])
                messages.info(request, 'Username Exits or invalid password!')
                return render(request = request,
                    template_name = "homepage.html",
                    context={"sigin_form":sigin_form,"login_form":login_form})
        
        if 'login' in request.POST:
            login_form = AuthenticationForm(request=request, data=request.POST)
            sigin_form = NewUserForm
            if login_form.is_valid():
                username = login_form.cleaned_data.get('username')
                password = login_form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    messages.info(request, f"You are now logged in as {username}")
                    valuenext= request.POST.get('next')
                    return redirect(valuenext if valuenext else '/')
                    
                else:
                    messages.info(request, "Invalid username or password.")
                    return render(request = request,
                    template_name = "homepage.html",
                    context={"sigin_form":sigin_form,"login_form":login_form})
            else:
                messages.info(request, "Invalid username or password.")
                return render(request = request,
                    template_name = "homepage.html",
                    context={"sigin_form":sigin_form,"login_form":login_form})

    login_form = AuthenticationForm
    sigin_form = NewUserForm

    return render(request = request,
                  template_name = "homepage.html",
                  context={"sigin_form":sigin_form,"login_form":login_form})


def logout_request(request):
    logout(request)
    messages.success(request, "Logged out successfully!")
    return redirect("/")