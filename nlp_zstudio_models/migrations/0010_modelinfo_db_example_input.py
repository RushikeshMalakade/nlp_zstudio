# Generated by Django 3.2.3 on 2021-10-07 08:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nlp_zstudio_models', '0009_alter_modelinfo_db_sample_input_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelinfo_db',
            name='Example_Input',
            field=models.TextField(default=1, max_length=2000),
        ),
    ]
