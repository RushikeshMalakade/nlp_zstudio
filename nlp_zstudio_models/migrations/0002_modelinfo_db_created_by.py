# Generated by Django 3.2.3 on 2021-09-22 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nlp_zstudio_models', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelinfo_db',
            name='Created_by',
            field=models.CharField(default='rm21016', max_length=300),
        ),
    ]
