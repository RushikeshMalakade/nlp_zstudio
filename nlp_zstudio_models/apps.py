from django.apps import AppConfig


class NlpZstudioModelsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nlp_zstudio_models'
