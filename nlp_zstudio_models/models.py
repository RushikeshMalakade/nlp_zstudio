from django.db import models
from django.db.models import fields
from django.shortcuts import redirect
from django.forms import ModelForm
from django import forms
from tinymce.widgets import TinyMCE

# Create your models here.

class modeltypes_db(models.Model):

    Model_Type= models.CharField(max_length=300)
    Model_Description= models.TextField(default=1)
    Model_Link= models.CharField(max_length=300,default=1)
    Model_Image= models.CharField(max_length=300)

    def __str__(self):
        return self.Model_Type  
    
class modelarchitecture_db(models.Model):
    Architecture_Type= models.CharField(max_length=300)
    Architecture_Description= models.TextField(default=1)
    Architecture_Link= models.CharField(max_length=300,default=1)
    Parameters= models.TextField()

    def __str__(self):
        return self.Architecture_Type

class architecturebase_db(models.Model):
    Architecture_Base= models.CharField(max_length=300)
    Architecture_Type= models.ForeignKey(modelarchitecture_db, default=1, verbose_name="base models", on_delete=models.SET_DEFAULT)

    def __str__(self):
        return str(self.Architecture_Base)

class modelall_db(models.Model):
    Model_Type=models.ForeignKey(modeltypes_db, default=1, verbose_name="Type Details", on_delete=models.SET_DEFAULT )
    Model_Architecture= models.ForeignKey(architecturebase_db, default=1, verbose_name="architectures", on_delete=models.SET_DEFAULT )

    def __str__(self):
        return str(self.Model_Type) + '-' +str(self.Model_Architecture)
    
class modelvisibility_db(models.Model):
    visibility= models.CharField(max_length=300)

    def __str__(self):
        return self.visibility

class modelinfo_db(models.Model):
    Created_by = models.CharField(max_length=300,default='rm21016')
    Model_Name = models.CharField(max_length=100,primary_key=True,unique=True)
    Model_Description = models.TextField()
    Model_Details= models.ForeignKey(modelall_db, default=1, verbose_name="model types", on_delete=models.SET_DEFAULT )
    Visibility=models.ForeignKey(modelvisibility_db, default=1, verbose_name="Visibility", on_delete=models.SET_DEFAULT )
    Redirect_Url= models.CharField(max_length=300,unique=True)
    Train_Data_Path= models.CharField(max_length=500)
    Model_Path= models.CharField(max_length=500)
    Test_Data_Path= models.CharField(max_length=500)
    Validation_Data_Path= models.CharField(max_length=500)
    Sample_Input_Text = models.TextField(default=1)
    Example_Input = models.CharField(max_length=2000,default=1)
    Host_Port= models.CharField(max_length=10)
    Host_Revo_IP= models.CharField(max_length=50)
    s3_Bucket_Name= models.CharField(max_length=300)
    Model_Labels=models.TextField()
    s3_Access_Key_ID=models.CharField(max_length=300)
    s3_Secret_Access_Key=models.CharField(max_length=300)
    s3_Session_Token=models.CharField(max_length=1000)
    Published_Date = models.DateTimeField(auto_now=True)


class modelinfo_form(ModelForm):
    Model_Description = forms.CharField(widget=TinyMCE(attrs={'cols': 50, 'rows': 10}))
    Model_Type = forms.ModelChoiceField(queryset=modeltypes_db.objects.all(),initial='Any')
    Architecture_Type = forms.ModelChoiceField(queryset=modelarchitecture_db.objects.all(),initial='Any',widget=forms.RadioSelect())
    Architecture_Base = forms.ModelChoiceField(queryset=architecturebase_db.objects.all(),initial='Any',widget=forms.RadioSelect())
    class Meta:
        model = modelinfo_db
        fields=['Model_Name','Model_Description','Model_Details','Model_Type','Visibility','Train_Data_Path','Test_Data_Path',
                'Validation_Data_Path','s3_Bucket_Name','s3_Access_Key_ID','s3_Secret_Access_Key','s3_Session_Token']
    
    



