from re import split, template
from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from django.db.models import Q

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from plotly.offline import plot
import plotly.graph_objs as go
import requests
import pandas as pd
from io import StringIO,BytesIO

def table_template(headers,rows):
    table='<table class="table table-dark"> <thead> <table class="table table-dark"> <tr> '
    for head in headers:
        table+=' <th scope="col">'+str(head)+'</th> '
    table+='</tr> </thead>  <tbody> '
    for row in rows:
        table+='  <tr> <th scope="row">'+str(row[0])+'</th> '
        for value in row[1:]:
            table+=' <td>'+str(value)+'</td> '
        table+='  </tr> '
    table+='  </tbody> </table> '
    return table

def line_plot(x,y):
    fig = go.Figure()
    scatter = go.Scatter(x=x,y=y,
                    mode='lines', name='test',
                    opacity=0.8, marker_color='green',fill='tozeroy')
    fig.add_trace(scatter)
    # fig.update_xaxes(showgrid=False, dtick=0.25, zerolinewidth=1.5, zerolinecolor='#C4C4C4')
    # fig.update_yaxes(showgrid=True, dtick=0.25, gridcolor="#F2F2F2", zerolinewidth=1.5, zerolinecolor='#C4C4C4')
    fig.update_layout(margin={'l': 0, 'r': 0, 't': 0, 'b': 0},
                    showlegend= False,
                    xaxis={'showgrid': True,
                            'zeroline': True,
                            'visible': True,
                            'title': ' '},
                    yaxis={'showgrid': True,
                            'zeroline': True,
                            'visible': True,
                            'title': ' '},
                    template='plotly_white',
                    height=500,
                    hoverlabel=dict(font_family="'Open Sans', sans-serif",
                                    bgcolor="#F2F2F2",
                                    bordercolor="#909090")
                            )
    return plot(fig, output_type='div', include_plotlyjs=False,show_link=False, link_text="")  
        
@login_required(login_url='/login/')
def model_redirect(request,redirect_url):
    modelinfo=modelinfo_db.objects.filter(Redirect_Url=redirect_url)[0]
    out={}
    
    labels_text=' '.join(['<p class="model-label" style="background:'+j+';">'+i+'</p>' for i,j in eval(modelinfo.Model_Labels.replace('</p>','').replace('<p>','')).items()])
    modelinfo.Model_Labels=labels_text
    out[modelinfo]={}
    return render(request = request,
                    template_name='model_info.html',
                    context = {"model_info":out})

# Create your views here.
@login_required(login_url='/login/')
def model_list(request):
    modelinfo=modelinfo_db.objects.filter(Q(Created_by=request.user) | Q(Visibility=1))
    return render(request = request,
                  template_name='model_list.html',
                  context = {"model_list":modelinfo})


@login_required(login_url='/login/')
def model_play_ground(request):
    modelinfo=modelinfo_db.objects.filter(Q(Created_by=request.user) | Q(Visibility=1))
    tasks=[]
    models_map={}
    labels_map={}
    labels = {}
    for i in modelinfo:
        tasks.append(i.Model_Details.Model_Type.Model_Type)
        if i.Model_Details.Model_Type.Model_Type not in  models_map: models_map[i.Model_Details.Model_Type.Model_Type] =[] 
        models_map[i.Model_Details.Model_Type.Model_Type].append(i.Model_Name)
        labels.update(eval(i.Model_Labels.replace('</p>','').replace('<p>','')))
        labels_map[i.Model_Name]=' '.join(['<p class="model-label" style="background:'+j+';">'+k+'</p>' for k,j in eval(i.Model_Labels.replace('</p>','').replace('<p>','')).items()])
        

    class tasks_form(forms.Form):
        task = forms.ChoiceField(choices = [(i,i) for i in set(tasks)])

    return render(request = request,
                  template_name='model_play_ground.html',
                  context = {"model_list":modelinfo,'models_map':models_map,'labels_map':labels_map,'labels':labels,'tasks':tasks_form})

@login_required(login_url='/login/')
def model_register(request):
    if request.method == "POST":
        model_form = modelinfo_form(request.POST)
        return render(request = request,
                  template_name='model_register.html',context={'model_form': model_form})
    model_form = modelinfo_form

    return render(request = request,
                  template_name='model_register.html',
                  context={'model_form': model_form, 'model_all':modelall_db.objects.all()})

@api_view(['GET','POST'])
def column_options(request):    
    data=request.data['file']
    return JsonResponse({'col_options':''.join(['<input style="margin-left: 2%; margin-right: 0.5%" type="radio" id="'+i+'" name="col_option" value="'+i+'"><label for="'+i+'">'+i+'</label>' for i in 
                            data.readline().decode('utf-8-sig').replace('\n','').replace('\r','').split(',')]),
                          'split_opt':''.join(['<input style="margin-left: 2%; margin-right: 0.5%" type="radio" id="'+i+'" name="split_option" value="'+i+'"><label for="'+i+'">'+i+'</label>' for i in 
                            ['Sentence','Document']]),
                            })

@api_view(['GET','POST'])
def model_play_ground_prediction(request):
    data = request.data
    out ={'labels':{},'sentence':data['sentence']}
    modelinfo=modelinfo_db.objects.filter(Model_Name__in=data['model_value'].split(','))
    # print(modelinfo,data['model_value'])
    for i in modelinfo:
        out['labels'].update(eval(i.Model_Labels.replace('</p>','').replace('<p>','')))
    out['labels_ui']=' '.join(['<p class="model-label" style="background:'+j+';">'+k+'</p>' for k,j in out['labels'].items()])
    out['output']={}
    out['output_ui']=''
    for i in modelinfo:
        # print('http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/')
        # print(requests.post(url = 'http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/',json={'sentence':data['sentence']}).json())
        out['output'][i.Model_Name] = requests.post(url = 'http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/',json={'sentence':data['sentence']}).json()
        out['output_ui']+='<a href="'+'/models/'+i.Model_Name.lower().replace(' ','_')+'" target="_blank" ><b>'+i.Model_Name+'</b></a><br><br>'+out['output'][i.Model_Name]['UI']+'<br><br><br>'
            


    return JsonResponse(out)

@api_view(['GET','POST'])
def model_play_ground_prediction_file(request):
    data=request.data['file']
    col_name= request.data['col_option']
    split_option=request.data['split_option']
    data=''.join([data.readlines()])
    data=pd.read_csv(StringIO(data))
    print(col_name,split_option)
    print(data)
    # out ={'labels':{},'sentence':data['sentence']}
    # modelinfo=modelinfo_db.objects.filter(Model_Name__in=data['model_value'].split(','))
    # # print(modelinfo,data['model_value'])
    # for i in modelinfo:
    #     out['labels'].update(eval(i.Model_Labels.replace('</p>','').replace('<p>','')))
    # out['labels_ui']=' '.join(['<p class="model-label" style="background:'+j+';">'+k+'</p>' for k,j in out['labels'].items()])
    # out['output']={}
    # out['output_ui']=''
    # for i in modelinfo:
    #     # print('http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/')
    #     # print(requests.post(url = 'http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/',json={'sentence':data['sentence']}).json())
    #     out['output'][i.Model_Name] = requests.post(url = 'http://'+i.Host_Revo_IP+':'+i.Host_Port+'/predict_sentence_ui/',json={'sentence':data['sentence']}).json()
    #     out['output_ui']+='<a href="'+'/models/'+i.Model_Name.lower().replace(' ','_')+'" target="_blank" ><b>'+i.Model_Name+'</b></a><br><br>'+out['output'][i.Model_Name]['UI']+'<br><br><br>'
            


    return b"cool,mul\n"