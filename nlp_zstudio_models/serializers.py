from rest_framework import serializers 
from nlp_zstudio_models.models import *
 
 
class architecturebase_sz(serializers.ModelSerializer):
 
    class Meta:
        model = architecturebase_db
        fields = ('Architecture_Base','Architecture_Type')

class modelvisibility_sz(serializers.ModelSerializer):
 
    class Meta:
        model = modelvisibility_db
        fields = ('visibility')

class modelarchitecture_sz(serializers.ModelSerializer):
 
    class Meta:
        model = modelarchitecture_db
        fields = ('Architecture_Type')


class modeltypes_sz(serializers.ModelSerializer):
 
    class Meta:
        model = modeltypes_db
        fields = ('Model_Type','Model_Image')

class modelall_sz(serializers.ModelSerializer):
 
    class Meta:
        model = modelall_db
        fields = ('Model_Type','Model_Image','Architecture_Base','Architecture_Type')


class modelinfo_sz(serializers.ModelSerializer):
 
    class Meta:
        model = modelinfo_db


