from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE
from django.db import models

# Register your models here.
# admin.site.register(modelinfo_db)

class model_listAdmin(admin.ModelAdmin):

    formfield_overrides = {
        models.TextField: {'widget': TinyMCE()},
        }


admin.site.register(modelinfo_db,model_listAdmin)
admin.site.register(architecturebase_db,model_listAdmin)
admin.site.register(modelvisibility_db,model_listAdmin)
admin.site.register(modelarchitecture_db,model_listAdmin)
admin.site.register(modeltypes_db,model_listAdmin)
admin.site.register(modelall_db,model_listAdmin)
