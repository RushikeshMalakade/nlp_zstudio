import tempfile
import boto3
import joblib
import os


class s3_handler():
    def __init__(self,folder_name,region_name='us-east-2',
    aws_access_key_id='ASIAX7UHZ7QCFHJD3KCO',
    aws_secret_access_key='UNKJ4fpQ+ON1AqHNIxPEhQK1lljMc7iNwbkaN/tA',
    aws_session_token='FwoGZXIvYXdzEP///////////wEaDMXdinbMlnRd3YJUVCK5AXsGstIr+CtCuF/JAy87GYmyjrIPDJGnatBFVhCy7ArBOzMbwKMBfHIXEOmzrDjPONi54PRRrk2nWTS+vXiw7Bat4PpYPYgs7lg74i7v7Vt0MLu0wCGE9fGeRaAtgKmawVC6kCJG+s61kCWYjLQYaXiXncLoaPOmLJMVMr8MbPJJghHzkVSNRGate8rCX8NyE0bm6qxqac/EedKgHMrBLiMwjSpHKsaTTAQ2CXEBdYu1f/aZKLUEpdlOKN/kuooGMi2vpF7lT94UW/hGEjRVrDltVHVo/W58gyJ3QE0Bva0JwPofE1Gp/fB34tM87b0=',
    bucket_name='aws-a0038-glbl-00-s-s3b-shrd-awb-ads-stg-28'):
        self.s3 = boto3.Session(region_name=region_name,aws_access_key_id=aws_access_key_id,aws_secret_access_key=aws_secret_access_key,
                            aws_session_token=aws_session_token)
        self.s3 = self.s3.resource('s3').Bucket(bucket_name)
        self.folder_name=folder_name
        
        
    def upload_object(self,obj,file_name):
        try:
            with tempfile.TemporaryFile() as fp:
                joblib.dump(obj, fp)
                fp.seek(0)
                self.s3.upload_fileobj(fp,self.folder_name+'/'+file_name)
            return { "Status":"Model Object Saved Sucessfully"}
        except Exception as e:
            print(e)
            return { "Status":"Model Object Upload Failed"}
    
    def load_object(self,file_name):
        try:
            with tempfile.TemporaryFile() as fp:
                self.s3.download_fileobj(Fileobj=fp, Key=self.folder_name+'/'+file_name)
                fp.seek(0)
                model = joblib.load(fp)
            return model
        except Exception as e:
            print(e)
            return { "Status":"Model Object Download Failed"}
            
                
        

        
        