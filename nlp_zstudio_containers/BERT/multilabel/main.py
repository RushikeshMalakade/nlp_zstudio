from typing import Optional
from fastapi import BackgroundTasks,FastAPI,File, Form, UploadFile
from classifier import *
from utils import s3_handler
import json

with open('config.json',) as f:
    config = json.load(f)

s3_handler_=s3_handler(config["model_name"])


model = s3_handler_.load_object(config["model_path"])
print(model)

if type(model) == dict:
    print('No model found')
    model=Bert_MultiLabel(s3_handler_.load_object(config["train_data_path"]), 
                          s3_handler_.load_object(config["val_data_path"]), 
                          s3_handler_.load_object(config["test_data_path"]) , None ,
                            base_path=config["base_path"],
                            train_batch_size=config["train_batch_size"],
                            valid_batch_size=config["valid_batch_size"],
                            epochs=config["epochs"],
                            learning_rate=config["learning_rate"],
                            max_len=config["max_len"])
    model.read_data()
    model.utils_to_train()
    s3_handler_.upload_object(model,config["model_path"])

app = FastAPI(title = config["model_name"].upper().replace('_',' ')+" API",
             version = config["model_version"],
            description = config["model_description"])

@app.get("/train")
def train(learning_rate: Optional[float] = config["learning_rate"], epochs: Optional[int] = config["epochs"]):
        
    global model,config
    
    try:
        model.learning_rate=learning_rate
        model.prepare_dataset()
        status=model.fine_tune_model(epochs)
        print(status)
        if type(status) == dict:
            return {"status": 200,"message":"Model is already trained for "+ str(epochs) + " Epochs."}
        model.test_report()
        config["model_version"]=int(config["model_version"])+1
        app.version = config["model_version"]
        config["learning_rate"]=model.learning_rate
        config["epochs"]=model.epochs
        config["model_path"]:"model_v"+ str(config["model_version"]) +".dmp"
        with open('config.json', 'w', encoding='utf-8') as f:
            json.dump(config, f, ensure_ascii=False, indent=4)
        print("model trained")
        response= s3_handler_.upload_object(model,config["model_path"])
        return response
    except Exception as e:
        print(e)
        return {"status":500 ,"message":"model training failed"}
    
@app.get("/")
def home():
        return {"status":"500","message":"home l"}

@app.get("/predict_sentence/{sentence}")
def predict_sentence(sentence: str):
    return model.predict_multilabels_sentences([sentence])

@app.get("/predict_sentence/{sentence}")
def predict_sentence(sentence: str):
    return model.predict_multilabels_sentences([sentence])

@app.get("/summaries")
def model_summaries():
    return {'Report':model.report,"Accuracy Log":model.accuracy_log,"Loss Log":model.loss_log}

@app.post("/files/")
def create_file(fileb: UploadFile = File(...)):
    return {"fileb_content_type": fileb.content_type,}