from django.apps import AppConfig


class NlpZstudioUiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nlp_zstudio_ui'
